﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System.Security.Cryptography;
using System.Linq;

public enum AttackerFocus { Gamer, Familiar, Enemy };
public class EventManager2 : MonoBehaviour
{
    static public EventManager2 instance2;

    //----Card Panel
    public static GameObject chosenCard;
    public static GameObject tempChosenCard;
    public static int maxCardsInHand = 7;

    //----Consumable Panel
    public static GameObject chosenConsumable;
    public static GameObject tempChosenConsumable;



    //Mechanics
    public static List<GameObject> CardsDeck = new List<GameObject>();
    public static List<GameObject> CardsSideDeck = new List<GameObject>();


    //----Enemy Panel
    public static int enemysToKill;
    public static GameObject chosenEnemy;
    public static int enemyStartCounter;
    public static int enemyEndCounter;

    //----Familiar Panel
    public static int familiarSlots;
    public static int familiarStartCounter;
    public static int familiarEndCounter;

    //Combat
    public static AttackerFocus attackerFocus;
    public static bool FightingPhase;
    public static int startEnergy = 5;
    public static int startFocus = 1;

    //Sniper
    public static GameObject sniperCard;
    public static SniperMode sniperMode;
    public static int burstSniper = 0;
    public static int ricochetSniper = 0;
    public static List<GameObject> sniperTargets = new List<GameObject>();

    //Settings
    public static float gameSpeed = 3f;

    //Timer
    public static float roundTimer = 15;

    void Awake()
    {
        instance2 = this;
        CombatSettingsOnAwake();
    }

    void Update()
    {
        if (Input.GetKeyDown("q"))
        {
            FightingPhase = true;
            GameObject.FindGameObjectWithTag("DeckCounter").transform.GetChild(1).GetComponent<DeckClockCounter>().StartCooldown();
        }
        if (Input.GetKeyDown("w"))
        {
            FightingPhase = false;
        }

    }



    void Start()
    {
        FightingPhase = false;
        SetupCombat();
        GameObject.FindGameObjectWithTag("HandPanel").GetComponent<CardMechanics>().DrawCardsOnStartTurn();
        StartCombat();

    }

    public static void StartCombat()
    {
        GameObject.FindGameObjectWithTag("ItemPanel").GetComponent<ItemManager>().StartCombat();
    }


    public static void SetupCombat()
    {
        //Set Focus
        attackerFocus = AttackerFocus.Gamer;

        //Create Cards and put in Random Ordner to Card
        EventManager2.CardsDeck.Clear();
        EventManager2.CardsSideDeck.Clear();
        PlayerPrefSerialize.LoadPlayerValues();
        for (int i = 0; i < PlayerPrefSerialize.playerValues.Deck.Count; i++)
        {
            GameObject scrollPrefab = Resources.Load<GameObject>("Combat2/CardPrefabs/Card");
            GameObject scrollClone = (GameObject)Instantiate(scrollPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            //Add Component & Transfer from Card To Hand
            scrollClone = EventManager2.AddGameComponent(scrollClone, PlayerPrefSerialize.playerValues.Deck[i], "Card");
            EventManager2.CardsDeck.Add(scrollClone);

        }
        //Shuffle Card
        ShuffleList.Shuffle(EventManager2.CardsDeck);

        //Push to Card
        for (int i = 0; i < EventManager2.CardsDeck.Count; i++)
        {
            EventManager2.CardsDeck[i].transform.SetParent(GameObject.Find("DeckCounterIn").transform.GetChild(0).transform, false);
        }

        //Update Card Coutner
        instance2.StartCoroutine(GameObject.Find("DeckCounterIn").transform.GetChild(0).GetComponent<UpdateDeckCounter>().UpdateCounter(0.1f));
        instance2.StartCoroutine(GameObject.Find("DeckCounterOut").transform.GetChild(0).GetComponent<UpdateSideDeckCounter>().UpdateCounter(0.1f));


    }

    public static GameObject AddGameComponent(GameObject gameObject, int id, string componentType)
    {
        if (componentType == "Card")
        {
            //Database 
            SkillsDatabase skillDB = GameObject.Find("Database").GetComponent<SkillsDatabase>();
            Skill tempSkill = skillDB.GetSkillByID(id);
            gameObject.AddComponent(System.Type.GetType(tempSkill.scriptName));
            Skill skillComponent = gameObject.GetComponent(System.Type.GetType(tempSkill.scriptName)) as Skill;
            skillDB.SyncValues(skillComponent, skillDB.GetSkillByID(id));

            //Card Gameobject
            gameObject.name = skillComponent.title;
            gameObject.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(skillComponent.imagePath);
            gameObject.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = skillComponent.title;
            gameObject.transform.GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().text = skillComponent.description;
            gameObject.transform.GetChild(0).GetChild(3).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = skillComponent.energyCost.ToString();



        }
        else if (componentType == "Enemy")
        {
            //Database 
            EnemiesDatabase enemyDB = GameObject.Find("Database").GetComponent<EnemiesDatabase>();
            Enemy tempEnemy = enemyDB.GetEnemyByID(id);
            gameObject.AddComponent(System.Type.GetType(tempEnemy.scriptName));
            Enemy enemyComponent = gameObject.GetComponent(System.Type.GetType(tempEnemy.scriptName)) as Enemy;
            enemyDB.SyncValues(enemyComponent, enemyDB.GetEnemyByID(id));

            //Card Gameobject
            gameObject.name = enemyComponent.title;
            gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(enemyComponent.imagePath);


        }
        else if (componentType == "Familiar")
        {
            //Database 
            FamiliarDatabase familiarDB = GameObject.Find("Database").GetComponent<FamiliarDatabase>();
            Familiar tempFamiliar = familiarDB.GetFamiliarByID(id);
            gameObject.AddComponent(System.Type.GetType(tempFamiliar.scriptName));
            Familiar familiarComponent = gameObject.GetComponent(System.Type.GetType(tempFamiliar.scriptName)) as Familiar;
            familiarDB.SyncValues(familiarComponent, familiarDB.GetFamiliarByID(id));

            //Card Gameobject
            gameObject.name = familiarComponent.title;
            gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(familiarComponent.imagePath);
        }
        else if (componentType == "Buff")
        {
            //Database 
            BuffsDatabase buffDB = GameObject.Find("Database").GetComponent<BuffsDatabase>();
            Buff tempBuff = buffDB.GetBuffByID(id);
            gameObject.AddComponent(System.Type.GetType(tempBuff.scriptName));
            Buff buffComponent = gameObject.GetComponent(System.Type.GetType(tempBuff.scriptName)) as Buff;
            buffDB.SyncValues(buffComponent, buffDB.GetBuffByID(id));

            //Card Gameobject
            gameObject.name = buffComponent.title;
            gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(buffComponent.imagePath);

        }
        else if (componentType == "Item")
        {

            //Database 
            ItemsDatabase itemDB = GameObject.Find("Database").GetComponent<ItemsDatabase>();
            Item tempBuff = itemDB.GetItemByID(id);
            gameObject.AddComponent(System.Type.GetType(tempBuff.scriptName));
            Item ItemComponent = gameObject.GetComponent(System.Type.GetType(tempBuff.scriptName)) as Item;
            itemDB.SyncValues(ItemComponent, itemDB.GetItemByID(id));

            //Card Gameobject
            gameObject.name = ItemComponent.title;
            gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(ItemComponent.imagePath);

        }
        else if (componentType == "Consumable")
        {
            //Database 
            ConsumablesDatabase consumableDB = GameObject.Find("Database").GetComponent<ConsumablesDatabase>();
            Consumable tempConsumable = consumableDB.GetConsumableByID(id);
            gameObject.AddComponent(System.Type.GetType(tempConsumable.scriptName));
            Consumable ConsumableComponent = gameObject.GetComponent(System.Type.GetType(tempConsumable.scriptName)) as Consumable;
            consumableDB.SyncValues(ConsumableComponent, consumableDB.GetConsumableByID(id));

            //Card Gameobject
            gameObject.name = ConsumableComponent.title;
            gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(ConsumableComponent.imagePath);
        }



        return gameObject;

    }
    //____________________________________________________________________________________FROM Card TO HAND
    public static void TransferCardFromDeckToHand()
    {

        EventManager2.CardsDeck.RemoveAt(0);
        //Falls In leer ist
        if (EventManager2.CardsDeck.Count == 0)
        {
            instance2.StartCoroutine(TransferCardFromSideDeckToDeck(0.6f));
        }

    }
    //____________________________________________________________________________________ FROM HAND TO SIDEDECK
    public static void TransferCardFromHandToSideDeck(GameObject Card)
    {
        EventManager2.CardsSideDeck.Add(Card);
    }
    public static void TransferCardFromHandToDeck(GameObject Card)
    {
        EventManager2.CardsDeck.Add(Card);
    }
    //____________________________________________________________________________________FROM SIDEDECK TO Card

    public static IEnumerator TransferCardFromSideDeckToDeck(float delay)
    {
        LockCursor(true);
        //Animation

        yield return new WaitForSeconds(delay);

        //Shuffle Card & Put into Card
        ShuffleList.Shuffle(EventManager2.CardsSideDeck);
        for (int i = 0; i < EventManager2.CardsSideDeck.Count; i++)
        {
            EventManager2.CardsSideDeck[i].transform.SetParent(GameObject.Find("DeckCounterIn").transform.GetChild(0).transform, false);
        }
        EventManager2.CardsDeck = new List<GameObject>();
        EventManager2.CardsDeck = EventManager2.CardsSideDeck;
        EventManager2.CardsSideDeck = new List<GameObject>();

        //Update Card Coutner
        instance2.StartCoroutine(GameObject.Find("DeckCounterIn").transform.GetChild(0).GetComponent<UpdateDeckCounter>().UpdateCounter(0.1f));
        instance2.StartCoroutine(GameObject.Find("DeckCounterOut").transform.GetChild(0).GetComponent<UpdateSideDeckCounter>().UpdateCounter(0.1f));
        //Draw rest
        int cardsToDraw = EventManager2.maxCardsInHand - GameObject.FindGameObjectWithTag("HandPanel").transform.childCount;
        if (cardsToDraw > 0)
        {
            instance2.StartCoroutine(GameObject.FindGameObjectWithTag("HandPanel").GetComponent<CardMechanics>().DrawXCards2(cardsToDraw, 1f));
        }
        //Unlock Cursor Again
        LockCursor(false);



    }
    //____________________________________________________________________________________
    public static void CombatSettingsOnAwake()
    {
        //Cursor unlock
        LockCursor(false);
    }
    public static void LockCursor(bool lockState)
    {
        if (lockState)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }


    private static bool ListContains(List<int> list, int value)
    {
        bool result = false;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == value)
            {
                return true;
            }
        }
        return result;
    }


    public static void DestroyConsumable(Component comp)
    {
        Destroy(comp);
    }






}
