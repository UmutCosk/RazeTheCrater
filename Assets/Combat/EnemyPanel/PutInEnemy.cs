﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PutInEnemy : MonoBehaviour
{
    public int size;
    Vector3 startPoint = new Vector3();
    Vector3 endPoint = new Vector3();

    GameObject EnemySize1;
    GameObject EnemySize2;
    GameObject EnemySize3;

    int size1counter;
    int size2counter;
    int size3counter;

    EnemySetsDatabase enemySetDB;
    EnemiesDatabase enemyDB;
    // Use this for initialization
    void Start()
    {
        EnemySize1 = Resources.Load("Combat2/EnemiePrefabs/Size1") as GameObject;
        EnemySize2 = Resources.Load("Combat2/EnemiePrefabs/Size2") as GameObject;
        EnemySize3 = Resources.Load("Combat2/EnemiePrefabs/Size3") as GameObject;
        enemySetDB = GameObject.Find("Database").GetComponent<EnemySetsDatabase>();
        enemyDB = GameObject.Find("Database").GetComponent<EnemiesDatabase>();

        enemySetDB.SetEnemySet();
        //  PutEnemySizeIntoCombat();
        PutEnemysIntoCombat();
    }

    float[] scaleArray;
    Vector3[] positionArray;
    void PutEnemysIntoCombat()
    {
        CombatPrefSerialize.LoadCombatValues();
        EnemySet curEnemySet = enemySetDB.GetEnemySetByID(CombatPrefSerialize.combatValues.CurrentEnemySetID);
        scaleArray = new float[] { curEnemySet.scale1, curEnemySet.scale2, curEnemySet.scale3, curEnemySet.scale4, curEnemySet.scale5 };
        positionArray = new Vector3[] { curEnemySet.position1, curEnemySet.position2, curEnemySet.position3, curEnemySet.position4, curEnemySet.position5 };


        for (int i = 0; i < CombatPrefSerialize.combatValues.EnemySetList.Count; i++)
        {
            int temp_id = CombatPrefSerialize.combatValues.EnemySetList[i];
            Enemy tempEnemy = enemyDB.GetEnemyByID(curEnemySet.monsterID1);
            int tempSize = tempEnemy.size;
            GameObject enemyPrefab = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Size" + tempSize);
            GameObject enemyClone = (GameObject)Instantiate(enemyPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;

            EventManager2.AddGameComponent(enemyClone, curEnemySet.monsterID1, "Enemy");

            enemyClone.transform.SetParent(GameObject.Find("EnemyPanel").transform.GetChild(0).transform, false);
            enemyClone.transform.localScale = new Vector3(1, 1, 1);
            enemyClone.transform.localScale = new Vector3(scaleArray[i], scaleArray[i], scaleArray[i]);
            Vector3 tempPosi = positionArray[i];
            enemyClone.transform.position = tempPosi;
            enemyClone.transform.GetChild(0).GetChild(4).GetComponent<Image>().sprite = Resources.Load<Sprite>(tempEnemy.imagePath);


        }
        /* 
                void PutEnemySizeIntoCombat()
                {

                    for (int i = 0; i < CombatPrefSerialize.combatValues.EnemySetList.Count; i++)
                    {
                        int temp_id = CombatPrefSerialize.combatValues.EnemySetList[i];
                        Enemy tempEnemy = enemyDB.GetEnemyByID(temp_id);

                        switch (tempEnemy.size)
                        {
                            case 1:
                                EventManager2.enemysToKill++;
                                Put1EnemyIn("EnemysGhost", temp_id);
                                Put1EnemyIn("Enemys", temp_id);
                                break;
                            case 2:
                                EventManager2.enemysToKill++;
                                Put2EnemyIn("EnemysGhost", temp_id);
                                Put2EnemyIn("Enemys", temp_id);
                                break;
                            case 3:
                                EventManager2.enemysToKill++;
                                Put3EnemyIn("EnemysGhost", temp_id);
                                Put3EnemyIn("Enemys", temp_id);
                                break;
                        }
                    }
                    StartCoroutine(IgnoreLayoutAndChangePivot(0.1f));
                }

                IEnumerator IgnoreLayoutAndChangePivot(float delay)
                {
                    yield return new WaitForSeconds(delay);

                    foreach (Transform child in GameObject.FindGameObjectWithTag("Enemys").transform)
                    {
                        child.GetComponent<LayoutElement>().ignoreLayout = true;

                    }
                }


            public List<IEnumerator> co = new List<IEnumerator>();
            IEnumerator MoveEnemiesAside(int index)
            {
                if (co.Count != 0)
                {
                    for (int i = 0; i < co.Count; i++)
                    {
                        StopCoroutine(co[i]);
                    }
                }

                yield return new WaitForSeconds(0.5f);
                for (int i = 0; i < GameObject.FindGameObjectWithTag("Enemys").transform.childCount; i++)
                {
                    if (index == i)
                    {
                        Vector3 adjustPosi = GameObject.FindGameObjectWithTag("EnemysGhost").transform.GetChild(i).transform.position;
                        co.Add(GameObject.FindGameObjectWithTag("Enemys").transform.GetChild(i).GetComponent<MoveToSpot>().MoveThisToPosition(adjustPosi, 0f, 0.001f, 10f));
                        StartCoroutine(co[co.Count - 1]);
                    }
                    else
                    {
                        Vector3 adjustPosi = GameObject.FindGameObjectWithTag("EnemysGhost").transform.GetChild(i).transform.position;
                        co.Add(GameObject.FindGameObjectWithTag("Enemys").transform.GetChild(i).GetComponent<MoveToSpot>().MoveThisToPosition(adjustPosi, 0f, 0.001f, 2f));
                        StartCoroutine(co[co.Count - 1]);
                    }

                    //StartCoroutine(Delay(0));
                }


            }
            IEnumerator Delay(int index)
            {
                yield return new WaitForSeconds(0.1f);
                for (int i = 0; i < GameObject.FindGameObjectWithTag("Enemys").transform.childCount; i++)
                {
                    if (!(GameObject.FindGameObjectWithTag("Enemys").transform.GetChild(i).transform.position.x < GameObject.FindGameObjectWithTag("EnemysGhost").transform.GetChild(i).position.x + 0.01 && GameObject.FindGameObjectWithTag("Enemys").transform.GetChild(i).transform.position.x > GameObject.FindGameObjectWithTag("EnemysGhost").transform.GetChild(i).position.x - 0.01))
                    {
                        StartCoroutine(MoveEnemiesAside(0));
                        break;
                    }
                }


            }

            public List<IEnumerator> co2 = new List<IEnumerator>();
            IEnumerator MoveEnemyToSlot(int index)
            {
                if (co2.Count != 0)
                {
                    for (int i = 0; i < co2.Count; i++)
                    {
                        StopCoroutine(co2[i]);
                    }
                }
                yield return new WaitForSeconds(0.5f);

                Vector3 adjustPosi = GameObject.FindGameObjectWithTag("EnemysGhost").transform.GetChild(index).transform.position;
                //adjustPosi.y += Random.Range(-0.02f, 0.3f);

                co2.Add(GameObject.FindGameObjectWithTag("Enemys").transform.GetChild(index).GetComponent<MoveToSpot>().MoveThisToPosition(adjustPosi, 0f, 0.001f, 5f));
                StartCoroutine(co2[co2.Count - 1]);




            }


            int OrderSiblingBySize(GameObject newEnemy)
            {

                if (newEnemy.GetComponent<Enemy>().size == 1)
                {
                    newEnemy.transform.SetAsFirstSibling();
                }
                if (newEnemy.GetComponent<Enemy>().size == 2)
                {
                    if (GetTheLastSiblingOfSize(1) > 9999) //kein einer Vorhanden
                    {
                        newEnemy.transform.SetAsFirstSibling();
                    }
                    else
                    {
                        newEnemy.transform.SetSiblingIndex(GetTheLastSiblingOfSize(1));
                    }


                }
                if (newEnemy.GetComponent<Enemy>().size == 3)
                {
                    if (GetTheLastSiblingOfSize(3) > 9999) //kein dreier Vorhanden
                    {
                        newEnemy.transform.SetAsLastSibling();
                    }
                    else
                    {
                        newEnemy.transform.SetSiblingIndex(GetTheLastSiblingOfSize(3));
                    }
                }

                return newEnemy.transform.GetSiblingIndex();
            }

            int GetTheLastSiblingOfSize(int size)
            {
                int result = 9999;
                foreach (Transform child in GameObject.FindGameObjectWithTag("EnemysGhost").transform)
                {
                    if (child.GetComponent<Enemy>().size == size)
                    {
                        result = child.transform.GetSiblingIndex();
                    }

                }
                return result + 1;
            }



            public int Put1EnemyIn(string tag, int id)
            {
                GameObject enemyPrefab = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Size1");
                GameObject enemyClone = (GameObject)Instantiate(enemyPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                int siblingIndex = 9999;

                if (tag == "Enemys2")
                {
                    enemyClone.transform.SetParent(GameObject.FindGameObjectWithTag("Enemys").transform, false);
                }
                else
                {
                    enemyClone.transform.SetParent(GameObject.FindGameObjectWithTag(tag).transform, false);
                }


                EventManager2.AddGameComponent(enemyClone, id, "Enemy");


                if (tag == "Enemys2")
                {

                    enemyClone.transform.position = GameObject.Find("EnemyStart").transform.GetChild(0).transform.position;
                    enemyClone.GetComponent<LayoutElement>().ignoreLayout = true;
                    tag = "Enemys";
                }



                //Child Order
                siblingIndex = OrderSiblingBySize(enemyClone);


                //Turn Off Image if tag GhostEnemys
                if (tag == "EnemysGhost")
                {
                    enemyClone.GetComponent<Image>().enabled = false;
                    foreach (Transform child in enemyClone.gameObject.transform)
                    {
                        Destroy(child.gameObject);
                    }

                }

                return siblingIndex;
            }



            public int Put2EnemyIn(string tag, int id)
            {
                GameObject enemyPrefab = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Size2");
                GameObject enemyClone = (GameObject)Instantiate(enemyPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                int siblingIndex = 9999;

                if (tag == "Enemys2")
                {
                    enemyClone.transform.SetParent(GameObject.FindGameObjectWithTag("Enemys").transform, false);
                }
                else
                {
                    enemyClone.transform.SetParent(GameObject.FindGameObjectWithTag(tag).transform, false);
                }

                EventManager2.AddGameComponent(enemyClone, id, "Enemy");


                if (tag == "Enemys2")
                {

                    enemyClone.transform.position = GameObject.Find("EnemyStart").transform.GetChild(0).transform.position;
                    enemyClone.GetComponent<LayoutElement>().ignoreLayout = true;
                    tag = "Enemys";
                }

                //Child Order
                siblingIndex = OrderSiblingBySize(enemyClone);



                //Turn Off Image if tag GhostEnemys
                if (tag == "EnemysGhost")
                {
                    enemyClone.GetComponent<Image>().enabled = false;
                    foreach (Transform child in enemyClone.gameObject.transform)
                    {
                        Destroy(child.gameObject);
                    }

                }

                return siblingIndex;
            }
            public int Put3EnemyIn(string tag, int id)
            {
                GameObject enemyPrefab = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Size3");
                GameObject enemyClone = (GameObject)Instantiate(enemyPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                int siblingIndex = 9999;

                if (tag == "Enemys2")
                {
                    enemyClone.transform.SetParent(GameObject.FindGameObjectWithTag("Enemys").transform, false);
                }
                else
                {
                    enemyClone.transform.SetParent(GameObject.FindGameObjectWithTag(tag).transform, false);
                }

                EventManager2.AddGameComponent(enemyClone, id, "Enemy");


                if (tag == "Enemys2")
                {
                    enemyClone.transform.position = GameObject.Find("EnemyStart").transform.GetChild(0).transform.position;
                    enemyClone.GetComponent<LayoutElement>().ignoreLayout = true;
                    tag = "Enemys";
                }

                //Child Order
                siblingIndex = OrderSiblingBySize(enemyClone);



                //Turn Off Image if tag GhostEnemys
                if (tag == "EnemysGhost")
                {
                    enemyClone.GetComponent<Image>().enabled = false;
                    foreach (Transform child in enemyClone.gameObject.transform)
                    {
                        Destroy(child.gameObject);
                    }

                }


                return siblingIndex;
            }


         */
    }
}
