﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;

public class MoveToSpot : MonoBehaviour
{
    int limit = 0;

    public IEnumerator MoveThisToPosition(Vector3 targetPosition, float delay, float functionSpeed, float clockSpeed)
    {
        yield return new WaitForSeconds(delay);

        this.transform.position = Vector3.MoveTowards(this.transform.position, targetPosition, clockSpeed * Time.deltaTime);
    
        if (!(this.transform.position.x < targetPosition.x + 0.01 && this.transform.position.x > targetPosition.x - 0.01))
        {
    
            StartCoroutine(MoveThisToPosition(targetPosition, functionSpeed, functionSpeed, clockSpeed));
        }
     
    }
  
   




}
