﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageStepEnemy : MonoBehaviour
{

    public static void EnemyAttack(GameObject enemy)
    {
        enemy.GetComponent<Enemy>().EnemyAttack();
    }

}
