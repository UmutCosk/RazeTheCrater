﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Timers;
using TMPro;

public class StandardCast : MonoBehaviour
{


    Image timerBar;
    float cooldown = 3f;
    float interval = 0.05f;
    float timeLeft;
    int skillIndex;

    void OnEnable()
    {

        this.name = "Shot";
        timerBar = this.GetComponent<Image>();
        timeLeft = cooldown;
        TimersManager.SetLoopableTimer(this, interval, CountDown);
    }

    public void SetDamageValueAndIndex(int value, int index)
    {
        this.transform.parent.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = value.ToString();
        skillIndex = index;


    }



    void CountDown()
    {
        timeLeft -= interval;
        float newAmount = 1 - timeLeft / cooldown;
        timerBar.fillAmount = newAmount;

        if (timeLeft <= 0)
        {
            this.transform.parent.parent.parent.parent.GetComponent<Enemy>().ShotGamer(skillIndex);
            Destroy(this.transform.parent.gameObject);
        }
    }



}
