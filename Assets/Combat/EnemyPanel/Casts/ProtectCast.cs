﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Timers;
using TMPro;

public class ProtectCast : MonoBehaviour
{


    Image timerBar;
    float cooldown = 3f;
    float interval = 0.05f;
    float timeLeft;

    int armorValue;
    void OnEnable()
    {

        this.name = "Protect";
        timerBar = this.GetComponent<Image>();
        timerBar.fillAmount = 1;
        TimersManager.SetLoopableTimer(this, interval, CountDown);
        ResetCountdown();
    }


    public void SetArmorValue(int value)
    {
        armorValue = value;
        this.transform.parent.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = value.ToString();
    }



    void CountDown()
    {
        timeLeft -= interval;
        float newAmount = 1 - timeLeft / cooldown;
        timerBar.fillAmount = newAmount;

        if (timeLeft <= 0)
        {
            if (!this.transform.parent.parent.parent.parent.GetComponent<BaseEnemy>().armorUp)
            {
                this.transform.parent.parent.parent.GetChild(1).GetChild(1).transform.gameObject.SetActive(true);
                this.transform.parent.parent.parent.parent.GetComponent<BasePlayer>().armorUp = true;
            }
            else
            {
                this.transform.parent.parent.parent.GetChild(1).GetChild(1).GetChild(0).GetComponent<ArmorBar>().ResetCountdown();
            }

            this.transform.parent.parent.parent.parent.GetComponent<Enemy>().GetBuffed(this.transform.parent.parent.parent.parent.gameObject, BuffType.Armor, armorValue, 0f);
            ResetCountdown();
            Destroy(this.transform.parent.gameObject);
        }


    }

    public void ResetCountdown()
    {
        timeLeft = cooldown;
    }


}
