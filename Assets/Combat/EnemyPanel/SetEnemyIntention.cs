﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetEnemyIntention : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public void SetIntention(Intention intention)
    {
        switch (intention)
        {
            case Intention.Attack:
                this.gameObject.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Combat2/Enemy/Intention/attack");
                break;
            case Intention.Buff:
                this.gameObject.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Combat2/Enemy/Intention/buff");
                break;
            case Intention.Defend:
                this.gameObject.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Combat2/Enemy/Intention/armor");
                break;
        }
    }
}
