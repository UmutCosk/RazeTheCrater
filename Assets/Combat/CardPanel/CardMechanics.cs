﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardMechanics : MonoBehaviour
{

    public bool movedAllToRight = false;
    int cardsInHandWhenDrawing;
    void Start()
    {

    }

    void ChangeColor(GameObject card)
    {
        SkillCategory tempCategory = card.GetComponent<Skill>().skillCategory;
        if (tempCategory == SkillCategory.Standard || tempCategory == SkillCategory.Throw || tempCategory == SkillCategory.Sniper)
        {
            card.transform.GetChild(0).GetComponent<Image>().color = new Color(185, 65, 65, 255) / 255f;
        }
        else if (tempCategory == SkillCategory.Protect)
        {
            card.transform.GetChild(0).GetComponent<Image>().color = new Color(65, 185, 65, 255) / 255f;
        }
        else if (tempCategory == SkillCategory.Drug)
        {
            card.transform.GetChild(0).GetComponent<Image>().color = new Color(65, 65, 185, 255) / 255f;
        }
        else if (tempCategory == SkillCategory.Quick)
        {
            card.transform.GetChild(0).GetComponent<Image>().color = new Color(255, 255, 0, 255) / 255f;
        }
    }

    public IEnumerator DropAll(float timer)
    {
        EventManager2.chosenCard = null;
        EventManager2.tempChosenCard = null;

        yield return new WaitForSeconds(timer);
        List<GameObject> scrollsToDrop = new List<GameObject>();
        foreach (Transform child in this.gameObject.transform)
        {
            scrollsToDrop.Add(child.gameObject);
        }
        for (int i = 0; i < scrollsToDrop.Count; i++)
        {
            scrollsToDrop[i].GetComponent<Card>().allDropped = true;
            scrollsToDrop[i].GetComponent<Card>().DropThisCard();
        }
        GameObject.FindGameObjectWithTag("HandPanel").GetComponent<CardMechanics>().movedAllToRight = false;
    }


    public void DrawCardsOnStartTurn()
    {

        PutInGhostCard(EventManager2.maxCardsInHand);
        int cardsToDraw = EventManager2.maxCardsInHand - GameObject.FindGameObjectWithTag("HandPanel").transform.childCount;

        StartCoroutine(GameObject.FindGameObjectWithTag("HandPanel").GetComponent<CardMechanics>().DrawXCards2(cardsToDraw, 0.1f));
    }


    public void PutInGhostCard(int numberOfCardsToDraw)
    {
        foreach (Transform child in GameObject.Find("GhostPanel2").transform)
        {
            Destroy(child.gameObject);
        }
        for (int i = 0; i < numberOfCardsToDraw; i++)
        {
            GameObject scrollPrefab = Resources.Load<GameObject>("Combat2/CardPrefabs/GhostCard");
            GameObject scrollClone = (GameObject)Instantiate(scrollPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            scrollClone.transform.SetParent(GameObject.Find("GhostPanel2").transform);
            scrollClone.transform.localScale = new Vector3(1, 1, 1);
            Vector3 tempPosi = scrollClone.transform.localPosition;
            tempPosi.z = 0;
            scrollClone.transform.localPosition = tempPosi;

        }

    }

    public IEnumerator DrawXCards2(int scrollsToDraw, float initDelay)
    {

        cardsInHandWhenDrawing = 0;
        yield return new WaitForSeconds(initDelay);

        for (int i = 0; i < scrollsToDraw; i++)
        {
            if (EventManager2.CardsDeck.Count != 0)
            {
                // FIRST Move Rest to Most Right
                if (!movedAllToRight)
                {
                    movedAllToRight = true;
                    MoveAllToMostLeft(0.05f);
                }

                //Get the first Index in DeckCounter(0) -> from Deck To Hand

                GameObject drawnCard = GameObject.Find("DeckCounterIn").transform.GetChild(0).GetChild(0).gameObject;


                drawnCard.transform.SetParent(GameObject.FindGameObjectWithTag("HandPanel").transform, false);
                drawnCard.transform.SetAsLastSibling();
                drawnCard.transform.position = GameObject.Find("GhostPanel").transform.GetChild(0).transform.position;
                EventManager2.TransferCardFromDeckToHand();



                //    Size Up , move to first, than to target
                StartCoroutine(drawnCard.GetComponent<Card>().SizeUpThisCard((1) * 0.1f, 0.01f, 0.08f, 0.08f, 0.8f));
                //   int positionNumber = GameObject.Find("GhostPanel2").transform.childCount - 1;
                StartCoroutine(drawnCard.GetComponent<Card>().MoveThisToSlot(GameObject.FindGameObjectWithTag("HandPanel").transform.childCount - 1, 0.1f, 0.00001f, 50f));

                //Update Card Coutner
                StartCoroutine(GameObject.Find("DeckCounterIn").transform.GetChild(0).GetComponent<UpdateDeckCounter>().UpdateCounter(0.3f));
                StartCoroutine(GameObject.Find("DeckCounterOut").transform.GetChild(0).GetComponent<UpdateSideDeckCounter>().UpdateCounter(0.3f));

                //Update Card Description
                drawnCard.GetComponent<DescriptionText>().UpdateText();

                //Turn All Dropped off
                drawnCard.GetComponent<Card>().allDropped = false;

                //Change Color
                ChangeColor(drawnCard);
            }
        }
    }
    // public IEnumerator DrawXCards(int scrollsToDraw, float initDelay, float delay)
    // {
    //     if (EventManager2.CardsDeck.Count == 0)
    //     {
    //         initDelay += 1.24f;
    //         movedAllToRight = true;
    //         //MoveAllToMostRight(0.05f);
    //     }
    //     if (EventManager2.scrollsInHand + scrollsToDraw > EventManager2.maxCardsInHand)
    //     {
    //         scrollsToDraw = EventManager2.maxCardsInHand - EventManager2.scrollsInHand;
    //     }

    //     yield return new WaitForSeconds(initDelay);

    //     if (scrollsToDraw > 0)
    //     {
    //         //FIRST Move Rest to Most Right
    //         if (!movedAllToRight)
    //         {
    //             movedAllToRight = true;
    //             MoveAllToMostRight(0.05f);
    //             MoveAllToMostRight(0.2f);
    //         }

    //         //Get the first Index in DeckCounter(0) -> from Deck To Hand
    //         GameObject drawnCard = GameObject.FindGameObjectWithTag("DeckCounter").transform.GetChild(0).GetChild(0).gameObject;
    //         drawnCard.transform.SetParent(GameObject.FindGameObjectWithTag("HandPanel").transform, false);
    //         drawnCard.transform.SetAsFirstSibling();
    //         drawnCard.transform.position = GameObject.Find("GhostPanel").transform.GetChild(0).transform.position;
    //         EventManager2.TransferCardFromDeckToHand();

    //         //Add Component & Transfer from Deck To Hand
    //         scrollsToDraw--;
    //         EventManager2.scrollsInHand++;
    //         //Size Up , move to first, than to target
    //         StartCoroutine(drawnCard.GetComponent<Card>().SizeUpThisCard((EventManager2.maxCardsInHand - scrollsToDraw) * 0.1f, 0.01f, 0.08f, 0.08f,0.7f));
    //         StartCoroutine(drawnCard.GetComponent<Card>().MoveThisToSlot(EventManager2.maxCardsInHand - EventManager2.scrollsInHand, (EventManager2.maxCardsInHand - scrollsToDraw) * 0.1f, 0.001f, 20f));

    //         //Update Card Coutner
    //         StartCoroutine(GameObject.FindGameObjectWithTag("DeckCounter").transform.GetChild(0).GetComponent<UpdateDeckCounter>().UpdateCounter(0.3f));
    //         StartCoroutine(GameObject.FindGameObjectWithTag("DeckCounter").transform.GetChild(1).GetComponent<UpdateSideDeckCounter>().UpdateCounter(0.3f));

    //         if (EventManager2.CardsDeck.Count == 0)
    //         {
    //             StartCoroutine(DrawXCards(scrollsToDraw, 0.5f, delay));
    //         }
    //         else
    //         {
    //             StartCoroutine(DrawXCards(scrollsToDraw, delay, delay));
    //         }
    //     }
    //     EventManager2.UpdateCardText();
    // }

    void MoveAllToMostLeft(float delay)
    {
        // int counter = EventManager2.maxCardsInHand - 1;
        cardsInHandWhenDrawing = GameObject.FindGameObjectWithTag("HandPanel").transform.childCount;
        for (int i = 0; i < cardsInHandWhenDrawing; i++)
        {
            StartCoroutine(GameObject.FindGameObjectWithTag("HandPanel").transform.GetChild(i).GetComponent<Card>().MoveThisToSlot(i, delay, 0.0001f, 15f));

        }
    }

}
