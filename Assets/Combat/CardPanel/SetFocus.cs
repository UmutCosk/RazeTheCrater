﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SetFocus : MonoBehaviour
{
    float focusCharge;

    // Use this for initialization
    void Start()
    {
        focusCharge = 0;
        UpdateFocus();
    }

    // Update is called once per frame
    public void UpdateFocus()
    {
        int curFocus = GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curFocus;
        this.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = curFocus.ToString();
    }

    public void ReduceFocus(int focusToReduce)
    {
        GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curFocus -= focusToReduce;
        UpdateFocus();
    }

    public void AddCharge(int focusToAdd)
    {
        GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curFocus += focusToAdd;
        UpdateFocus();

    }
    /*   public void ResetFocus()
      {
          GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy = EventManager2.startEnergy;
          UpdateEnergy();
      } */

    public void AddFocusCharge()
    {
        /*    focusCharge++;
           this.transform.GetChild(0).GetComponent<Image>().fillAmount = focusCharge / 2;

           if (focusCharge == 2)
           { */
        AddCharge(1);
        /*        focusCharge = 0;
               this.transform.GetChild(0).GetComponent<Image>().fillAmount = 0;
           } */

    }
}
