﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;


public class SetChosenCard : MonoBehaviour, IPointerClickHandler
{




    void Start()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {

        EventManager2.chosenCard = this.gameObject;

        if (EventManager2.chosenCard.gameObject.GetComponent<Skill>().targetType == TargetStyle.None)
        {
            GameObject.Find("UseCardPanel").GetComponent<Image>().raycastTarget = true;
            if (EventManager2.chosenCard.GetComponent<Skill>().skillCategory == SkillCategory.Sniper || EventManager2.chosenCard.GetComponent<Skill>().skillCategory == SkillCategory.Quick)
            {
                EventManager2.sniperCard = this.gameObject;
            }
            else
            {
                EventManager2.sniperCard = null;
            }

        }
        else
        {
            GameObject.Find("UseCardPanel").GetComponent<Image>().raycastTarget = false;
        }
    }



}
