﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpdateSideDeckCounter : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public IEnumerator UpdateCounter(float delay)
    {
        yield return new WaitForSeconds(delay);
        this.GetComponent<TextMeshProUGUI>().text = this.gameObject.transform.childCount.ToString();
    }

}
