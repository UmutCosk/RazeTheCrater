﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SetEnergy : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        UpdateEnergy();
    }

    // Update is called once per frame
    public void UpdateEnergy()
    {
        int curEnergy = GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy;
        this.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = curEnergy.ToString();
    }

    public void ReduceEnergy(int energyToReduce)
    {
        GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy -= energyToReduce;
        UpdateEnergy();
    }

    public void AddEnergy(int energyToAdd)
    {
        GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy += energyToAdd;
        UpdateEnergy();

    }

    public void ResetEnergy()
    {
        GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy = EventManager2.startEnergy;
        UpdateEnergy();
    }
}
