﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;



public class SniperCheck : MonoBehaviour, IPointerClickHandler
{
    public bool activeGlow = false;
    bool useSniper = false;
    public SniperTarget sniperTarget;

    public void OnPointerClick(PointerEventData eventData)
    {

        if (activeGlow && EventManager2.sniperCard != null)
        {
            if (EventManager2.sniperMode == SniperMode.Ricochet)
            {
                activeGlow = false;
                if (this.gameObject.GetComponent<SniperCheck>().sniperTarget == SniperTarget.Throw)
                {
                    useSniper = true;
                }
                if (this.gameObject.GetComponent<SniperCheck>().sniperTarget == SniperTarget.Enemy)
                {
                    useSniper = true;
                }

            }
            else if (EventManager2.sniperMode == SniperMode.Burst)
            {
                if (this.gameObject.GetComponent<SniperCheck>().sniperTarget == SniperTarget.Standard)
                {
                    activeGlow = false;
                    this.transform.GetChild(1).gameObject.SetActive(false);
                }
                if (this.gameObject.GetComponent<SniperCheck>().sniperTarget == SniperTarget.Throw)
                {
                    activeGlow = false;
                    this.transform.GetChild(1).gameObject.SetActive(false);
                }
            }

            EventManager2.sniperTargets.Add(this.gameObject);

            CheckSniperFinish();

        }

    }

    void Start()
    {
        if (this.gameObject.tag == "Enemys")
        {
            activeGlow = true;
        }
    }



    // Update is called once per frame
    void Update()
    {

        if (GameObject.Find("SlowMotion").GetComponent<SlowMotion>().sniper)
        {

            if (activeGlow)
            {
                this.transform.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                this.transform.GetChild(1).gameObject.SetActive(false);
            }
        }
        else
        {
            this.transform.GetChild(1).gameObject.SetActive(false);
        }
    }

    void CheckSniperFinish()
    {
        if (useSniper)
        {
            Debug.Log("test");
            CloseGlow();
            EventManager2.sniperCard.GetComponent<Skill>().UseSniper();
            useSniper = false;
        }
        else
        {
            if (EventManager2.burstSniper > 0)
            {
                EventManager2.burstSniper--;
                if (EventManager2.burstSniper == 0)
                {
                    CloseGlow();
                    EventManager2.sniperCard.GetComponent<Skill>().UseSniper();
                }
            }
            else if (EventManager2.ricochetSniper > 0)
            {
                EventManager2.ricochetSniper--;
                if (EventManager2.ricochetSniper == 0)
                {
                    CloseGlow();
                    EventManager2.sniperCard.GetComponent<Skill>().UseSniper();
                }
            }
        }
        //CheckGlowLeft();

    }

    public void SniperHit()
    {

        if (sniperTarget == SniperTarget.Standard && EventManager2.sniperCard.GetComponent<Skill>().skillCategory == SkillCategory.Sniper)
        {
            this.GetComponent<Skill>().SnipedEffect();
        }
        else if (sniperTarget == SniperTarget.Standard && EventManager2.sniperCard.GetComponent<Skill>().skillCategory == SkillCategory.Quick)
        {
            EventManager2.sniperCard.GetComponent<Skill>().QuickEffect(this.gameObject);
        }
        if (sniperTarget == SniperTarget.Throw)
        {
            this.GetComponent<Skill>().SnipedEffect();
            if (EventManager2.sniperMode == SniperMode.Ricochet)
            {
                GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOff();
                foreach (GameObject sniper in GameObject.FindGameObjectsWithTag("Snipers"))
                {
                    Destroy(sniper);
                }
            }
        }
        if (sniperTarget == SniperTarget.Enemy)
        {
            EventManager2.sniperCard.GetComponent<Skill>().HitEffect(GameObject.FindGameObjectWithTag("Gamer").gameObject, this.gameObject);
        }
    }

    void CloseGlow()
    {

        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Shots"))
        {
            glowObject.GetComponent<SniperCheck>().activeGlow = false;
            glowObject.transform.GetChild(1).gameObject.SetActive(false);
        }
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Throws"))
        {
            glowObject.GetComponent<SniperCheck>().activeGlow = false;
            glowObject.transform.GetChild(1).gameObject.SetActive(false);

        }
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Enemys"))
        {
            glowObject.GetComponent<SniperCheck>().activeGlow = false;
            glowObject.transform.GetChild(1).gameObject.SetActive(false);

        }
    }

    void CheckGlowLeft()
    {
        bool atLeastOneGlow = false;
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Shots"))
        {
            if (glowObject.GetComponent<SniperCheck>().activeGlow)
            {
                atLeastOneGlow = true;
                break;
            }
        }
        if (!atLeastOneGlow)
        {
            foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Throws"))
            {
                if (glowObject.GetComponent<SniperCheck>().activeGlow)
                {
                    atLeastOneGlow = true;
                    break;
                }
            }
        }
        if (!atLeastOneGlow)
        {
            foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Enemys"))
            {
                if (glowObject.GetComponent<SniperCheck>().activeGlow)
                {
                    atLeastOneGlow = true;
                    break;
                }
            }
        }
        if (!atLeastOneGlow)
        {
            EventManager2.sniperCard.GetComponent<Skill>().UseSniper();
        }

    }
}
