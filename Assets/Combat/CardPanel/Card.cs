﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;


public class Card : MonoBehaviour, IPointerClickHandler
{
    bool alreadyDropped = false;
    bool gotTofirstPosi = false;
    public bool allDropped = false;

    public void OnPointerClick(PointerEventData eventData)
    {



    }


    public IEnumerator PutDroppedCardToSideDeck(float delay)
    {

        yield return new WaitForSeconds(delay);
        GameObject scroll = this.gameObject;
        scroll.GetComponent<Rigidbody2D>().gravityScale = 0f;
        scroll.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        scroll.transform.localScale = new Vector3(0, 0, 0);
        scroll.transform.position = GameObject.Find("DeckCounterOut").transform.GetChild(0).transform.position;
        alreadyDropped = false;
        gotTofirstPosi = false;

    }
    public IEnumerator PutDroppedCardToDeck(float delay)
    {

        yield return new WaitForSeconds(delay);
        GameObject scroll = this.gameObject;
        scroll.GetComponent<Rigidbody2D>().gravityScale = 0f;
        scroll.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        scroll.transform.localScale = new Vector3(0, 0, 0);
        scroll.transform.position = GameObject.Find("DeckCounterIn").transform.GetChild(0).transform.position;
        alreadyDropped = false;
        gotTofirstPosi = false;

    }


    public void DropThisCard()
    {
        //First use of Card:
        if (EventManager2.FightingPhase == false && allDropped == false)
        {
            EventManager2.FightingPhase = true;
            GameObject.FindGameObjectWithTag("DeckCounter").transform.GetChild(1).GetComponent<DeckClockCounter>().StartCooldown();
        }

        if (!alreadyDropped)
        {
            alreadyDropped = true;
            EventManager2.TransferCardFromHandToSideDeck(this.gameObject);
            // StartCoroutine(EventManager2.UpdateTextFromHandToSideDeck(0.25f));
            this.GetComponent<Rigidbody2D>().gravityScale = 4f;
            this.transform.SetParent(GameObject.Find("DeckCounterOut").transform.GetChild(0).transform);
            this.transform.SetAsFirstSibling();
            StartCoroutine(PutDroppedCardToSideDeck(1.5f));
            //Update Card Coutner
            StartCoroutine(GameObject.Find("DeckCounterIn").transform.GetChild(0).GetComponent<UpdateDeckCounter>().UpdateCounter(0));
            StartCoroutine(GameObject.Find("DeckCounterOut").transform.GetChild(0).GetComponent<UpdateSideDeckCounter>().UpdateCounter(0));
        }
    }

    public IEnumerator MoveThisToSlot(int targetSlotNr, float delay, float functionSpeed, float clockSpeed)
    {
        yield return new WaitForSeconds(delay);
        Vector3 targetPosition2 = GameObject.Find("GhostPanel2").transform.GetChild(targetSlotNr).transform.position;
        if (!gotTofirstPosi)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, GameObject.Find("GhostPanel2").transform.GetChild(0).transform.position, clockSpeed * Time.deltaTime);
        }
        else
        {

            this.transform.position = Vector3.MoveTowards(this.transform.position, targetPosition2, clockSpeed * Time.deltaTime);
        }
        if (!gotTofirstPosi && !(this.transform.position.x < GameObject.Find("GhostPanel2").transform.GetChild(0).transform.position.x + 0.01 && this.transform.position.x > GameObject.Find("GhostPanel2").transform.GetChild(0).transform.position.x - 0.01))
        {
            StartCoroutine(MoveThisToSlot(targetSlotNr, functionSpeed, functionSpeed, clockSpeed));

        }
        else if (gotTofirstPosi == false)
        {
            gotTofirstPosi = true;
        }

        if (gotTofirstPosi && !(this.transform.position.x < targetPosition2.x + 0.01 && this.transform.position.x > targetPosition2.x - 0.01))
        {

            StartCoroutine(MoveThisToSlot(targetSlotNr, functionSpeed, functionSpeed, clockSpeed));
        }

    }

    public IEnumerator SizeUpThisCard(float delay, float timer, float deltasizex, float deltasizey, float maxSize)
    {
        yield return new WaitForSeconds(delay);

        if (this.gameObject.transform.localScale.x < maxSize)
        {
            Vector3 newScale = this.gameObject.transform.localScale;
            newScale.x += deltasizex;
            newScale.y += deltasizey;
            this.gameObject.transform.localScale = newScale;
            StartCoroutine(SizeUpThisCard(timer, timer, deltasizex, deltasizey, maxSize));
        }
        else if (this.gameObject.transform.localScale.x > maxSize)
        {
            Vector3 newScale = this.gameObject.transform.localScale;
            newScale.x = maxSize;
            newScale.y = maxSize;
            this.gameObject.transform.localScale = newScale;
        }


    }








}
