﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SetHP : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        UpdateHP();
    }

    public void UpdateHP()
    {
        float curHP = this.transform.parent.parent.GetComponent<BasePlayer>().curHP;
        float maxHP = this.transform.parent.parent.GetComponent<BasePlayer>().maxHP;
        float hpQuotient = curHP / maxHP;

        this.transform.GetChild(0).GetComponent<Image>().fillAmount = hpQuotient;
        this.transform.GetChild(0).GetChild(0).GetComponent<Image>().fillAmount = hpQuotient;
        this.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = curHP + " / " + maxHP;

    }
}
