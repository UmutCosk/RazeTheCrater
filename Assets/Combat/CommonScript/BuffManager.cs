﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuffManager : MonoBehaviour
{

    bool buffAlreadyExists;
    int buffAmount;
    GameObject buffPrefab;
    BuffsDatabase buffDB;
    void Start()
    {
        buffPrefab = Resources.Load<GameObject>("Buffs/Buff");
        buffDB = GameObject.Find("Database").GetComponent<BuffsDatabase>();
    }
    public void UpdateBuff(BuffType bufftype)
    {
        buffAmount = GetAmountOfBuff(bufftype);
        buffAlreadyExists = false;
        foreach (Transform searchingBuff in this.transform)
        {
            if (searchingBuff.GetComponent<Buff>().buffType == bufftype) //if already in there?
            {
                if (buffAmount > 0) //Update Text
                {
                    searchingBuff.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = buffAmount.ToString();
                }
                else //Destroy Prefab
                {
                    Destroy(searchingBuff.gameObject);
                }

                buffAlreadyExists = true;
                break;
            }

        }

        if (!buffAlreadyExists && buffAmount > 0) //Instantiated Prefab +AddComponent & Update Text 
        {


            GameObject buff_clone = Instantiate(buffPrefab);
            buff_clone.transform.SetParent(this.gameObject.transform);
            buff_clone.transform.localScale = new Vector3(1, 1, 1);
            Vector3 tempVector = buff_clone.transform.localPosition;
            tempVector.z = 0;
            buff_clone.transform.localPosition = tempVector;
            int id = buffDB.GetBuffIDByBuffType(bufftype);
            //AddComponent
            buff_clone = EventManager2.AddGameComponent(buff_clone, id, "Buff");
            //Update Text
            buff_clone.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = buffAmount.ToString();
        }



    }





    int GetAmountOfBuff(BuffType bufftype)
    {
        int amount = 0;
        switch (bufftype)
        {
            case BuffType.Armor:
                amount = this.transform.parent.parent.GetComponent<BasePlayer>().armor;
                break;
            case BuffType.Power:
                amount = this.transform.parent.parent.GetComponent<BasePlayer>().power;
                break;
            case BuffType.Toughness:
                amount = this.transform.parent.parent.GetComponent<BasePlayer>().toughness;
                break;
            case BuffType.Dodge:
                amount = this.transform.parent.parent.GetComponent<BasePlayer>().dodge;
                break;
        }
        return amount;
    }



}
