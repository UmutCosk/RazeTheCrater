﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SetArmor : MonoBehaviour
{


    public void UpdateArmor()
    {

        int armor = this.transform.parent.parent.GetComponent<BasePlayer>().armor;
        if (armor == 0)
        {
            this.transform.GetChild(1).gameObject.SetActive(false);
        }
        this.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = armor.ToString();
    }
}
