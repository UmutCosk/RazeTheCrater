﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class SpawnFamiliar : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("z"))
        {
            if (EventManager2.familiarSlots < 3)
            {
                EventManager2.familiarSlots++;
                if (EventManager2.familiarSlots == 1)
                {
                    PutInGhostSlots();
                    StartCoroutine(SpawnFamiliarWithEffect("Familiar1"));
                }
                else
                {
                    PutInGhostSlots();
                    StartCoroutine(MoveFamiliarAside());
                    StartCoroutine(SpawnFamiliarWithEffect("Familiar1"));
                }
            }
        }
        if (Input.GetKeyDown("9"))
        {
            if (EventManager2.familiarSlots > 0)
            {
                EventManager2.familiarSlots--;
                RemoveGhostSlots(0);
            }

        }
        if (Input.GetKeyDown("0"))
        {
            if (EventManager2.familiarSlots > 0)
            {
                EventManager2.familiarSlots--;
                RemoveGhostSlots(1);
            }

        }
    }
    void RemoveGhostSlots(int index)
    {

        Destroy(GameObject.Find("FamiliarGhost").transform.GetChild(index).gameObject);
        Destroy(GameObject.FindGameObjectWithTag("Familiar").transform.GetChild(index).gameObject);
        if (EventManager2.familiarSlots != 0)
        {
            StartCoroutine(MoveFamiliarBack());
        }
    }

    void PutInGhostSlots()
    {
        GameObject familiarPrefab = Resources.Load<GameObject>("Combat2/FamiliarPrefab/Familiar1");
        GameObject familiarClone = (GameObject)Instantiate(familiarPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        familiarClone.transform.SetParent(GameObject.Find("FamiliarGhost").transform, false);
        familiarClone.transform.GetComponent<Image>().enabled = false;
        foreach (Transform child in familiarClone.gameObject.transform)
        {
            Destroy(child.gameObject);
        }
        familiarClone.transform.SetAsFirstSibling();
    }
    IEnumerator SpawnFamiliarWithEffect(string scriptName)
    {
        yield return new WaitForSeconds(0.5f);
        GameObject familiarPrefab = Resources.Load<GameObject>("Combat2/FamiliarPrefab/Familiar1");
        GameObject familiarClone = (GameObject)Instantiate(familiarPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        familiarClone.transform.SetParent(GameObject.FindGameObjectWithTag("Familiar").transform, false);
        familiarClone = EventManager2.AddGameComponent(familiarClone,0,"Familiar");
        familiarClone.transform.SetAsFirstSibling();
        familiarClone.transform.position = GameObject.Find("FamiliarGhost").transform.GetChild(0).transform.position;
        Color familiarColor = new Color();
        familiarColor = familiarClone.transform.GetComponent<Image>().color;
        familiarColor.a = 0;
        familiarClone.transform.GetComponent<Image>().color = familiarColor;
        familiarClone.GetComponent<LayoutElement>().ignoreLayout = true;
        familiarClone.GetComponent<Familiar>().UpdateFamiliarIntention();
        StartCoroutine(RaisingUpAlpha(familiarClone));

    }
    IEnumerator RaisingUpAlpha(GameObject familiarClone)
    {
        yield return new WaitForSeconds(0.01f);
        Color familiarColor = new Color();
        familiarColor = familiarClone.transform.GetComponent<Image>().color;
        familiarColor.a = familiarColor.a + 0.01f;
        familiarClone.transform.GetComponent<Image>().color = familiarColor;
        if (familiarColor.a < 1)
        {
            StartCoroutine(RaisingUpAlpha(familiarClone));
        }
    }

    IEnumerator MoveFamiliarAside()
    {
        yield return new WaitForSeconds(0.1f);
        Vector3 targetPosi = GameObject.Find("FamiliarGhost").transform.GetChild(1).position;
        StartCoroutine(GameObject.FindGameObjectWithTag("Familiar").transform.GetChild(0).GetComponent<MoveToSpot>().MoveThisToPosition(targetPosi, 0.1f, 0.001f, 3));
    }
    IEnumerator MoveFamiliarBack()
    {
        yield return new WaitForSeconds(0.1f);
        Vector3 targetPosi = GameObject.Find("FamiliarGhost").transform.GetChild(0).position;
        StartCoroutine(GameObject.FindGameObjectWithTag("Familiar").transform.GetChild(0).GetComponent<MoveToSpot>().MoveThisToPosition(targetPosi, 0.1f, 0.001f, 3));
    }


}
