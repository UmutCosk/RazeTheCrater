﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DescriptionText : MonoBehaviour
{

    // Use this for initialization
    public void UpdateText()
    {
        StartCoroutine(UpdateTextDelay(this.gameObject, 0.01f));
    }
    public static IEnumerator UpdateTextDelay(GameObject card, float delay)
    {
        yield return new WaitForSeconds(delay);
        card.transform.GetChild(0).GetChild(2).transform.GetComponent<TextMeshProUGUI>().text = card.GetComponent<Skill>().description;

    }


}
