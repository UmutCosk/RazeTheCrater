﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LaserScript : MonoBehaviour
{

    public GameObject firePoint;
    public LineRenderer lineRender;
    public GameObject sniperText;

    // Update is called once per frame
    void Update()
    {

        var mousePos = Input.mousePosition;
        mousePos.z = 1f;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        lineRender.SetPosition(1, mousePos);
        if (GameObject.Find("SlowMotion").GetComponent<SlowMotion>().sniper)
        {
            sniperText.SetActive(true);
            var sniperTextPos = mousePos;
            sniperTextPos += new Vector3(0.35f, 0.35f, 0);
            sniperText.transform.position = sniperTextPos;
            if (EventManager2.ricochetSniper > 0)
            {
                sniperText.GetComponent<TextMeshProUGUI>().text = EventManager2.ricochetSniper.ToString();
            }
            else if (EventManager2.burstSniper > 0)
            {
                sniperText.GetComponent<TextMeshProUGUI>().text = EventManager2.burstSniper.ToString();
            }
            else
            {
                sniperText.SetActive(false);
            }

        }
        else
        {
            sniperText.SetActive(false);
        }

        /* 
                float distance = Vector3.Distance(firePoint.transform.position, mousePos);
                Vector3 dir = mousePos - firePoint.transform.position;
                float angleZ = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

         */

    }
}
