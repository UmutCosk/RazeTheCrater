﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Timers;

public class FocusCounter : MonoBehaviour
{

    Image timerBar;
    float cooldown = 12;
    float focusTime;
    bool startTimer;


    void Start()
    {
        startTimer = false;
        timerBar = this.GetComponent<Image>();
    }

    public void Update()
    {
        if (startTimer)
        {

            focusTime += Time.unscaledDeltaTime / cooldown;
            timerBar.fillAmount = focusTime;
            if (focusTime >= 1)
            {

                GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOff();
                GameObject.Find("SlowMotion").GetComponent<SlowMotion>().toggle = true;
                GameObject.Find("SlowMotion").GetComponent<SlowMotion>().sniper = false;
                ResetCooldown();
            }
        }
    }

    public void StopTimer()
    {
        startTimer = false;
    }

    public void StartTimer()
    {
        startTimer = true;
    }


    public void ResetCooldown()
    {
        focusTime = 0;
        timerBar.fillAmount = 0;
        StopTimer();

    }
}
