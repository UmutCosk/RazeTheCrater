﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;

public class ConsumableShowUse : MonoBehaviour, IPointerClickHandler
{

    public void OnPointerClick(PointerEventData eventData)
    {
        foreach (Transform child in GameObject.Find("ConsumablePanel").transform)
        {
            child.GetChild(1).gameObject.SetActive(false);
        }
        if (this.gameObject.name != "Consumable")
        {
            this.gameObject.transform.parent.GetChild(1).gameObject.SetActive(true);
        }


    }
}
