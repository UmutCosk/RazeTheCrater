﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;

public class SelectConsumable : MonoBehaviour, IPointerClickHandler
{

    public void OnPointerClick(PointerEventData eventData)
    {
        foreach (Transform child in GameObject.Find("ConsumablePanel").transform)
        {
            child.GetChild(1).gameObject.SetActive(false);
        }
        EventManager2.chosenConsumable = this.gameObject.transform.parent.parent.GetChild(0).gameObject;

        if (EventManager2.chosenConsumable.GetComponent<Consumable>().targetType == TargetStyle.None)
        {
            EventManager2.chosenConsumable.GetComponent<Consumable>().FruitEffect(GameObject.FindGameObjectWithTag("Gamer").gameObject);
        }

    }
}
