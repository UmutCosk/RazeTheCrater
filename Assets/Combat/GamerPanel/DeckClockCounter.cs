﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Timers;

public class DeckClockCounter : MonoBehaviour
{

    Image timerBar;
    float cooldown;
    float interval = 0.05f;
    float timeLeft;
    void Start()
    {
        cooldown = EventManager2.roundTimer;
        timerBar = this.GetComponent<Image>();
        timeLeft = cooldown;
        TimersManager.SetLoopableTimer(this, interval, CountDown);
        StopCooldown();
    }

    public void StartCooldown()
    {
        TimersManager.SetPaused(CountDown, false);
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemys"))
        {
            enemy.GetComponent<Enemy>().AllowAttack();
        }
    }

    public void StopCooldown()
    {
        TimersManager.SetPaused(CountDown, true);
        EventManager2.FightingPhase = false;
    }

    void CountDown()
    {
        timeLeft -= interval;
        float newAmount = 1 - timeLeft / cooldown;
        timerBar.fillAmount = newAmount;

        if (timeLeft <= 0)
        {
            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemys"))
            {
                enemy.GetComponent<Enemy>().StopAttack();
            }
            ResetCooldown();
            DropAllCardsAndDraw();
            GameObject.FindGameObjectWithTag("Energy").GetComponent<SetEnergy>().ResetEnergy();
            GameObject.FindGameObjectWithTag("Focus").GetComponent<SetFocus>().AddFocusCharge();
            StopCooldown();

        }


    }

    void ResetCooldown()
    {
        timeLeft = cooldown;
        timerBar.fillAmount = 1 - timeLeft / cooldown;

    }

    void DropAllCardsAndDraw()
    {
        StartCoroutine(GameObject.FindGameObjectWithTag("HandPanel").GetComponent<CardMechanics>().DropAll(0.1f));
        StartCoroutine(GameObject.FindGameObjectWithTag("HandPanel").GetComponent<CardMechanics>().DrawXCards2(EventManager2.maxCardsInHand, 0.5f));

        ReduceAllStackBuffs();
    }

    void ReduceAllStackBuffs()
    {
        GameObject.FindGameObjectWithTag("Gamer").GetComponent<BasePlayer>().ReduceAllBuffs();
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemys"))
        {
            enemy.GetComponent<BasePlayer>().ReduceAllBuffs();
        }
    }
}
