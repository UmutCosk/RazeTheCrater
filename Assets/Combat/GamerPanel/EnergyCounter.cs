﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Timers;

public class EnergyCounter : MonoBehaviour
{

    Image timerBar;
    float cooldown = 6f;
    float interval = 0.05f;
    float timeLeft;
    void Start()
    {
        /*      timerBar = this.GetComponent<Image>();
             timeLeft = cooldown;
             TimersManager.SetLoopableTimer(this, interval, CountDown);
      */

    }


    void CountDown()
    {
        timeLeft -= interval;
        timerBar.fillAmount = 1 - timeLeft / cooldown;
        if (timeLeft <= 0)
        {
            ResetCooldown();
        }


    }

    void ResetCooldown()
    {
        this.transform.parent.GetComponent<SetEnergy>().AddEnergy(1);
        timeLeft = cooldown;
        timerBar.fillAmount = 1 - timeLeft / cooldown;
    }


}
