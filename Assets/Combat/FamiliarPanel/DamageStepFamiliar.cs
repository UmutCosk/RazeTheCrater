﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageStepFamiliar : MonoBehaviour {

	 public static void FamiliarAttack(GameObject familiar)
    {
        familiar.GetComponent<Familiar>().FamiliarAttack();
    }

}
