﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageStep : MonoBehaviour
{

    public static void UseCard(GameObject target)
    {

        if (GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy >= EventManager2.chosenCard.GetComponent<Skill>().energyCost)
        {
            //Energy Update
            GameObject.FindGameObjectWithTag("Energy").transform.GetComponent<SetEnergy>().ReduceEnergy(EventManager2.chosenCard.GetComponent<Skill>().energyCost);

            //Card Usage
            if (EventManager2.chosenCard != null)
            {
                EventManager2.tempChosenCard = EventManager2.chosenCard;
            }
            switch (EventManager2.tempChosenCard.GetComponent<Skill>().skillCategory)
            {
                case SkillCategory.Standard:
                    EventManager2.tempChosenCard.GetComponent<Skill>().UseCard(GameObject.FindGameObjectWithTag("Gamer").gameObject, target);
                    break;
                case SkillCategory.Throw:
                    EventManager2.tempChosenCard.GetComponent<Skill>().UseCard(GameObject.FindGameObjectWithTag("Gamer").gameObject, target);
                    break;
                case SkillCategory.Sniper:
                    EventManager2.tempChosenCard.GetComponent<Skill>().UseCard(GameObject.FindGameObjectWithTag("Gamer").gameObject, target);
                    break;
                case SkillCategory.Drug:
                    break;
                case SkillCategory.Quick:
                    break;
            }
            //Card Mechanics
            EventManager2.chosenCard.GetComponent<Card>().DropThisCard();



            EventManager2.chosenCard = null;
        }
    }

    public static void UseConsumableOnEnemy()
    {
        //Card Usage
        EventManager2.tempChosenConsumable = EventManager2.chosenConsumable;
        EventManager2.tempChosenConsumable.GetComponent<Consumable>().FruitEffect(EventManager2.chosenEnemy);


        EventManager2.chosenConsumable = null;
    }
}

