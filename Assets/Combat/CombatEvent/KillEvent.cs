﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillEvent : MonoBehaviour
{


    public static void OnKillEventEnemy(GameObject killedEnemy)
    {
        //Set Deathrattle


        //One Less Enemy To Kill
        EventManager2.enemysToKill--;

        DestroyEnemy(killedEnemy);


    }

    public static void OnKillEventFamiliar(GameObject killedFamiliar)
    {
        //Set Deathrattle


        //One Less Enemy To Kill
        EventManager2.familiarSlots--;

        DestroyFamiliar(killedFamiliar);


    }

    static void DestroyEnemy(GameObject enemy)
    {
        Destroy(enemy.gameObject);
    }

    static void DestroyFamiliar(GameObject familiar)
    {
        Destroy(GameObject.FindGameObjectWithTag("Familiar").transform.GetChild(familiar.transform.GetSiblingIndex()).gameObject);
    }



}
