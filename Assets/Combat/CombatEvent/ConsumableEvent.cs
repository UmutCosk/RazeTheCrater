﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsumableEvent : MonoBehaviour
{

    public static void DiscardConsumable()
    {
        PlayerPrefSerialize.DiscardConsumable(EventManager2.chosenConsumable.transform.parent.GetSiblingIndex());
    }
}
