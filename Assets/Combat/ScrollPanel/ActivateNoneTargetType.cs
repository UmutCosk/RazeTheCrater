﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;

public class ActivateNoneTargetType : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (EventManager2.sniperCard != null && GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy >= EventManager2.sniperCard.GetComponent<Skill>().energyCost)
            {
                //Energy Update
                GameObject.FindGameObjectWithTag("Energy").transform.GetComponent<SetEnergy>().ReduceEnergy(EventManager2.sniperCard.GetComponent<Skill>().energyCost);


                EventManager2.sniperCard.GetComponent<Skill>().UseFocus(GameObject.FindGameObjectWithTag("Gamer").gameObject);
                EventManager2.sniperCard.GetComponent<Card>().DropThisCard();
                GameObject.Find("SkillPanel").transform.GetChild(8).gameObject.SetActive(true);
                GameObject.FindGameObjectWithTag("Focus").transform.GetChild(0).GetComponent<FocusCounter>().StartTimer();
                GameObject.Find("SlowMotion").transform.GetComponent<SlowMotion>().sniper = true;
            }
            else if (EventManager2.chosenCard.GetComponent<Skill>().skillCategory == SkillCategory.Protect && GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy >= EventManager2.chosenCard.GetComponent<Skill>().energyCost)
            {
                //Energy Update
                GameObject.FindGameObjectWithTag("Energy").transform.GetComponent<SetEnergy>().ReduceEnergy(EventManager2.chosenCard.GetComponent<Skill>().energyCost);

                EventManager2.chosenCard.GetComponent<Skill>().UseInstantCard(GameObject.FindGameObjectWithTag("Gamer").gameObject);
                EventManager2.chosenCard.GetComponent<Card>().DropThisCard();
            }
            else if (EventManager2.chosenCard.GetComponent<Skill>().skillCategory == SkillCategory.Drug && GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curEnergy >= EventManager2.chosenCard.GetComponent<Skill>().energyCost)
            {
                //Energy Update
                GameObject.FindGameObjectWithTag("Energy").transform.GetComponent<SetEnergy>().ReduceEnergy(EventManager2.chosenCard.GetComponent<Skill>().energyCost);

                EventManager2.chosenCard.GetComponent<Skill>().UseCastCard(GameObject.FindGameObjectWithTag("Gamer").gameObject);
                EventManager2.chosenCard.GetComponent<Card>().DropThisCard();
            }
            this.GetComponent<Image>().raycastTarget = false;
        }
    }
}
