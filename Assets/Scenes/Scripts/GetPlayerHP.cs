﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetPlayerHP : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        UpdateHP();
    }

    public void UpdateHP()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        int maxHP = PlayerPrefSerialize.playerValues.maxHP;
        int curHP = PlayerPrefSerialize.playerValues.curHP;
        this.GetComponent<Text>().text = "HP: " + curHP + " / " + maxHP;
    }
}
