﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillCardSlots : MonoBehaviour
{

    int scrollsToShow = 2;
    private GameObject scrollButton;
    int bronzeChance = 100;
    int silverChance = 0;
    int goldChance = 0;

    int bronzeCounter;
    int silverCounter;
    int goldCounter;

    List<Skill> randomSkill;

    BuyCardOnClick buyCardOnClick;

    SkillsDatabase skillsDB;
    // Use this for initialization
    void Start()
    {

        bronzeCounter = 0;
        silverCounter = 0;
        goldCounter = 0;
        scrollButton = Resources.Load("Combat2/CardPrefabs/CardShop") as GameObject;
        skillsDB = GameObject.Find("Database").GetComponent<SkillsDatabase>();

        for (int i = 0; i < scrollsToShow; i++)
        {
            int checkChance = Random.Range(1, 101);
            if (checkChance <= bronzeChance)
            {
                bronzeCounter++;
            }
            else if (checkChance <= bronzeChance + silverChance)
            {
                silverCounter++;
            }
            else if (checkChance <= bronzeChance + silverChance + goldChance)
            {
                goldCounter++;
            }
        }

        List<Skill> randomSkill = new List<Skill>();

        List<Skill> tempBronzeList = new List<Skill>();
        tempBronzeList = skillsDB.GetRandomBronzeSkills(bronzeCounter);

        List<Skill> tempSilverList = new List<Skill>();
        tempSilverList = skillsDB.GetRandomSilverSkills(silverCounter);

        List<Skill> tempGoldList = new List<Skill>();
        tempGoldList = skillsDB.GetRandomGoldSkills(goldCounter);

        for (int i = 0; i < tempBronzeList.Count; i++)
        {
            randomSkill.Add(tempBronzeList[i]);
        }

        for (int i = 0; i < tempSilverList.Count; i++)
        {
            randomSkill.Add(tempSilverList[i]);
        }

        for (int i = 0; i < tempGoldList.Count; i++)
        {
            randomSkill.Add(tempGoldList[i]);
        }



        if (skillsDB != null)
        {
            for (int i = 0; i < randomSkill.Count; i++)
            {
                //Create Card Buttons
                GameObject scrollButton_clone = (GameObject)Instantiate(scrollButton) as GameObject;
                scrollButton_clone.transform.SetParent(this.transform);
                scrollButton_clone.transform.localScale = new Vector3(1, 1, 1);
                Vector3 tempPosi = scrollButton_clone.transform.localPosition;
                tempPosi.z = 0;
                scrollButton_clone.transform.localPosition = tempPosi;
                EventManager2.AddGameComponent(scrollButton_clone, randomSkill[i].id, "Card");

                //Set Price
                buyCardOnClick = scrollButton_clone.GetComponent<BuyCardOnClick>();
                buyCardOnClick.GetPrice();

                //Ignore Layout
                StartCoroutine(DelayIgnoreLayout(scrollButton_clone));
            }
        }

    }

    IEnumerator DelayIgnoreLayout(GameObject gameobject)
    {
        yield return new WaitForSeconds(0.1f);

        //Ignore Layout
        gameobject.GetComponent<LayoutElement>().ignoreLayout = true;
    }
}
