﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillInkSlot : MonoBehaviour
{


    int inksToShow = 1;
    private GameObject consumableButton;

    BuyConsumableOnClick buyConsumableOnClick;

    ConsumablesDatabase consumableDB;
    // Use this for initialization
    void Start()
    {
        consumableButton = Resources.Load("Consumables/ConsumableShop") as GameObject;
        consumableDB = GameObject.Find("Database").GetComponent<ConsumablesDatabase>();
        if (consumableDB != null)
        {

            List<Consumable> randomInk = consumableDB.GetXConsumablesBySource(inksToShow, ConsumableType.Ink);
            for (int i = 0; i < inksToShow; i++)
            {
                //Create Consumable Button
                GameObject consumableButton_clone = (GameObject)Instantiate(consumableButton) as GameObject;
                consumableButton_clone.transform.SetParent(this.transform);
                consumableButton_clone.transform.localScale = new Vector3(1, 1, 1);
                Vector3 tempPosi = consumableButton_clone.transform.localPosition;
                tempPosi.z = 0;
                consumableButton_clone.transform.localPosition = tempPosi;
                consumableButton_clone.name = consumableDB.GetConsumableByID(randomInk[i].id).scriptName;
                consumableButton_clone.AddComponent(System.Type.GetType(consumableButton_clone.name));
                Consumable consumableComponent = consumableButton_clone.GetComponent(System.Type.GetType(consumableButton_clone.name)) as Consumable;
                consumableDB.SyncValues(consumableComponent, randomInk[i]);
                consumableComponent.ConsumableSlotNR = i;
                consumableButton_clone.GetComponent<Image>().sprite = Resources.Load<Sprite>(consumableComponent.imagePath);

                //Set Price
                buyConsumableOnClick = consumableButton_clone.GetComponent<BuyConsumableOnClick>();
                buyConsumableOnClick.GetPrice();

                //Ignore Layout
                StartCoroutine(DelayIgnoreLayout(consumableButton_clone));
            }
        }
    }


    IEnumerator DelayIgnoreLayout(GameObject gameobject)
    {
        yield return new WaitForSeconds(0.1f);

        //Ignore Layout
        gameobject.GetComponent<LayoutElement>().ignoreLayout = true;
    }

}
