﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class BuyItemOnClick : MonoBehaviour, IPointerClickHandler
{
    int priceMinBronze = 20;
    int priceMaxBronze = 40;
    int priceMinSilver = 50;
    int priceMaxSilver = 70;

    int itemPrice;
    ItemsDatabase itemDB;
    private Item itemComponent;

    public void GetPrice(ItemTier itemTier)
    {
        switch (itemTier)
        {
            case ItemTier.Common:
                itemPrice = Random.Range(priceMinBronze, priceMaxBronze + 1);

                break;
            case ItemTier.Rare:
                itemPrice = Random.Range(priceMinSilver, priceMaxSilver + 1);

                break;
        }
        this.transform.GetChild(0).GetComponent<Text>().text = itemPrice + " G";
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        itemComponent = this.GetComponent(System.Type.GetType(this.name)) as Item;
        PlayerPrefSerialize.LoadPlayerValues();
        int playerGold;

        //Item in Shop clicked
        playerGold = PlayerPrefSerialize.playerValues.Gold;
        if (playerGold >= itemPrice)
        {
            // begleiche rechnung
            PlayerPrefSerialize.PayGold(itemPrice);
            //füge item zum inventory des spielers hinzu
            PlayerPrefSerialize.AddItem(itemComponent.id);

            //Upon PickUp 
            itemComponent.GetComponent<Item>().UponPickUp();

            //Destroy gameObject
            Destroy(this.gameObject);

        }
    }
}
