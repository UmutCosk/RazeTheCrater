﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
public class BuyCardOnClick : MonoBehaviour, IPointerClickHandler
{

    // Use this for initialization
    int priceMinCard = 20;
    int priceMaxCard = 30;
    int scrollPrice;
    Consumable consumableComponent;

    public void GetPrice()
    {

        scrollPrice = Random.Range(priceMinCard, priceMaxCard + 1);

        this.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = scrollPrice + " G";
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        PlayerPrefSerialize.LoadPlayerValues();
        if (PlayerPrefSerialize.PayGold(scrollPrice))
        {
            PlayerPrefSerialize.LoadPlayerValues();
            //destroy gameObject
            Destroy(this.gameObject);
        }
    }

}
