﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class BuyConsumableOnClick : MonoBehaviour, IPointerClickHandler
{
    // Use this for initialization
    int priceMinConsumable = 20;
    int priceMaxConsumable = 30;
    int ConsumablePrice;
    Consumable consumableComponent;

    public void GetPrice()
    {

        ConsumablePrice = Random.Range(priceMinConsumable, priceMaxConsumable + 1);
        foreach (Transform child in GameObject.Find("ItemPanel").transform)
        {
            //   ConsumablePrice = child.GetComponent<Item>().ChangePriceOf(ConsumablePrice, ChangePrice.ShopRabatt);
        }
        this.transform.GetChild(0).GetComponent<Text>().text = ConsumablePrice + " G";
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        PlayerPrefSerialize.LoadPlayerValues();
        if (PlayerPrefSerialize.PayGold(ConsumablePrice))
        {
            PlayerPrefSerialize.LoadPlayerValues();
            PlayerPrefSerialize.AddConsumable(this.GetComponent<Consumable>().id);

            //destroy gameObject
            Destroy(this.gameObject);
        }
    }



}
