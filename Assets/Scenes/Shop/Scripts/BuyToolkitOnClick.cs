﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class BuyToolkitOnClick : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
    // Use this for initialization
    int priceMinToolkit = 30;
    int priceMaxToolkit = 45;
    int ToolkitPrice;
    Consumable consumableComponent;

    public void GetPrice()
    {
        ToolkitPrice = Random.Range(priceMinToolkit, priceMaxToolkit + 1);
        foreach (Transform child in GameObject.Find("ItemPanel").transform)
        {
         //   ToolkitPrice = child.GetComponent<Item>().ChangePriceOf(ToolkitPrice, ChangePrice.ShopRabatt);
        }
        this.transform.GetChild(0).GetComponent<Text>().text = ToolkitPrice + " G";
    }


    public void OnPointerEnter(PointerEventData eventData)
    {

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        EventSystem.current.SetSelectedGameObject(null);

    }
    public void OnPointerClick(PointerEventData eventData)
    {


        consumableComponent = this.GetComponent(System.Type.GetType(this.name)) as Consumable;
        PlayerPrefSerialize.LoadPlayerValues();

        //Toolkit in Shop clicked
        int playerGold = PlayerPrefSerialize.playerValues.Gold;
        if (playerGold >= ToolkitPrice && PlayerPrefSerialize.playerValues.ConsumableCounter < PlayerPrefSerialize.playerValues.maxConsumableSlots)
        {
            // begleiche rechnung
            PlayerPrefSerialize.PayGold(ToolkitPrice);
            PlayerPrefSerialize.SavePlayerValues();
            PlayerPrefSerialize.LoadPlayerValues();

            //füge item zum inventory des spielers hinzu
            for (int i = 0; i < PlayerPrefSerialize.playerValues.maxConsumableSlots; i++)
            {
                if (PlayerPrefSerialize.playerValues.ConsumableSlotNR[i] == 9999)
                {
                    PlayerPrefSerialize.playerValues.ConsumableSlotNR[i] = consumableComponent.id;
                    break;
                }
            }
            PlayerPrefSerialize.playerValues.ConsumableCounter++;
            PlayerPrefSerialize.SavePlayerValues();

            //Update Gold
            GameObject.Find("MiscPanel").transform.GetChild(1).GetComponent<GetGoldAmount>().SetAmount();

            //destroy gameObject
            Destroy(this.gameObject);
        }

    }

}
