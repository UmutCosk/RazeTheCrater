﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillItemSlots : MonoBehaviour
{

    private int itemsToShow = 3;

    private GameObject itemButton;

    ItemsDatabase itemDB;
    BuyItemOnClick buyItemOnClick;

    int tier1chance = 100;
    int tier2chance = 0;
    int maxToDraw;

    List<Item> itemDrawList;
    // Use this for initialization
    public void Start()
    {
        itemDrawList = new List<Item>();
        List<Item> itemTier2List = new List<Item>();
        itemButton = Resources.Load("Items/ItemShop") as GameObject;
        itemDB = GameObject.Find("Database").GetComponent<ItemsDatabase>();

        maxToDraw = itemDB.GetItemAmountByTier(ItemTier.Common) - itemDB.GetPlayerItemAmountByTier(ItemTier.Common);

        if ((itemDB.GetItemAmountByTier(ItemTier.Common) - itemDB.GetPlayerItemAmountByTier(ItemTier.Common)) > 0)
        {
            ItemRandomizer();
        }

        for (int i = 0; i < itemDrawList.Count; i++)
        {
            //Create Item Button
            GameObject itemButton_clone = (GameObject)Instantiate(itemButton) as GameObject;
            itemButton_clone.transform.SetParent(this.transform);
            itemButton_clone.transform.localScale = new Vector3(1, 1, 1);
            Vector3 tempPosi = itemButton_clone.transform.localPosition;
            tempPosi.z = 0;
            itemButton_clone.transform.localPosition = tempPosi;
            Item tempItem = itemDrawList[i];
            itemButton_clone.name = tempItem.scriptName;
            itemButton_clone.AddComponent(System.Type.GetType(itemButton_clone.name));
            // itemDB.ScriptNameCheck(itemButton_clone.name); // Fehlerbehandlung
            Item itemComponent = itemButton_clone.GetComponent(System.Type.GetType(itemButton_clone.name)) as Item;
            itemDB.SyncValues(itemComponent, tempItem);
            itemButton_clone.GetComponent<Image>().sprite = Resources.Load<Sprite>(itemComponent.imagePath);

            //Set Price
            buyItemOnClick = itemButton_clone.GetComponent<BuyItemOnClick>();
            buyItemOnClick.GetPrice(tempItem.itemTier);

            StartCoroutine(DelayIgnoreLayout(itemButton_clone));
        }
    }

    IEnumerator DelayIgnoreLayout(GameObject gameobject)
    {
        yield return new WaitForSeconds(0.1f);

        //Ignore Layout
        gameobject.GetComponent<LayoutElement>().ignoreLayout = true;
    }

    void ItemRandomizer()
    {
        int tier1counter = 0;
        int tier2counter = 0;

        if (itemsToShow > maxToDraw)
        {
            itemsToShow = maxToDraw;
        }

        for (int i = 0; i < itemsToShow; i++)
        {

            int randomValue = Random.Range(1, 101);
            if (randomValue <= tier1chance)
            {
                tier1counter++;
            }
            else if (randomValue <= tier1chance + tier2chance)
            {
                int itemsTier2Left = itemDB.GetItemAmountByTier(ItemTier.Rare) - itemDB.GetPlayerItemAmountByTier(ItemTier.Rare);
                if (itemsTier2Left > 0)
                {
                    tier2counter++;
                }
                else
                {
                    tier1counter++;
                }
            }
        }
        List<Item> tempList = new List<Item>();
        tempList = itemDB.GetXItemsBySource(tier1counter, ItemTier.Common);
        for (int i = 0; i < tier1counter; i++)
        {
            itemDrawList.Add(tempList[i]);
        }
        tempList.Clear();
        tempList = itemDB.GetXItemsBySource(tier2counter, ItemTier.Rare);
        for (int i = 0; i < tier2counter; i++)
        {
            itemDrawList.Add(tempList[i]);

        }
    }
}
