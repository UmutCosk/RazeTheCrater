﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using UnityEngine.SceneManagement;


public class RewardEvents : MonoBehaviour
{


    void Start()
    {

    }

    public void MidasGlove()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        int randomItem = Random.Range(0, 2);
        switch (randomItem)
        {
            case 0:
                PlayerPrefSerialize.playerValues.Items.Add(25); //Midas Glove
                this.gameObject.transform.GetChild(0).GetComponent<Text>().text = "You found: Midas Glove!";
                break;
            case 1:
                PlayerPrefSerialize.playerValues.Items.Add(4); //Feels Like Home
                this.gameObject.transform.GetChild(0).GetComponent<Text>().text = "You found: Feels Like Home!";
                break;
        }
        PlayerPrefSerialize.SavePlayerValues();
    }



}
