﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateRandomEvent : MonoBehaviour
{
    int rarity;
    int interruptCounter;
    QuestionRoomDatabase questionDB;
    // Use this for initialization
    void Awake()
    {
        questionDB = GameObject.Find("Database").GetComponent<QuestionRoomDatabase>();
        foreach (Transform child in GameObject.Find("AllEvents").transform)
        {
            child.gameObject.SetActive(true);
        }
        int check = Random.Range(0, 101);
        if (check <= 66)
        {
            rarity = 1;
        }
        else if (check <= 99)
        {
            rarity = 2;
        }
        else if (check == 100)
        {
            rarity = 3;
        }
        string eventName = questionDB.GetRandomEventByRarity(rarity).title;
        foreach (Transform child in GameObject.Find("AllEvents").transform)
        {
            if (child.name != eventName)
            {
                child.gameObject.SetActive(false);
            }
        }
    }
}
