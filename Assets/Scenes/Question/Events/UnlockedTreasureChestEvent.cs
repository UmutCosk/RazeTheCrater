﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;


public class UnlockedTreasureChestEvent : MonoBehaviour, IPointerClickHandler
{
    bool alreadyClicked;
    void Start()
    {
        alreadyClicked = false;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!alreadyClicked)
        {
         /*    int randomRange = Random.Range(1, 101);
            if (randomRange <= 50)
            {
                this.gameObject.transform.GetChild(0).GetComponent<Text>().text = "You found nothing";
            }
            else if (randomRange <= 85)
            {
                EventManager.PutItemReward(1, null, ItemTier.Tier1, true);
                EventManager.CallRewardWindow(this.gameObject.scene.name);
            }
            else
            {
                EventManager.PutItemReward(1, null, ItemTier.Tier2, true);
                EventManager.PutSkillReward(5, null, 1, true);
                EventManager.PutGoldReward(50, 50);
                EventManager.PutConsumableReward(1, null, true);
                EventManager.CallRewardWindow(this.gameObject.scene.name);
            } */
            alreadyClicked = true;
        }
    }
}
