﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using TMPro;

public class LoadItemsToGive : MonoBehaviour
{

    ItemsDatabase itemDB;

    public GameObject ItemToLoad;
    // Use this for initialization
    void LoadItems()
    {
        itemDB = GameObject.Find("Database").GetComponent<ItemsDatabase>();
        PlayerPrefSerialize.LoadPlayerValues();
        for (int i = 0; i < PlayerPrefSerialize.playerValues.Items.Count; i++)
        {
            GameObject itemPrefab_clone = Instantiate(ItemToLoad, new Vector3(0, 0, 0), Quaternion.identity);
            itemPrefab_clone.transform.SetParent(this.gameObject.transform);
            itemPrefab_clone.transform.localScale = new Vector3(1, 1, 1);
            Vector3 temp_position = itemPrefab_clone.transform.localPosition;
            temp_position.z = 0;
            itemPrefab_clone.transform.localPosition = temp_position;
            Item ItemComponent = itemDB.GetItemByID(PlayerPrefSerialize.playerValues.Items[i]);
            itemPrefab_clone.name = ItemComponent.scriptName;
            itemPrefab_clone.gameObject.AddComponent(System.Type.GetType(ItemComponent.scriptName));
            itemPrefab_clone.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(ItemComponent.imagePath);
            int temp_id = ItemComponent.id;
            ItemComponent = itemPrefab_clone.GetComponent(System.Type.GetType(ItemComponent.scriptName)) as Item;
            itemDB.SyncValues(ItemComponent, itemDB.GetItemByID(temp_id));
        }
    }

    public void AttachScript(string scriptName)
    {
        LoadItems();
        foreach (Transform child in this.gameObject.transform)
        {
            child.gameObject.AddComponent(System.Type.GetType(scriptName));
        }

    }
    public void DisattachScript()
    {

        foreach (Transform child in this.gameObject.transform)
        {
            Destroy(child.gameObject, 0.2f);
        }

    }
}
