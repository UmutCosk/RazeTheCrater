﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRoomPanel : MonoBehaviour
{
    float width;
    float height;

    float curPositionX;
    float curPositionY;

    float lastPositionX;

    float endPositionX;
    float adjustPosi = 750;

    bool scroll = true;


    Vector2 SetPosition = new Vector2();
    Vector2 SetSize = new Vector2();
    void Start()
    {
        //--------   currentStep
        MapPrefSerialize.LoadMapValues();
        int currentStep = MapPrefSerialize.mapValues.CurrentStep; 

        //-------    position
        //Get current position in X
        curPositionX = this.GetComponent<RectTransform>().anchoredPosition.x;
        curPositionY = this.GetComponent<RectTransform>().anchoredPosition.y;

        //calc last position depending on currentStep - this the position to go instantly
        lastPositionX = curPositionX;
        //calc end position to scroll to in time
        endPositionX = lastPositionX - adjustPosi;

        //Set last Position
        SetPosition.x = lastPositionX - 1500 - (currentStep - 1) * 590;
        SetPosition.y = curPositionY;
        this.GetComponent<RectTransform>().anchoredPosition = SetPosition;

        // ----------SIZE - limits scrolling, so you cannot go to the right as much as possible
        //Get current size
        height = this.GetComponent<RectTransform>().sizeDelta.y;
        width = this.GetComponent<RectTransform>().sizeDelta.x;

        //calc new width depending on currentStep
        width = width + 3000 + 1200 * (currentStep);
        endPositionX = SetPosition.x - 590;
        //Put new Values to Vector2 Size
        SetSize.x = width;
        SetSize.y = height;

        //Set new Size
        this.GetComponent<RectTransform>().sizeDelta = SetSize;
    }

    void Update()
    {
        //auto-scrolls exponentially to the desired endpoint
        if (scroll == true)
        {
            MapPrefSerialize.LoadMapValues();
            this.transform.Translate((1.25f - Mathf.Exp(Time.timeSinceLevelLoad)) * 0.01f, 0, 0);
        }
        //if the desired endpoint is reached, than stop the auto-scroll
        if (this.GetComponent<RectTransform>().anchoredPosition.x < endPositionX)
        {
            scroll = false;
        }
    }


}
