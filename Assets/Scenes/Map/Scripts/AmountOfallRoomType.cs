﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmountOfallRoomTypes {
  public int ofINN ;
  public int ofShop;
  public int ofBlacksmith;
  public int ofQuestion;
  public int ofNonEnemyRooms;
  public int ofGoldroom ;
  public int ofEliteEnemy;
  public int ofNormalEnemy;
}
