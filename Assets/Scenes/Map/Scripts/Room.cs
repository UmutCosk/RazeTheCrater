﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum Roomtype { Empty, INN, Shop, Question, Event, NormalEnemy, Goldroom, Blacksmith, Boss, Start };
public class Room : MonoBehaviour
{
    public Roomtype roomtype;
    public bool clickable;
    // Use this for initialization
    public void Init()
    {
        Sprite example1;
        Button uiBtn;
        switch (roomtype)
        {
            case Roomtype.Goldroom:
                example1 = (Sprite)Resources.Load<Sprite>("Scenes/Maps/Pictures/Roomicons/Gold") as Sprite;
                this.GetComponent<Image>().sprite = example1;
                uiBtn = this.GetComponent<Button>();
                uiBtn.onClick.AddListener(() => RoomOnClickEvent(roomtype));
                break;
            case Roomtype.INN:
                example1 = (Sprite)Resources.Load<Sprite>("Scenes/Maps/Pictures/Roomicons/Inn") as Sprite;
                this.GetComponent<Image>().sprite = example1;
                uiBtn = this.GetComponent<Button>();
                uiBtn.onClick.AddListener(() => RoomOnClickEvent(roomtype));
                break;
            case Roomtype.Boss:
                example1 = (Sprite)Resources.Load<Sprite>("Scenes/Maps/Pictures/Roomicons/Boss") as Sprite;
                this.GetComponent<Image>().sprite = example1;
                uiBtn = this.GetComponent<Button>();
                uiBtn.onClick.AddListener(() => RoomOnClickEvent(roomtype));
                break;
            case Roomtype.NormalEnemy:
                example1 = (Sprite)Resources.Load<Sprite>("Scenes/Maps/Pictures/Roomicons/Normal") as Sprite;
                this.GetComponent<Image>().sprite = example1;
                uiBtn = this.GetComponent<Button>();
                uiBtn.onClick.AddListener(() => RoomOnClickEvent(roomtype));
                break;
            case Roomtype.Start:
                example1 = (Sprite)Resources.Load<Sprite>("Scenes/Maps/Pictures/Roomicons/Start") as Sprite;
                this.GetComponent<Image>().sprite = example1;
                uiBtn = this.GetComponent<Button>();
                uiBtn.onClick.AddListener(() => RoomOnClickEvent(roomtype));
                break;
            case Roomtype.Question:
                example1 = (Sprite)Resources.Load<Sprite>("Scenes/Maps/Pictures/Roomicons/Question") as Sprite;
                this.GetComponent<Image>().sprite = example1;
                uiBtn = this.GetComponent<Button>();
                uiBtn.onClick.AddListener(() => RoomOnClickEvent(roomtype));
                break;
            case Roomtype.Blacksmith:
                example1 = (Sprite)Resources.Load<Sprite>("Scenes/Maps/Pictures/Roomicons/Blacksmith") as Sprite;
                this.GetComponent<Image>().sprite = example1;
                uiBtn = this.GetComponent<Button>();
                uiBtn.onClick.AddListener(() => RoomOnClickEvent(roomtype));
                break;
            case Roomtype.Shop:
                example1 = (Sprite)Resources.Load<Sprite>("Scenes/Maps/Pictures/Roomicons/Shop") as Sprite;
                this.GetComponent<Image>().sprite = example1;
                uiBtn = this.GetComponent<Button>();
                uiBtn.onClick.AddListener(() => RoomOnClickEvent(roomtype));
                break;
        }
    }

    public void RoomOnClickEvent(Roomtype roomtype)
    {

        switch (roomtype)
        {
            case Roomtype.Goldroom:
                MapPrefSerialize.LoadMapValues();
                MapPrefSerialize.mapValues.CurrentStep++;
                MapPrefSerialize.SaveMapValues();
                SceneManager.LoadScene("Goldroom");
                break;

            case Roomtype.INN:
                MapPrefSerialize.LoadMapValues();
                MapPrefSerialize.mapValues.CurrentStep++;
                MapPrefSerialize.SaveMapValues();
                SceneManager.LoadScene("INN");
                break;

            case Roomtype.Boss:
                SceneManager.LoadScene("Combat");
                break;

            case Roomtype.NormalEnemy:

                SceneManager.LoadScene("Combat2");
                break;
            case Roomtype.Start:

                SceneManager.LoadScene("Menu");
                break;

            case Roomtype.Question:
                MapPrefSerialize.LoadMapValues();
                MapPrefSerialize.mapValues.CurrentStep++;
                MapPrefSerialize.SaveMapValues();
                int randomNumber = Random.Range(1, 101);
                if (randomNumber <= 10)
                {
                    SceneManager.LoadScene("INN");
                }
                else if (randomNumber <= 20)
                {
                    SceneManager.LoadScene("Combat");
                }
                else if (randomNumber <= 30)
                {
                    SceneManager.LoadScene("Shop");
                }
                else
                {
                    SceneManager.LoadScene("Question");
                }

                break;

            case Roomtype.Blacksmith:
                MapPrefSerialize.LoadMapValues();
                MapPrefSerialize.mapValues.CurrentStep++;
                MapPrefSerialize.SaveMapValues();
                SceneManager.LoadScene("Blacksmith");
                break;

            case Roomtype.Shop:
                MapPrefSerialize.LoadMapValues();
                MapPrefSerialize.mapValues.CurrentStep++;
                MapPrefSerialize.SaveMapValues();
                SceneManager.LoadScene("Shop");
                break;
        }
    }
}