﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetGoldAmount : MonoBehaviour {

	// Use this for initialization
	void Start () {
		PlayerPrefSerialize.LoadPlayerValues();
		this.GetComponent<Text>().text = "Gold: "+PlayerPrefSerialize.playerValues.Gold;
	}

	public void SetAmount()
	{
			PlayerPrefSerialize.LoadPlayerValues();
		this.GetComponent<Text>().text = "Gold: "+PlayerPrefSerialize.playerValues.Gold;
		 
	}
	
	
}
