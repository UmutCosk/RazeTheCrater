﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class CreateRoom : MonoBehaviour
{
    public Room room;
    public int NewGame;
    public AmountOfallRoomTypes amount;
    public HighestOrLowestRoomsOfaStep highOrLow;
    //Anzahl der Schritte mit der jeweiligen Raumanzahl
    private int room1maxCounter = 5;
    private int room2maxCounter = 3; //komibiniert mit room3maxCounter....
    private int room3maxCounter = 4; //insgesamt 9
    private int sumOfAllRooms;
    private int stepsUntilBoss;
    private int sameStep = 0;
    private int currentStep;
    private int neuerVersuchCounter = 0;
    public List<int> RoomsInStep;
    public GameObject RoomButtonPrefab;


    public class RoomNrSubListObj
    {
        public List<int> RoomNrList = new List<int>();

    }

    public List<Object> RoomDistributionList = new List<Object>();



    void Start()
    {
        MapPrefSerialize.LoadMapValues();

        stepsUntilBoss = room3maxCounter + room2maxCounter + room1maxCounter;
        sumOfAllRooms = 1 * room1maxCounter + 2 * room2maxCounter + 3 * room3maxCounter + 1; //+1 wegen Startbutton 

        int[,] StepAndRoomEqualsRoomNR = new int[stepsUntilBoss + 1, 3];
        int[] RoomsInStep = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        Roomtype[] roomtypeArray = new Roomtype[sumOfAllRooms];

        //NewGame = PlayerPrefs.GetInt("NewGame");
        NewGame = MapPrefSerialize.mapValues.NewGame;
        int roomNR = 0;
        if (NewGame == 1)
        {
            currentStep = 1;
            MapPrefSerialize.mapValues.CurrentStep = 1;

            RoomsInStep = GenerateStepsAndRooms(RoomsInStep, stepsUntilBoss, room2maxCounter, room3maxCounter);

            for (int step = 0; step <= stepsUntilBoss; step++)
            {
                sameStep = 0;
                for (int roomCounter = 0; roomCounter < RoomsInStep[step]; roomCounter++)
                {
                    StepAndRoomEqualsRoomNR[step, roomCounter] = roomNR;
                    roomtypeArray[roomNR] = Roomtype.Empty;
                    roomNR++;
                }
            }
            GenerateRoomType(sumOfAllRooms, roomtypeArray, RoomsInStep, StepAndRoomEqualsRoomNR);
        }
        else
        {
            currentStep = MapPrefSerialize.mapValues.CurrentStep;
            for (int i = 0; i <= stepsUntilBoss; i++)
            {
                RoomsInStep[i] = MapPrefSerialize.mapValues.RoomsInStep[i];

            }

            for (int j = 0; j < sumOfAllRooms; j++)
            {
                roomtypeArray[j] = (Roomtype)System.Enum.Parse(typeof(Roomtype), MapPrefSerialize.mapValues.RoomTypes[j]);
            }

            roomNR = 0;
            for (int step = 0; step <= stepsUntilBoss; step++)
            {
                sameStep = 0;
                for (int roomCounter = 0; roomCounter < RoomsInStep[step]; roomCounter++)
                {
                    StepAndRoomEqualsRoomNR[step, roomCounter] = roomNR;
                    roomNR++;
                }
            }

        }

        Room[] room = new Room[sumOfAllRooms];
        roomNR = 0;

        for (int step = 0; step <= stepsUntilBoss; step++)
        {
            sameStep = 0;
            for (int roomCounter = 0; roomCounter < RoomsInStep[step]; roomCounter++)
            {
                room[roomNR] = gameObject.AddComponent<Room>();

                GenerateButton(roomtypeArray[roomNR], roomNR, step, sameStep, RoomsInStep);

                roomNR++;
                sameStep++;
            }
        }

        // // Save Map in PlayerPrefs
        if (MapPrefSerialize.mapValues.NewGame == 1)
        {
            for (int i = 0; i < RoomsInStep.Length; i++)
            {

                MapPrefSerialize.mapValues.RoomsInStep.Add(RoomsInStep[i]);
            }
            for (int j = 0; j < sumOfAllRooms; j++)
            {
                MapPrefSerialize.mapValues.RoomTypes.Add(roomtypeArray[j].ToString());
            }
        }
        MapPrefSerialize.mapValues.NewGame = 0;
        currentStep = MapPrefSerialize.mapValues.CurrentStep;

        MapPrefSerialize.SaveMapValues();

        SetAllRoomsToFalse(room);
        SetRoomsInStepToActive(room, RoomsInStep[currentStep + 1], currentStep + 1, StepAndRoomEqualsRoomNR);
        SetClickable(room);
    }


    void GenerateButton(Roomtype roomtype, int roomNR, int step, int sameStep, int[] RoomsInStep)
    {
        GameObject RoomButtonPrefabClone = (GameObject)Instantiate(RoomButtonPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        RoomButtonPrefabClone.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
        SetRoomsPositions(RoomButtonPrefabClone, RoomsInStep[step], step, sameStep);
        RoomButtonPrefabClone.name = "Room" + roomNR;
        Room uiBtnRoom = RoomButtonPrefabClone.GetComponent<Room>();
        if (uiBtnRoom != null)
        {
            uiBtnRoom.roomtype = roomtype;
            uiBtnRoom.Init();
        }
    }

    public int GoldRoomStep;
    public List<int> exceptionSteps = new List<int>();
    int[] GenerateStepsAndRooms(int[] RoomsInStep, int stepsUntilBoss, int room2maxCounter, int room3maxCounter)
    {
        int stepNumber;
        int numberOfRoom;
        int amountOfSteps = 0;
        int fromStep;
        int untilStep;
        int amountOfRooms;

        for (int i = 0; i < stepsUntilBoss; i++)
        {
            RoomsInStep[i] = 3;
        }

        //set known steps with 1 room
        numberOfRoom = 1;

        stepNumber = 0;
        RoomsInStep = SetNrOfRoomsInStep(stepNumber, numberOfRoom, RoomsInStep);

        stepNumber = 1;
        RoomsInStep = SetNrOfRoomsInStep(stepNumber, numberOfRoom, RoomsInStep);

        stepNumber = stepsUntilBoss;
        RoomsInStep = SetNrOfRoomsInStep(stepNumber, numberOfRoom, RoomsInStep);

        GoldRoomStep = Random.Range(6, 8);
        RoomsInStep = SetNrOfRoomsInStep(GoldRoomStep, numberOfRoom, RoomsInStep);


        // set 1 random steps with 1 room before Goldroom
        amountOfRooms = 1;
        amountOfSteps = 1;
        fromStep = 3;
        untilStep = GoldRoomStep - 2;
        RoomsInStep = SetStepsWithXRoom(fromStep, untilStep, amountOfSteps, amountOfRooms, RoomsInStep);


        // set 1 random steps with 1 room after Goldroom
        amountOfRooms = 1;
        amountOfSteps = 1;
        fromStep = GoldRoomStep + 2;
        untilStep = stepsUntilBoss - 2;
        RoomsInStep = SetStepsWithXRoom(fromStep, untilStep, amountOfSteps, amountOfRooms, RoomsInStep);

        int randomdistr = 3;
        // set 1 or 2 random steps with 2 room before Goldroom
        amountOfRooms = 2;
        amountOfSteps = Random.Range(1, 3);
        randomdistr = randomdistr - amountOfSteps;
        fromStep = 2;
        untilStep = GoldRoomStep - 1;
        RoomsInStep = SetStepsWithXRoom(fromStep, untilStep, amountOfSteps, amountOfRooms, RoomsInStep);


        // set 1 or 2 random steps with 2 room after Goldroom
        amountOfRooms = 2;
        amountOfSteps = randomdistr;
        fromStep = GoldRoomStep + 1;
        untilStep = stepsUntilBoss - 1;
        RoomsInStep = SetStepsWithXRoom(fromStep, untilStep, amountOfSteps, amountOfRooms, RoomsInStep);

        return RoomsInStep;
    }

    int[] SetNrOfRoomsInStep(int stepNumber, int numberOfRoom, int[] newRoomsInStep)
    {
        newRoomsInStep[stepNumber] = numberOfRoom;
        exceptionSteps.Add(stepNumber);
        return newRoomsInStep;
    }
    int[] SetStepsWithXRoom(int fromStep, int untilStep, int setAmount, int roomNumber, int[] newRoomsInStep)
    {
        int randomStep = Random.Range(fromStep, untilStep + 1);
        bool stepIsSet = false;

        for (int i = 0; i < setAmount; i++)
        {
            while (stepIsSet == false)
            {
                stepIsSet = true;
                randomStep = Random.Range(fromStep, untilStep + 1);
                for (int j = 0; j < exceptionSteps.Count; j++)
                {
                    if (randomStep == exceptionSteps[j])
                    {
                        stepIsSet = false;
                        break;
                    }

                }
            }
            stepIsSet = false;
            newRoomsInStep[randomStep] = roomNumber;
            exceptionSteps.Add(randomStep);
        }
        return newRoomsInStep;
    }


    void SetRoomsPositions(GameObject go, int RoomInStep, int StepNumber, int sameStep)
    {
        // Die Räume werden auf der Map verteilt. Dabei hängt die Y-Koordinate jeweils von der Anzahl der Räume im jeweiligen Step ab.
        // Die variable sameStep wird nach jedem Step resettet und sorgt dafür, dass 2 Räume in einem Step jeweils gespielt um Y verschoben werden.
        int startPointAtX = 300;
        int offsetX = 590;
        int offsetYwhen2Rooms = 210;
        int offsetYwhen3Rooms = 350;
        int applyRandomAdjustmentX = Random.Range(-0, 0); //+-25
        int applyRandomAdjustmentY = Random.Range(-0, 0);  //+-35

        Vector3 tempPos = new Vector2(0, 0);

        if (RoomInStep == 1)
        {
            tempPos.x = startPointAtX + StepNumber * offsetX;
            tempPos.y = 0;
            go.GetComponent<RectTransform>().anchoredPosition = tempPos;
        }
        if (RoomInStep == 2)
        {
            if (sameStep == 0)
            {
                tempPos.x = startPointAtX + StepNumber * offsetX + 2 * applyRandomAdjustmentX;
                tempPos.y = offsetYwhen2Rooms + applyRandomAdjustmentY;
                go.GetComponent<RectTransform>().anchoredPosition = tempPos;
            }
            else
            {
                tempPos.x = startPointAtX + StepNumber * offsetX + 2 * applyRandomAdjustmentX;
                tempPos.y = -offsetYwhen2Rooms + applyRandomAdjustmentY;
                go.GetComponent<RectTransform>().anchoredPosition = tempPos;
            }
        }

        if (RoomInStep == 3)
        {
            if (sameStep == 0)
            {
                tempPos.x = startPointAtX + StepNumber * offsetX + applyRandomAdjustmentX;
                tempPos.y = applyRandomAdjustmentY;
                go.GetComponent<RectTransform>().anchoredPosition = tempPos;
            }
            else if (sameStep == 1)
            {
                tempPos.x = startPointAtX + StepNumber * offsetX + applyRandomAdjustmentX;
                tempPos.y = offsetYwhen3Rooms + applyRandomAdjustmentY;
                go.GetComponent<RectTransform>().anchoredPosition = tempPos;
            }
            else
            {
                tempPos.x = startPointAtX + StepNumber * offsetX + applyRandomAdjustmentX;
                tempPos.y = -offsetYwhen3Rooms + applyRandomAdjustmentY;
                go.GetComponent<RectTransform>().anchoredPosition = tempPos;
            }
        }
    }




    void GenerateRoomType(int sumOfAllRooms, Roomtype[] roomtypeArray, int[] RoomsInStepCounter, int[,] StepAndRoomEqualsRoomNR)
    {
    neuerVersuch:
        AmountOfallRoomTypes amount = new AmountOfallRoomTypes();
        amount.ofINN = Random.Range(3, 5);
        amount.ofShop = Random.Range(1, 3);
        amount.ofBlacksmith = Random.Range(2, 4);
        amount.ofQuestion = Random.Range(5, 7);

        int sumOfNonEnemyRooms = amount.ofQuestion + amount.ofBlacksmith + amount.ofShop + amount.ofINN;

        // if less then 13, add random rooms until 13
        if (sumOfNonEnemyRooms < 13)
        {
            int roomsLeftToFill = 12 - sumOfNonEnemyRooms;
            int roomFillCounter = 1;
            while (roomFillCounter <= roomsLeftToFill)

            {
                int whichRoom = Random.Range(1, 5);
                if ((whichRoom == 1) && (amount.ofINN < 4))
                {

                    amount.ofINN += 1;
                    roomFillCounter += 1;
                }
                if ((whichRoom == 2) && (amount.ofShop < 2))
                {
                    amount.ofShop += 1;
                    roomFillCounter += 1;
                }
                if ((whichRoom == 3) && (amount.ofBlacksmith < 2))
                {
                    amount.ofBlacksmith += 1;
                    roomFillCounter += 1;
                }
                if ((whichRoom == 4) && (amount.ofQuestion < 6))
                {
                    amount.ofQuestion += 1;
                    roomFillCounter += 1;
                }


            }

        }
        //if more then 14, delete random rooms until 14
        if (sumOfNonEnemyRooms > 14)
        {
            int roomsLeftToFill = 12 - sumOfNonEnemyRooms;
            int roomFillCounter = 1;
            while (roomFillCounter <= roomsLeftToFill)
            {
                int whichRoom = Random.Range(1, 5);
                if ((whichRoom == 1) && (amount.ofINN < 4))
                {

                    amount.ofINN -= 1;
                    roomFillCounter += 1;
                }
                if ((whichRoom == 2) && (amount.ofShop < 2))
                {
                    amount.ofShop -= 1;
                    roomFillCounter += 1;
                }
                if ((whichRoom == 3) && (amount.ofBlacksmith < 2))
                {
                    amount.ofBlacksmith -= 1;
                    roomFillCounter -= 1;
                }
                if ((whichRoom == 4) && (amount.ofQuestion < 6))
                {
                    amount.ofQuestion -= 1;
                    roomFillCounter += 1;
                }


            }

        }

        sumOfNonEnemyRooms = amount.ofQuestion + amount.ofBlacksmith + amount.ofShop + amount.ofINN;
        amount.ofGoldroom = 1;
        amount.ofEliteEnemy = 0;
        amount.ofNormalEnemy = sumOfAllRooms - amount.ofEliteEnemy - sumOfNonEnemyRooms - amount.ofGoldroom - 1 - 1; //-1 wegen Bossraum -1 wegen Startbutton 

        //Start
        roomtypeArray[0] = Roomtype.Start;
        //erster Gegner 
        roomtypeArray[1] = Roomtype.NormalEnemy;
        amount.ofNormalEnemy = amount.ofNormalEnemy - 1;

        //Bossraum
        roomtypeArray[sumOfAllRooms - 1] = Roomtype.Boss;
        //Goldraum
        int GoldroomNumber = StepAndRoomEqualsRoomNR[GoldRoomStep, 0];
        roomtypeArray[GoldroomNumber] = Roomtype.Goldroom;

        int stepDistance;
        int setNrOfRooms;
        int fromStep;
        int untilStep;


        //INN vor Bossraum 
        fromStep = stepsUntilBoss - 1;
        untilStep = stepsUntilBoss; //muss immer ein höher als gewollt
        stepDistance = 0;
        setNrOfRooms = 1;

        amount.ofINN = SetRoomsOfType(roomtypeArray, Roomtype.INN, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                              amount.ofINN, setNrOfRooms, fromStep, untilStep, StepPrio.Neither);

        //set 2 Enemys in Steps with 2 Rooms with being 2 Steps apart
        fromStep = 3;
        untilStep = stepsUntilBoss - 1 - 1; //muss immer ein höher als gewollt
        stepDistance = 0;
        setNrOfRooms = 2;

        amount.ofNormalEnemy = SetRoomsOfType(roomtypeArray, Roomtype.NormalEnemy, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                              amount.ofNormalEnemy, setNrOfRooms, fromStep, untilStep, StepPrio.LowestRoom);
        // restliche Gegner
        fromStep = 2;
        untilStep = stepsUntilBoss;
        stepDistance = 0;
        setNrOfRooms = amount.ofNormalEnemy;

        amount.ofNormalEnemy = SetRoomsOfType(roomtypeArray, Roomtype.NormalEnemy, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                             amount.ofNormalEnemy, setNrOfRooms, fromStep, untilStep, StepPrio.HighestRoom);


        // INN vor Gold
        //set 2 Enemys in Steps with 2 Rooms with being 2 Steps apart
        fromStep = 4;
        untilStep = GoldRoomStep; //muss immer ein höher als gewollt
        stepDistance = 0;
        setNrOfRooms = 1;

        amount.ofINN = SetRoomsOfType(roomtypeArray, Roomtype.INN, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                              amount.ofINN, setNrOfRooms, fromStep, untilStep, StepPrio.HighestRoom);

        // ?-Rooms
        //set 2 Enemys in Steps with 2 Rooms with being 2 Steps apart
        fromStep = 2;
        untilStep = stepsUntilBoss; //muss immer ein höher als gewollt
        stepDistance = 0;
        setNrOfRooms = amount.ofQuestion;

        amount.ofQuestion = SetRoomsOfType(roomtypeArray, Roomtype.Question, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                              amount.ofQuestion, setNrOfRooms, fromStep, untilStep, StepPrio.HighestRoom);

        //Shop nach Goldraum
        fromStep = GoldRoomStep + 1;
        untilStep = stepsUntilBoss - 1; //muss immer ein höher als gewollt
        stepDistance = 0;
        setNrOfRooms = 1;

        amount.ofShop = SetRoomsOfType(roomtypeArray, Roomtype.Shop, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                              amount.ofShop, setNrOfRooms, fromStep, untilStep, StepPrio.Neither);

        if (amount.ofShop != 0)
        {
            //Shop vor Goldraum
            fromStep = 2;
            untilStep = GoldRoomStep; //muss immer ein höher als gewollt
            stepDistance = 0;
            setNrOfRooms = 1;
            amount.ofShop = SetRoomsOfType(roomtypeArray, Roomtype.Shop, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                                  amount.ofShop, setNrOfRooms, fromStep, untilStep, StepPrio.Neither);
        }

        //Blacksmith random vor oder nach Goldraum
        int[] randomFromStep = new int[] { 2, GoldRoomStep + 1 };
        int[] randomUntilStep = new int[] { GoldRoomStep, stepsUntilBoss };
        int randomIndexForBlacksmith = Random.Range(0, 2);

        fromStep = randomFromStep[randomIndexForBlacksmith];
        untilStep = randomUntilStep[randomIndexForBlacksmith]; //muss immer ein höher als gewollt

        stepDistance = 0;
        setNrOfRooms = 1;
        amount.ofBlacksmith = SetRoomsOfType(roomtypeArray, Roomtype.Blacksmith, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                              amount.ofBlacksmith, setNrOfRooms, fromStep, untilStep, StepPrio.Neither);

        if (amount.ofBlacksmith != 0)
        {
            if (randomIndexForBlacksmith == 1) { randomIndexForBlacksmith = 0; } else { randomIndexForBlacksmith = 1; }

            //Blacksmith vice verca , falls noch ein Blacksmith exisitert.
            fromStep = randomFromStep[randomIndexForBlacksmith];
            untilStep = randomUntilStep[randomIndexForBlacksmith]; //muss immer ein höher als gewollt

            stepDistance = 0;
            setNrOfRooms = 1;

            amount.ofBlacksmith = SetRoomsOfType(roomtypeArray, Roomtype.Blacksmith, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                                  amount.ofBlacksmith, setNrOfRooms, fromStep, untilStep, StepPrio.Neither);
        }

        if (amount.ofBlacksmith != 0)
        {
            //Blacksmith , falls obere Bedingungen unerfüllt blieben.
            fromStep = 2;
            untilStep = stepsUntilBoss - 1; //muss immer ein höher als gewollt
            stepDistance = 0;
            setNrOfRooms = 1;

            amount.ofBlacksmith = SetRoomsOfType(roomtypeArray, Roomtype.Blacksmith, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                                  amount.ofBlacksmith, setNrOfRooms, fromStep, untilStep, StepPrio.Neither);
        }

        // restliche INN
        fromStep = 2;
        untilStep = stepsUntilBoss;
        stepDistance = 0;
        setNrOfRooms = amount.ofINN;

        amount.ofINN = SetRoomsOfType(roomtypeArray, Roomtype.INN, stepDistance, StepAndRoomEqualsRoomNR, RoomsInStepCounter,
                                             amount.ofINN, setNrOfRooms, fromStep, untilStep, StepPrio.HighestRoom);

        // restliche Elite
        if (HasRoomType(roomtypeArray, Roomtype.Empty))
        { //falls Regeln im Widerspruch stehen, bleiben leere Räume übrig. Wird momentan mit einem neuen Versuch gelöst.

            for (int m = 0; m < sumOfAllRooms;    m++    )
            {
                roomtypeArray[m] = Roomtype.Empty;

                neuerVersuchCounter++;
                goto neuerVersuch;
            }
        }

    }


    int SetRoomsOfType(Roomtype[] roomtypeArray, Roomtype thisRoomType, int stepDistance, int[,] StepAndRoomEqualsRoomNR, int[] RoomsInStepCounter,
                   int amountOfThisRoomType, int setNrOfRooms, int fromStep, int untilStep, StepPrio stepPrio)
    {
        int startamount = amountOfThisRoomType;
        int[] stepsWithEmptyRoomsTemp;
        int[] stepsWithEmptyRooms;
        int cancelIfNotPossible = 1;
        int randomIndex;
        int randomStep = 0;
        int interrupt = 50;
        int interrupt2 = 50;
        while (startamount - setNrOfRooms < amountOfThisRoomType * cancelIfNotPossible)
        {
        iferror:
            interrupt -= 1;
            if (interrupt < 0)
            {
                interrupt2 -= 1; if (StepPrio.HighestRoom == stepPrio) { stepPrio = StepPrio.Neither; }

                if (interrupt2 < 0) { cancelIfNotPossible = -1; }
            } // für Gegnersetzen
            switch (stepPrio)
            {
                case StepPrio.HighestRoom:
                    stepsWithEmptyRoomsTemp = GetStepsWithHighestEmptyRooms(roomtypeArray, StepAndRoomEqualsRoomNR, RoomsInStepCounter);
                    stepsWithEmptyRooms = RemoveStepsOutsideOfRange(stepsWithEmptyRoomsTemp, fromStep, untilStep);
                    if (stepsWithEmptyRooms.Length == 0) { stepPrio = StepPrio.Neither; goto iferror; }
                    randomIndex = Random.Range(0, stepsWithEmptyRooms.Length);
                    randomStep = stepsWithEmptyRooms[randomIndex];

                    break;
                case StepPrio.LowestRoom:
                    stepsWithEmptyRoomsTemp = GetStepsWithLowestEmptyRooms(roomtypeArray, StepAndRoomEqualsRoomNR, RoomsInStepCounter);

                    stepsWithEmptyRooms = RemoveStepsOutsideOfRange(stepsWithEmptyRoomsTemp, fromStep, untilStep);

                    if (stepsWithEmptyRooms.Length == 0) { stepPrio = StepPrio.Neither; goto iferror; }
                    randomIndex = Random.Range(0, stepsWithEmptyRooms.Length);
                    randomStep = stepsWithEmptyRooms[randomIndex];

                    break;
                case StepPrio.Neither:
                    randomStep = Random.Range(fromStep, untilStep);
                    break;

            }

            if (RoomsInStepCounter[randomStep] == 1)
            {
                int temp1 = StepAndRoomEqualsRoomNR[randomStep, 0];
                if (roomtypeArray[temp1] == Roomtype.Empty)
                {

                    roomtypeArray[temp1] = thisRoomType;
                    amountOfThisRoomType -= 1;

                }
            }


            if (RoomsInStepCounter[randomStep] == 2)
            {
                int temp1 = StepAndRoomEqualsRoomNR[randomStep, 0];
                int temp2 = StepAndRoomEqualsRoomNR[randomStep, 1];

                Roomtype[] arrayToCheck = {
               roomtypeArray[StepAndRoomEqualsRoomNR[randomStep,0]],
               roomtypeArray[StepAndRoomEqualsRoomNR[randomStep,1]],
           };
                if (roomtypeArray[temp1] == Roomtype.Empty || roomtypeArray[temp2] == Roomtype.Empty)
                {


                    if (!HasRoomType(arrayToCheck, thisRoomType))
                    {
                        if (roomtypeArray[temp1] == Roomtype.Empty && roomtypeArray[temp2] == Roomtype.Empty)
                        {

                            int randomRoom = Random.Range(0, 2);
                            int temp3 = StepAndRoomEqualsRoomNR[randomStep, randomRoom];
                            roomtypeArray[temp3] = thisRoomType;
                            amountOfThisRoomType -= 1;
                        }

                        else if (roomtypeArray[temp1] == Roomtype.Empty)
                        {
                            roomtypeArray[temp1] = thisRoomType;
                            amountOfThisRoomType -= 1;
                        }
                        else if (roomtypeArray[temp2] == Roomtype.Empty)
                        {
                            roomtypeArray[temp2] = thisRoomType;
                            amountOfThisRoomType -= 1;
                        }



                    }

                }
            }



            if (RoomsInStepCounter[randomStep] == 3)
            {
                int temp1 = StepAndRoomEqualsRoomNR[randomStep, 0];
                int temp2 = StepAndRoomEqualsRoomNR[randomStep, 1];
                int temp3 = StepAndRoomEqualsRoomNR[randomStep, 2];
                Roomtype[] arrayToCheck = {
               roomtypeArray[temp1],
              roomtypeArray[temp2],
               roomtypeArray[temp3]
           };

                if (roomtypeArray[temp1] == Roomtype.Empty || roomtypeArray[temp2] == Roomtype.Empty || roomtypeArray[temp3] == Roomtype.Empty)
                {
                    if (!HasRoomType(arrayToCheck, thisRoomType))
                    {

                        if (roomtypeArray[temp1] == Roomtype.Empty && roomtypeArray[temp2] == Roomtype.Empty && roomtypeArray[temp3] == Roomtype.Empty)
                        {

                            int randomRoom = Random.Range(0, 3);
                            int temp4 = StepAndRoomEqualsRoomNR[randomStep, randomRoom];
                            roomtypeArray[temp4] = thisRoomType;
                            amountOfThisRoomType -= 1;
                        }

                        else if (roomtypeArray[temp1] == Roomtype.Empty)
                        {
                            roomtypeArray[temp1] = thisRoomType;
                            amountOfThisRoomType -= 1;
                        }
                        else if (roomtypeArray[temp2] == Roomtype.Empty)
                        {
                            roomtypeArray[temp2] = thisRoomType;
                            amountOfThisRoomType -= 1;
                        }
                        else if (roomtypeArray[temp3] == Roomtype.Empty)
                        {
                            roomtypeArray[temp3] = thisRoomType;
                            amountOfThisRoomType -= 1;
                        }

                    }

                }
            }

        }


        return amountOfThisRoomType;
    }

    int[] RemoveStepsOutsideOfRange(int[] stepsWithEmptyRoomsTemp, int fromStep, int untilStep)
    {
        List<int> stepsWithEmptyRooms = new List<int>();

        for (int i = 0; i < stepsWithEmptyRoomsTemp.Length; i++)
        {
            if (stepsWithEmptyRoomsTemp[i] >= fromStep && stepsWithEmptyRoomsTemp[i] <= untilStep)
            {
                stepsWithEmptyRooms.Add(stepsWithEmptyRoomsTemp[i]);
            }
        }

        return stepsWithEmptyRooms.ToArray();
    }

    int[] GetStepsWithHighestEmptyRooms(Roomtype[] roomtypeArray, int[,] StepAndRoomEqualsRoomNR, int[] RoomsInStepCounter)
    {
        int[] NumberOfEmptyRooms = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        List<int> stepsWithHighestEmptyRooms = new List<int>();
        int roomsInStep;
        for (int step = 2; step < stepsUntilBoss; step++)
        { //for1 anfang
            roomsInStep = RoomsInStepCounter[step];

            if (roomsInStep == 2)
            { //if 2 anfang
                Roomtype[] arrayToCheck = {
               roomtypeArray[StepAndRoomEqualsRoomNR[step,0]],
               roomtypeArray[StepAndRoomEqualsRoomNR[step,1]]
           };
                for (int i = 0; i < 2; i++)
                {
                    if (isEmpty(arrayToCheck[i]))
                    {
                        NumberOfEmptyRooms[step] += 1;
                    }
                }


            } //if 2 ende
            if (roomsInStep == 3)
            { //if 3 anfang
                Roomtype[] arrayToCheck = {
               roomtypeArray[StepAndRoomEqualsRoomNR[step,0]],
                roomtypeArray[StepAndRoomEqualsRoomNR[step,1]],
                roomtypeArray[StepAndRoomEqualsRoomNR[step,2]]
           };
                for (int i = 0; i < 3; i++)
                {
                    if (isEmpty(arrayToCheck[i]))
                    {
                        NumberOfEmptyRooms[step] += 1;
                    }
                }

            } //if 3 ende

        }//for1 ende

        int maxEmptyRooms;
        maxEmptyRooms = MaxValue(NumberOfEmptyRooms);
        for (int step = 2; step < stepsUntilBoss; step++)
        {  //for2 anfang

            if (NumberOfEmptyRooms[step] == maxEmptyRooms)
            {
                stepsWithHighestEmptyRooms.Add(step);
            }

        }//for2 ende
        return stepsWithHighestEmptyRooms.ToArray();

    }


    int[] GetStepsWithLowestEmptyRooms(Roomtype[] roomtypeArray, int[,] StepAndRoomEqualsRoomNR, int[] RoomsInStepCounter)
    {
        int[] NumberOfEmptyRooms = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        List<int> stepsWithLowestEmptyRooms = new List<int>();
        int roomsInStep;
        for (int step = 2; step < stepsUntilBoss; step++)
        { //for1 anfang
            roomsInStep = RoomsInStepCounter[step];

            if (roomsInStep == 2)
            { //if 2 anfang
                Roomtype[] arrayToCheck = {
               roomtypeArray[StepAndRoomEqualsRoomNR[step,0]],
               roomtypeArray[StepAndRoomEqualsRoomNR[step,1]]
           };
                for (int i = 0; i < 2; i++)
                {
                    if (isEmpty(arrayToCheck[i]))
                    {
                        NumberOfEmptyRooms[step] += 1;
                    }
                }


            } //if 2 ende
            if (roomsInStep == 3)
            { //if 3 anfang
                Roomtype[] arrayToCheck = {
                roomtypeArray[StepAndRoomEqualsRoomNR[step,0]],
                roomtypeArray[StepAndRoomEqualsRoomNR[step,1]],
                roomtypeArray[StepAndRoomEqualsRoomNR[step,2]]
           };
                for (int i = 0; i < 3; i++)
                {
                    if (isEmpty(arrayToCheck[i]))
                    {
                        NumberOfEmptyRooms[step] += 1;
                    }
                }

            } //if 3 ende

        }//for1 ende

        int minEmptyRooms;
        minEmptyRooms = MinValue(NumberOfEmptyRooms);
        for (int step = 2; step < stepsUntilBoss; step++)
        {  //for2 anfang

            if (NumberOfEmptyRooms[step] == minEmptyRooms)
            {
                stepsWithLowestEmptyRooms.Add(step);
            }

        }//for2 ende
        return stepsWithLowestEmptyRooms.ToArray();

    }




    public int MaxValue(int[] array)
    {
        int maxValue = 0;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] > maxValue) { maxValue = array[i]; }
        }


        return maxValue;
    }




    public int MinValue(int[] array)
    {
        int minValue = 3;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] < minValue) { minValue = array[i]; }
        }


        return minValue;
    }


    public int abs(int toAbsoluteNr)
    {
        if (toAbsoluteNr < 0)
        { toAbsoluteNr = toAbsoluteNr * (-1); }
        return toAbsoluteNr;

    }



    private bool HasRoomType(Roomtype[] roomtypeArray, Roomtype thisRoomType)
    {
        bool result = false;
        for (int i = 0; i < roomtypeArray.Length; i++)
        {
            if (roomtypeArray[i] == thisRoomType)
            {
                result = true;
            }
        }
        return result;
    }



    private bool isEmpty(Roomtype roomtypeArray)
    {
        bool result = false;

        if (roomtypeArray == Roomtype.Empty)
        {
            result = true;
        }

        return result;
    }

    void SetAllRoomsToFalse(Room[] rooms)
    {
        for (int i = 0; i < sumOfAllRooms; i++)
        {
            rooms[i].clickable = false;
        }
    }

    void SetRoomsInStepToActive(Room[] rooms, int roomsInStep, int currentStep, int[,] stepAndRoomEqualsRoomNR)
    {
        int temp_index;
        for (int localroomNR = 0; localroomNR < roomsInStep; localroomNR++)
        {
            temp_index = stepAndRoomEqualsRoomNR[currentStep, localroomNR];

            rooms[temp_index].clickable = true;

        }
    }

    void SetClickable(Room[] rooms)
    {
        for (int i = 0; i < sumOfAllRooms; i++)
        {
            GameObject.Find("Room" + i).GetComponent<Button>().interactable = true;
        }
    }

}




