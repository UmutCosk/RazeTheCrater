﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum CharacterType { Warrior };
public class CharacterSelection : MonoBehaviour
{


    public CharacterType chartype;
    // Use this for initialization
    void Start()
    {
        GlobalPrefSerialize.LoadPlayerValues();
        GlobalPrefSerialize.ResetGlobalValues();
        //GlobalPrefSerialize.LockIfFirstGame();
        GlobalPrefSerialize.SaveGlobalValues();

        MapPrefSerialize.ResetMapValues();
        MapPrefSerialize.LoadMapValues();
        MapPrefSerialize.mapValues.NewGame = 1;
        MapPrefSerialize.mapValues.CurrentLevel = 1;
        MapPrefSerialize.mapValues.CurrentStep = -1;
        MapPrefSerialize.mapValues.CurrentMonsterType = MonsterType.Start;
        MapPrefSerialize.SaveMapValues();

        PlayerPrefSerialize.LoadPlayerValues();
        PlayerPrefSerialize.ResetPlayerValues();
        PlayerPrefSerialize.SavePlayerValues();

        CombatPrefSerialize.ResetCombatValues();
        CombatPrefSerialize.LoadCombatValues();
        CombatPrefSerialize.combatValues.EnemySetIDsDone.Add(9999);
        CombatPrefSerialize.SaveCombatValues();
    }


    public void setToWarrior()
    {
        chartype = CharacterType.Warrior;
    }

    public void StartGame()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        //hier werden alle notwendigen PlayerPrefs für den ausgewählten Char festgelegt!
        switch (chartype)
        {
            case CharacterType.Warrior:
                {

                    PlayerPrefSerialize.playerValues.Gold = 9990;
                    PlayerPrefSerialize.playerValues.maxConsumableSlots = 3;
                    PlayerPrefSerialize.playerValues.curHP = 60;
                    PlayerPrefSerialize.playerValues.maxHP = 60;
                    PlayerPrefSerialize.playerValues.characterType = CharacterType.Warrior;
                    List<int> CardIDs = new List<int>() { 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 3, 3 };
                    PlayerPrefSerialize.AddCardIDList(CardIDs);


                }
                break;


        }
        PlayerPrefSerialize.SavePlayerValues();

        // Load Map
        MapPrefSerialize.LoadMapValues();
        SceneManager.LoadScene("Map");
    }
}
