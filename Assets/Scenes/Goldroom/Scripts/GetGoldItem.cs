﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class GetGoldItem : MonoBehaviour, IPointerClickHandler
{
    private Item itemComponent;
    bool pickBothItems;

    void Start()
    {
        pickBothItems = false;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        itemComponent = this.GetComponent(System.Type.GetType(this.name)) as Item;
        PlayerPrefSerialize.LoadPlayerValues();

        //füge item zum inventory des spielers hinzu
        PlayerPrefSerialize.playerValues.Items.Add(itemComponent.id);
        PlayerPrefSerialize.SavePlayerValues();
       // GameObject.FindGameObjectWithTag("SlotPanel").GetComponent<LoadItems>().UpdateItemPanel();

        foreach (Transform child in GameObject.Find("ItemPanel").gameObject.transform)
        {
            if (child.name == "RoyalPrivilege")
            {
                pickBothItems = true;
            }
        }
        if (pickBothItems)
        {
            this.gameObject.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            GameObject.Find("ItemButtons").gameObject.SetActive(false);
        }

    }
}
