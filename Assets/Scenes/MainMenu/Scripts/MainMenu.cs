﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MainMenu : MonoBehaviour
{

    private Text playORconti;

    void Update()
    {
        if (this.gameObject.scene.name == "Menu")
        {
            MapPrefSerialize.LoadMapValues();
            playORconti = GameObject.Find("PlayOrContinue").GetComponent<Text>();

            if (MapPrefSerialize.mapValues.NewGame == 0)
            {
                playORconti.text = "Continue Run";
            }
            else
            {
                playORconti.text = "Start New Run";
            }
        }
    }

    public void StartRun()
    {
        SceneManager.LoadScene("ChooseCharacter");
        MapPrefSerialize.LoadMapValues();
        if (MapPrefSerialize.mapValues.NewGame == 0)
        {
            PlayerPrefSerialize.LoadPlayerValues();
            if (PlayerPrefSerialize.playerValues.quitInCombat == true)
            {
                SceneManager.LoadScene("Combat");
            }
            else
            {
                MapPrefSerialize.LoadMapValues();
                SceneManager.LoadScene("Map" + (MapPrefSerialize.mapValues.CurrentLevel - 1));
            }

        }
        else
        {
            SceneManager.LoadScene("ChooseCharacter");
        }

    }


    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
        Debug.Log("Zurück");
    }

    public void QuitGame()
    {
        Debug.Log("Quit!");
        Application.Quit();

    }



}
