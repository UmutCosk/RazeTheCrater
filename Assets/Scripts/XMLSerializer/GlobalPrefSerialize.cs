﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;


[System.Serializable]
public class GlobalValues
{
    public List<int> lockCharacterID;
    public List<int> lockConsumableID;
    public List<int> lockItemID;
    public List<int> lockSkillID;
    public List<int> lockMapID;
    public List<int> lockEnemyID;
    public List<int> lockEventID;
    public List<int> lockPowerID;
    public List<int> lockToolkitsID;
    public bool firstGame = true;

    public string GameSpeed;
}




public static class GlobalPrefSerialize
{

    public static GlobalValues globalValues;
    private static string globalValuesKey = "GlobalValues";

    public static void LoadPlayerValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(GlobalValues));
        string text = PlayerPrefs.GetString(globalValuesKey);
        if (text.Length == 0)
        {
            globalValues = new GlobalValues();
        }
        else
        {
            using (var reader = new System.IO.StringReader(text))
            {
                globalValues = serializer.Deserialize(reader) as GlobalValues;
            }
        }


    }

    public static void SaveGlobalValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(GlobalValues));
        using (StringWriter sw = new StringWriter())
        {
            serializer.Serialize(sw, globalValues);
            PlayerPrefs.SetString(globalValuesKey, sw.ToString());
        }
    }

    public static void ResetGlobalValues()
    {
        PlayerPrefs.SetString(globalValuesKey, "");
        List<int> lockCharacterID = new List<int>();
        List<int> lockConsumableID = new List<int>();
        List<int> lockItemID = new List<int>();
        List<int> lockSkillID = new List<int>();
        List<int> lockMapID = new List<int>();
        List<int> lockEnemyID = new List<int>();
        List<int> lockEventID = new List<int>();
        List<int> lockPowerID = new List<int>();
        List<int> lockToolkitsID = new List<int>();
    }

    public static void UnlockCharacter(List<int> idToUnlock)
    {
        GlobalPrefSerialize.LoadPlayerValues();
        List<int> newIDList = new List<int>();
        for (int i = 0; i < globalValues.lockCharacterID.Count; i++)
        {
            for (int j = 0; j < idToUnlock.Count; i++)
            {
                if (idToUnlock[j] != globalValues.lockCharacterID[i])
                {
                    newIDList.Add(globalValues.lockCharacterID[i]);
                }
            }

        }
        globalValues.lockCharacterID.Clear();
        globalValues.lockCharacterID = newIDList;
        GlobalPrefSerialize.SaveGlobalValues();
    }
    public static void UnlockCosumables(List<int> idToUnlock)
    {
        GlobalPrefSerialize.LoadPlayerValues();
        List<int> newIDList = new List<int>();
        for (int i = 0; i < globalValues.lockConsumableID.Count; i++)
        {
            for (int j = 0; j < idToUnlock.Count; i++)
            {
                if (idToUnlock[j] != globalValues.lockConsumableID[i])
                {
                    newIDList.Add(globalValues.lockConsumableID[i]);
                }
            }

        }
        globalValues.lockConsumableID.Clear();
        globalValues.lockConsumableID = newIDList;
        GlobalPrefSerialize.SaveGlobalValues();
    }
    public static void UnlockItems(List<int> idToUnlock)
    {
        GlobalPrefSerialize.LoadPlayerValues();
        List<int> newIDList = new List<int>();
        for (int i = 0; i < globalValues.lockItemID.Count; i++)
        {
            for (int j = 0; j < idToUnlock.Count; i++)
            {
                if (idToUnlock[j] != globalValues.lockItemID[i])
                {
                    newIDList.Add(globalValues.lockItemID[i]);
                }
            }

        }
        globalValues.lockItemID.Clear();
        globalValues.lockItemID = newIDList;
        GlobalPrefSerialize.SaveGlobalValues();
    }
    public static void UnlockSkills(List<int> idToUnlock)
    {
        GlobalPrefSerialize.LoadPlayerValues();
        List<int> newIDList = new List<int>();
        for (int i = 0; i < globalValues.lockSkillID.Count; i++)
        {
            for (int j = 0; j < idToUnlock.Count; i++)
            {
                if (idToUnlock[j] != globalValues.lockSkillID[i])
                {
                    newIDList.Add(globalValues.lockSkillID[i]);
                }
            }

        }
        globalValues.lockSkillID.Clear();
        globalValues.lockSkillID = newIDList;
        GlobalPrefSerialize.SaveGlobalValues();
    }
    public static void UnlockMap(List<int> idToUnlock)
    {
        GlobalPrefSerialize.LoadPlayerValues();
        List<int> newIDList = new List<int>();
        for (int i = 0; i < globalValues.lockMapID.Count; i++)
        {
            for (int j = 0; j < idToUnlock.Count; i++)
            {
                if (idToUnlock[j] != globalValues.lockMapID[i])
                {
                    newIDList.Add(globalValues.lockMapID[i]);
                }
            }

        }
        globalValues.lockMapID.Clear();
        globalValues.lockMapID = newIDList;
        GlobalPrefSerialize.SaveGlobalValues();
    }
    public static void UnlockEnemy(List<int> idToUnlock)
    {
        GlobalPrefSerialize.LoadPlayerValues();
        List<int> newIDList = new List<int>();
        for (int i = 0; i < globalValues.lockEnemyID.Count; i++)
        {
            for (int j = 0; j < idToUnlock.Count; i++)
            {
                if (idToUnlock[j] != globalValues.lockEnemyID[i])
                {
                    newIDList.Add(globalValues.lockEnemyID[i]);
                }
            }

        }
        globalValues.lockEnemyID.Clear();
        globalValues.lockEnemyID = newIDList;
        GlobalPrefSerialize.SaveGlobalValues();
    }
    public static void UnlockEvent(List<int> idToUnlock)
    {
        GlobalPrefSerialize.LoadPlayerValues();
        List<int> newIDList = new List<int>();
        for (int i = 0; i < globalValues.lockEventID.Count; i++)
        {
            for (int j = 0; j < idToUnlock.Count; i++)
            {
                if (idToUnlock[j] != globalValues.lockEventID[i])
                {
                    newIDList.Add(globalValues.lockEventID[i]);
                }
            }

        }
        globalValues.lockEventID.Clear();
        globalValues.lockEventID = newIDList;
        GlobalPrefSerialize.SaveGlobalValues();
    }
    public static void LockIfFirstGame()
    {
        GlobalPrefSerialize.LoadPlayerValues();
        int[] lockCharacterID = new int[] { };
        globalValues.lockCharacterID = lockCharacterID.OfType<int>().ToList();

        int[] lockConsumableID = new int[] { };
        globalValues.lockConsumableID = lockConsumableID.OfType<int>().ToList();

        int[] lockItemID = new int[] { };
        globalValues.lockItemID = lockItemID.OfType<int>().ToList();

        int[] lockSkillID = new int[] { };
        globalValues.lockSkillID = lockSkillID.OfType<int>().ToList();

        int[] lockMapID = new int[] { };
        globalValues.lockMapID = lockMapID.OfType<int>().ToList();

        int[] lockEnemyID = new int[] { };
        globalValues.lockEnemyID = lockEnemyID.OfType<int>().ToList();

        int[] lockEventID = new int[] { };
        globalValues.lockEventID = lockEventID.OfType<int>().ToList();

        globalValues.firstGame = false;
        GlobalPrefSerialize.SaveGlobalValues();
    }

}







