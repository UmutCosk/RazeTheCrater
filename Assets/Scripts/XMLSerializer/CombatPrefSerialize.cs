﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;


[System.Serializable]
public class CombatValues
{   
    // Combat
    public int startFightAgainst;

    // Create Enemys
 
    public List<int> EnemySetIDsDone = new List<int>();
    public int CurrentEnemySetID;
    public List<int> EnemySetList = new List<int>();

    //Enemys Already Killed
    public List<int> UniqueEnemiesKilled = new List<int>();
    //Elite killed
    public int EliteAlreadyKilled;

    //Permanent Attributes
    public int permanentStrength;

    //Temp Attributes
    public int tempEnergy;
    public int tempShield;
    public int tempArmor;
    public int tempBroken;
    public int tempWeaken;
    public int tempStrength;
    public int tempStability;

    //Cooldown SupportSkills
    public int supportCD1 = 0;
    public int supportCD2 = 0;
    public int supportCD3 = 0;
    public int supportCD4 = 0;
    public int supportCD5 = 0;
    public int supportCD0 = 0;


}

public static class CombatPrefSerialize
{

    public static CombatValues combatValues;
    private static string combatValuesKey = "Combat";

    public static void LoadCombatValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(CombatValues));
        string text = PlayerPrefs.GetString(combatValuesKey);
        if (text.Length == 0)
        {
            combatValues = new CombatValues();
        }
        else
        {
            using (var reader = new System.IO.StringReader(text))
            {
                combatValues = serializer.Deserialize(reader) as CombatValues;
            }
        }


    }

    public static void SetNewSupportCD(int value, int position)
    {
        CombatPrefSerialize.LoadCombatValues();
        switch (position)
        {
            case 0:
                combatValues.supportCD0 = value;
                break;
            case 1:
                combatValues.supportCD1 = value;
                break;
            case 2:
                combatValues.supportCD2 = value;
                break;
            case 3:
                combatValues.supportCD3 = value;
                break;
            case 4:
                combatValues.supportCD4 = value;
                break;
            case 5:
                combatValues.supportCD5 = value;
                break;
        }
        CombatPrefSerialize.SaveCombatValues();
    }
    public static int GetSupportCD(int position)
    {
        CombatPrefSerialize.LoadCombatValues();
        int value = 0;
        switch (position)
        {
            case 0:
                value = combatValues.supportCD0;
                break;
            case 1:
                value = combatValues.supportCD1;
                break;
            case 2:
                value = combatValues.supportCD2;
                break;
            case 3:
                value = combatValues.supportCD3;
                break;
            case 4:
                value = combatValues.supportCD4;
                break;
            case 5:
                value = combatValues.supportCD5;
                break;
        }
        return value;
    }
    public static void SaveCombatValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(CombatValues));
        using (StringWriter sw = new StringWriter())
        {
            serializer.Serialize(sw, combatValues);
            PlayerPrefs.SetString(combatValuesKey, sw.ToString());
        }
    }

    public static void ResetCombatValues()
    {
        PlayerPrefs.SetString(combatValuesKey, "");
    }

 

  

}
