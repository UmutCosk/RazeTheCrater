﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine.UI;


[System.Serializable]
public class PlayerValues
{
    //WhichPlayer?
    public CharacterType characterType;

    //QuitWhileInCombat?
    public bool quitInCombat = false;

    //Combat
    public int maxHP;
    public int curHP;
    public int maxEnergyExtra;

    //Items , Consumables, Skills
    public int Gold;
    public List<int> Deck;


    //Einstellungen
    public List<int> Items;
    public int maxConsumableSlots;
    public int[] ConsumableSlotNR;
    public int ConsumableCounter;

    public List<int> RoomsInStep;
    //public List<SkillType> RoomTypeArray;




}




public static class PlayerPrefSerialize
{

    public static PlayerValues playerValues;
    private static string playerValuesKey = "PlayerValues";

    public static void LoadPlayerValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(PlayerValues));
        string text = PlayerPrefs.GetString(playerValuesKey);
        if (text.Length == 0)
        {
            playerValues = new PlayerValues();
        }
        else
        {
            using (var reader = new System.IO.StringReader(text))
            {
                playerValues = serializer.Deserialize(reader) as PlayerValues;
            }
        }


    }

    public static void SavePlayerValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(PlayerValues));
        using (StringWriter sw = new StringWriter())
        {
            serializer.Serialize(sw, playerValues);
            PlayerPrefs.SetString(playerValuesKey, sw.ToString());
        }
    }

    public static void ResetPlayerValues()
    {
        PlayerPrefs.SetString(playerValuesKey, "");
        playerValues.Deck = new List<int>();
        playerValues.Items = new List<int>();
        playerValues.ConsumableSlotNR = new int[] { 9999, 9999, 9999, 9999, 9999 };
    }


    public static void LoseHP(int damage, float percentDamage)
    {

        if (damage > 0)
        {
            playerValues.curHP -= damage;
            PlayerPrefSerialize.SavePlayerValues();
            if (playerValues.curHP <= 0)
            {
                Debug.Log("VERLOREN EVENT!");
            }


        }
        else if (percentDamage > 0)
        {
            int hpToLose = Mathf.RoundToInt(playerValues.maxHP * percentDamage);
            playerValues.curHP -= hpToLose;
            PlayerPrefSerialize.SavePlayerValues();
            if (playerValues.curHP <= 0)
            {
                Debug.Log("VERLOREN EVENT!");
            }

        }


    }

    public static void AddCardIDList(List<int> CardListID)
    {
        for (int i = 0; i < CardListID.Count; i++)
        {
            playerValues.Deck.Add(CardListID[i]);
        }
        PlayerPrefSerialize.SavePlayerValues();
    }
    public static void AddItem(int itemID)
    {
        playerValues.Items.Add(itemID);
        PlayerPrefSerialize.SavePlayerValues();
        GameObject.FindGameObjectWithTag("ItemPanel").GetComponent<LoadItemPanel>().UpdateItemPanel();

    }



    public static void GetGold(int goldvalue)
    {

        playerValues.Gold += goldvalue;
        PlayerPrefSerialize.SavePlayerValues();
        GameObject.FindGameObjectWithTag("Misc").transform.GetChild(2).GetComponent<MiscGold>().UpdateGold();
    }

    public static int HealHP(int value)
    {
        int healed;
        if (playerValues.curHP + value > playerValues.maxHP)
        {
            healed = playerValues.maxHP - playerValues.curHP;
            playerValues.curHP = playerValues.maxHP;
        }
        else
        {
            playerValues.curHP = playerValues.curHP + value;
            healed = value;
        }
        PlayerPrefSerialize.SavePlayerValues();

        return healed;

    }

    public static bool PayGold(int goldvalue)
    {
        if (playerValues.Gold - goldvalue < 0)
        {
            return false;
        }
        else
        {
            playerValues.Gold = playerValues.Gold - goldvalue;
            PlayerPrefSerialize.SavePlayerValues();
            return true;
        }
    }


    public static void RemoveItemByID(int itemID)
    {
        PlayerPrefSerialize.LoadPlayerValues();
        List<int> newItemList = new List<int>();

        for (int i = 0; i < playerValues.Items.Count; i++)
        {
            if (playerValues.Items[i] != itemID)
            {
                newItemList.Add(playerValues.Items[i]);
            }
        }
        playerValues.Items.Clear();
        playerValues.Items = newItemList;
        PlayerPrefSerialize.SavePlayerValues();
    }


    public static void RaiseMaxHP(int value)
    {

        playerValues.maxHP += value;
        PlayerPrefSerialize.HealHP(value);
        PlayerPrefSerialize.SavePlayerValues();
        GameObject.FindGameObjectWithTag("Misc").transform.GetChild(1).GetComponent<MiscHP>().UpdateHP();
    }


    public static void AddConsumable(int id)
    {
        for (int i = 0; i < playerValues.ConsumableSlotNR.Length; i++)
        {
            if (playerValues.ConsumableSlotNR[i] == 9999)
            {
                playerValues.ConsumableSlotNR[i] = id;
                PlayerPrefSerialize.SavePlayerValues();
                break;
            }
        }

    }
    public static void DiscardConsumable(int slotnumber)
    {
        playerValues.ConsumableSlotNR[slotnumber] = 9999;
        GameObject.FindGameObjectWithTag("ConsumablePanel").transform.GetChild(slotnumber).GetChild(0).GetComponent<Image>().sprite = null;
        EventManager2.DestroyConsumable(GameObject.FindGameObjectWithTag("ConsumablePanel").transform.GetChild(slotnumber).GetChild(0).GetComponent<Consumable>());
        PlayerPrefSerialize.SavePlayerValues();
        EventManager2.chosenConsumable = null;
    }




}
