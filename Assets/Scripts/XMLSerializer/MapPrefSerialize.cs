﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class MapValues
{
    public int NewGame;
    public int CurrentStep;
    public int CurrentLevel;
    public MonsterType CurrentMonsterType;

    //QuestionRoom
    public List<int> EventIDAlreadyDone;


    //Map0
    public List<int> RoomsInStep;
    public List<string> RoomTypes;






}




public static class MapPrefSerialize
{

    public static MapValues mapValues;
    private static string mapValuesKey = "MapValues";

    public static void LoadMapValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(MapValues));
        string text = PlayerPrefs.GetString(mapValuesKey);
        if (text.Length == 0)
        {
            mapValues = new MapValues();
        }
        else
        {
            using (var reader = new System.IO.StringReader(text))
            {
                mapValues = serializer.Deserialize(reader) as MapValues;
            }
        }


    }

    public static void SaveMapValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(MapValues));
        using (StringWriter sw = new StringWriter())
        {
            serializer.Serialize(sw, mapValues);
            PlayerPrefs.SetString(mapValuesKey, sw.ToString());
        }
    }

    public static void ResetMapValues()
    {
        PlayerPrefs.SetString(mapValuesKey, "");
    }

   
    /*   public static void DeactivateAllRoomsInMap1()
      {

          for (int i = 0; i < 8; i++)
          {
              mapValues.activeMap1Room1[i] = false;
              mapValues.activeMap1Room2[i] = false;
              mapValues.activeMap1Room3[i] = false;
          }

      }
   */

}
