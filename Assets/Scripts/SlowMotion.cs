﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class SlowMotion : MonoBehaviour
{

    float targetSlowFactor;
    float curSlowFactor;
    public bool toggle;
    public bool sniper;
    void Start()
    {
        DOTween.Init();
        Application.targetFrameRate = 60;
        toggle = true;
        targetSlowFactor = 0.05f;
        curSlowFactor = 1f;
        sniper = false;
    }

    void SetBlackScreen(bool on)
    {
        if (on)
        {
            DOTween.Pause("blackscreenon");
            DOTween.Restart("blackscreenoff");
        }
        else
        {
            DOTween.Pause("blackscreenoff");
            DOTween.Restart("blackscreenon");
        }
    }


    public void ToggleOn()
    {

        curSlowFactor = 1;
        targetSlowFactor = 0.05f;
        SetBlackScreen(true);
        StartCoroutine(DoSlowMotion(curSlowFactor, targetSlowFactor, 0.01f));
        GameObject.FindGameObjectWithTag("Focus").transform.GetChild(0).GetComponent<FocusCounter>().StartTimer();
    }
    public void ToggleOff()
    {

        curSlowFactor = 0.05f;
        targetSlowFactor = 1;

        SetBlackScreen(false);
        GameObject.Find("Canvas").transform.GetChild(8).gameObject.SetActive(false);
        StartCoroutine(DontSlowMotion(curSlowFactor, targetSlowFactor, 0.01f));
        GameObject.FindGameObjectWithTag("Focus").transform.GetChild(0).GetComponent<FocusCounter>().ResetCooldown();

        //Sniper Values
        sniper = false;
    }




    IEnumerator DoSlowMotion(float curSlowFactor, float targetSlowFactor, float delay)
    {
        yield return new WaitForSeconds(delay);
        curSlowFactor -= 0.05f;
        Time.timeScale = curSlowFactor;
        this.GetComponent<AudioSource>().pitch = curSlowFactor;

        if (targetSlowFactor < curSlowFactor)
        {
            StartCoroutine(DoSlowMotion(curSlowFactor, targetSlowFactor, delay));
        }
        GameObject.FindGameObjectWithTag("Focus").transform.GetChild(0).GetComponent<FocusCounter>().StartTimer();


    }

    IEnumerator DontSlowMotion(float curSlowFactor, float targetSlowFactor, float delay)
    {
        GameObject.Find("Canvas").transform.GetChild(8).gameObject.SetActive(false);
        yield return new WaitForSeconds(delay);
        curSlowFactor += 0.05f;
        Time.timeScale = curSlowFactor;
        this.GetComponent<AudioSource>().pitch = curSlowFactor;

        if (targetSlowFactor > curSlowFactor)
        {
            StartCoroutine(DontSlowMotion(curSlowFactor, targetSlowFactor, delay));
        }
        GameObject.FindGameObjectWithTag("Focus").transform.GetChild(0).GetComponent<FocusCounter>().ResetCooldown();
        GameObject.Find("SkillPanel").transform.GetChild(8).gameObject.SetActive(false);
        EventManager2.chosenCard = null;
    }

    void Update()
    {
        if (sniper)
        {
            GameObject.Find("Canvas").transform.GetChild(8).gameObject.SetActive(true);
        }
        else
        {
            GameObject.Find("Canvas").transform.GetChild(8).gameObject.SetActive(false);
        }
        if (Input.GetMouseButtonDown(1) && GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().curFocus > 0 || Input.GetMouseButtonDown(1) && !toggle)
        {
            if (!sniper)
            {
                if (toggle)
                {
                    toggle = false;
                    GameObject.FindGameObjectWithTag("Focus").GetComponent<SetFocus>().ReduceFocus(1);
                    curSlowFactor = 1;
                    targetSlowFactor = 0.05f;
                    SetBlackScreen(true);
                    StartCoroutine(DoSlowMotion(curSlowFactor, targetSlowFactor, 0.01f));


                }
                else if (!toggle)
                {

                    toggle = true;
                    curSlowFactor = 0.05f;
                    targetSlowFactor = 1;
                    SetBlackScreen(false);
                    StartCoroutine(DontSlowMotion(curSlowFactor, targetSlowFactor, 0.01f));
                }
            }
        }
    }
}
