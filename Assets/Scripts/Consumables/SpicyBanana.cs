﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpicyBanana : Consumable
{

    int powerGain = 3;

    void Start()
    {
        description = "Gain " + powerGain + " Power";
    }

    public override void FruitEffect(GameObject target)
    {
      GetBuffed(target, BuffType.Power, powerGain, 0);
    }
}
