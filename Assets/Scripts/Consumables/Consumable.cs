using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public enum ConsumableType { Ink, Fruit }
public enum ConsumableColor { orange, purple, red, blue, green }
public class Consumable : MonoBehaviour
{

    public int ConsumableSlotNR;

    public int id;
    public string title;
    public string description;
    public string imagePath;
    public TargetStyle targetType;
    public ConsumableType consumableType;
    public ConsumableColor consumableColor;
    public string scriptName;


    public Skill skill;
    void Start()
    {

    }

    public virtual void FruitEffect(GameObject target) { }

    public virtual void InkEffect(GameObject scroll) { }

    public void DamageEnemy(GameObject target, TargetStyle targetType, int damage, float delay)
    {
        StartCoroutine(DamageWithConsumable2(target, targetType, damage, delay));
    }

    public IEnumerator DamageWithConsumable2(GameObject target, TargetStyle targetType, int damage, float delay)
    {
        yield return new WaitForSeconds(0.1f);
        //Calc extra Damage from Buffs & Items
        if (targetType == TargetStyle.PureSingle)
        {
            target.GetComponent<BaseEnemy>().GetPureDamage(damage);
        }
        //Discard
        ConsumableEvent.DiscardConsumable();

    }

    public void GetBuffed(GameObject usingObject, BuffType bufftype, int amount, float timer)
    {
        StartCoroutine(GetBuffed2(usingObject, bufftype, amount, timer));
    }

    IEnumerator GetBuffed2(GameObject usingObject, BuffType bufftype, int amount, float timer)
    {
        yield return new WaitForSeconds(timer);
        switch (bufftype)
        {
            case BuffType.Armor:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().GetArmor(amount, usingObject);
                break;
            case BuffType.Power:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().GetPower(amount);
                break;
            case BuffType.Toughness:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().GetToughness(amount);
                break;
            case BuffType.Dodge:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().GetDodge(amount);
                break;
            case BuffType.Energy:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().GainCurEnergy(amount);
                break;
            case BuffType.Heal:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().HealHP(amount);
                break;
        }
        ConsumableEvent.DiscardConsumable();
    }

    // public Vector3 zero = new Vector3(0, 0, 0);
    // public Skill skill;
    // public GameObject temp_consumable;


    // void Awake()
    // {
    //     skill = GameObject.Find("Database").GetComponent<Skill>();
    // }

    // void Start()
    // {

    //     switch (this.gameObject.scene.name)
    //     {
    //         case "Shop":
    //             break;
    //         case "Combat":
    //             ConsumableSlotNR = this.GetComponent<LoadConsumables>().slotNR;
    //             break;

    //     }
    // }

    // public int GetChoosenEnemyPosition()
    // {
    //     return EventManager.chosenEnemy.GetComponent<BaseEnemy>().position;
    // }

    // public virtual void ConsumableEffect()
    // {


    // }
    // public virtual void ConsumableEffectTimer(GameObject chosenEnemy, int damageTimer)
    // {


    // }

    // public virtual void Cooldown()
    // {


    // }

    // public virtual void TriggerCountdown()
    // {


    // }

    // public void SpawnFamiliar(int hp)
    // {
    //     //Animation Spawn
    //     GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/FamiliarFX");
    //     Vector3 temp_position2 = GameObject.Find("FamiliarSpawnPoint").transform.position;
    //     temp_position2.z = 0;
    //     GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //     animation_prefab_clone2.transform.localRotation = Quaternion.Euler(-90f, -90f, 0f);
    //     Destroy(animation_prefab_clone2, 3f);
    //     StartCoroutine(Spawn(0.5f, animation_prefab_clone2, hp));

    // }
    // IEnumerator Spawn(float timer, GameObject clone, int hp)
    // {
    //     yield return new WaitForSeconds(timer);
    //     //Bild aktivieren
    //     GameObject Familiar = GameObject.Find("Player").transform.GetChild(7).gameObject;
    //     Familiar.SetActive(true);
    //     Familiar.GetComponent<Image>().sprite = Resources.Load<Sprite>("Consumables/Pictures/best_friend");
    //     Color alpha = Familiar.GetComponent<Image>().color;
    //     Familiar.GetComponent<Image>().color = alpha;
    //     Familiar.transform.GetComponent<BaseFamiliar>().SetHP(hp);
    //     Familiar.transform.GetChild(0).GetComponent<SetFamiliarHP>().SetHP();
    //     Familiar.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<SetFamiliarHP>().SetHP();
    // }


    // public GameObject GetConsumableSlotNrByName(string name)
    // {
    //     int counter = 0;
    //     int result = 0;
    //     foreach (Transform child in GameObject.Find("ConsumablesPrefab").gameObject.transform)
    //     {

    //         if (child.name == name)
    //         {
    //             result = counter;
    //         }
    //         counter++;
    //     }

    //     return GameObject.Find("ConsumablesPrefab").transform.GetChild(result).gameObject;
    // }

    // public void SetConsumableTimer(GameObject consumable)
    // {
    //     skill = GameObject.Find("Database").GetComponent<Skill>();
    //     if (skill.chosenConsumableTimer1 == null)
    //     {
    //         skill.chosenConsumableTimer1 = consumable;
    //     }
    //     else if (skill.chosenConsumableTimer2 == null)
    //     {
    //         skill.chosenConsumableTimer2 = consumable;
    //     }
    //     else if (skill.chosenConsumableTimer3 == null)
    //     {
    //         skill.chosenConsumableTimer3 = consumable;
    //     }
    //     else if (skill.chosenConsumableTimer4 == null)
    //     {
    //         skill.chosenConsumableTimer4 = consumable;
    //     }
    //     else if (skill.chosenConsumableTimer5 == null)
    //     {
    //         skill.chosenConsumableTimer5 = consumable;
    //     }
    // }

    // public bool ListContains(List<int> list, int value)
    // {
    //     bool result = false;
    //     for (int i = 0; i < list.Count; i++)
    //     {
    //         if (list[i] == value)
    //         {
    //             return true;
    //         }
    //     }
    //     return result;
    // }

}