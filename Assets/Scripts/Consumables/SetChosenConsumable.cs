﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;
using TMPro;

public class SetChosenConsumable : MonoBehaviour, IPointerClickHandler
{

    public IEnumerator TurnOffOnDelay(GameObject consumable, bool turnOn)
    {
        float timer = 0;
        if (turnOn)
        {
            timer = 0.15f;
        }
        else
        {
            timer = 0.035f;
        }
        yield return new WaitForSeconds(timer);

        if (turnOn)
        {
            consumable.SetActive(true);
        }
        else
        {
            consumable.SetActive(false);
        }
    }



    public void OnPointerClick(PointerEventData eventData)
    {
        //Turn off Card
        // EventManager2.chosenCard = null;


        GameObject.FindGameObjectWithTag("ConsumablePanel").GetComponent<UpdateConsumablePanel>().panelActive = true;
        if (this.GetComponent<Consumable>() != null)
        {
            foreach (Transform child in GameObject.FindGameObjectWithTag("ConsumablePanel").gameObject.transform)
            {

                if (child.GetChild(0).gameObject.name != this.gameObject.name)
                {
                    StartCoroutine(TurnOffOnDelay(child.GetChild(0).GetChild(0).gameObject, false));
                }
            }
            StartCoroutine(TurnOffOnDelay(this.gameObject.transform.GetChild(0).gameObject, true));

            if (this.GetComponent<Consumable>().consumableType == ConsumableType.Fruit)
            {
                if (this.GetComponent<Consumable>().targetType == TargetStyle.None)
                {
                    this.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = "Eat";
                }
                else
                {
                    this.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = "Throw";
                }
            }
            else
            {
                this.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = "Apply";
            }
        }
        else
        {
            foreach (Transform child in GameObject.FindGameObjectWithTag("ConsumablePanel").gameObject.transform)
            {
                StartCoroutine(TurnOffOnDelay(child.GetChild(0).GetChild(0).gameObject, false));
            }
        }
    }




}
