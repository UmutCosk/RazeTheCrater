﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TenderStrawberry : Consumable
{

    int healAmount = 9;
    void Start()
    {
        description = "Heal yourself by " + healAmount;
    }

    public override void FruitEffect(GameObject target)
    {
        GetBuffed(this.gameObject, BuffType.Heal, healAmount, 0);
    }


}
