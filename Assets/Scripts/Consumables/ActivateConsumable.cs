﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEditor;


public class ActivateConsumable : MonoBehaviour, IPointerClickHandler
{



    public void OnPointerClick(PointerEventData eventData)
    {
        if (this.transform.parent.parent.GetComponent<Consumable>().consumableType == ConsumableType.Fruit)
        {
            if (this.transform.parent.parent.GetComponent<Consumable>().targetType == TargetStyle.None)
            {
                this.transform.parent.parent.gameObject.GetComponent<Consumable>().FruitEffect(GameObject.FindGameObjectWithTag("Gamer").gameObject);
                ConsumableEvent.DiscardConsumable();
                TurnOffOtherConsumablePanels();

            }
            else
            {
                TurnOffOtherConsumablePanels();
                EventManager2.chosenConsumable = this.transform.parent.parent.gameObject;
            }
        }
        else if (this.transform.parent.parent.GetComponent<Consumable>().consumableType == ConsumableType.Ink)
        {
            TurnOffOtherConsumablePanels();
            EventManager2.chosenConsumable = this.transform.parent.parent.gameObject;
        }
    }

    void TurnOffOtherConsumablePanels()
    {
        foreach (Transform child in GameObject.FindGameObjectWithTag("ConsumablePanel").gameObject.transform)
        {
            child.GetChild(0).GetChild(0).gameObject.SetActive(false);
        }
    }
}


