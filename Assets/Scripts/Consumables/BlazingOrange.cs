﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlazingOrange : Consumable
{
    int damageValue = 15;

    // Use this for initialization
    void Start()
    {
        description = "Deal " + damageValue + " damage to an enemy";
    }

    public override void FruitEffect(GameObject target)
    {

        DamageEnemy(target, this.targetType, damageValue, 0);

    }
}
