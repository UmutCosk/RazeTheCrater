﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateConsumablePanel : MonoBehaviour
{
    public bool panelActive;
    // Use this for initialization
    void Start()
    {
        panelActive = false;
        UpdateConsumable();
    }

    void Update()
    {
     /*    if (panelActive && Input.GetMouseButtonDown(0))
        {
            StartCoroutine(Delay());
        } */
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.1f);
        panelActive = false;
        foreach (Transform child in GameObject.FindGameObjectWithTag("ConsumablePanel").gameObject.transform)
        {
            if (child.GetChild(0).gameObject.name != this.gameObject.name)
            {
                child.GetChild(0).GetChild(0).gameObject.SetActive(false);
            }
        }
    }


    // Update is called once per frame
    public void UpdateConsumable()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        for (int i = 0; i < 5; i++)
        {
            int id = PlayerPrefSerialize.playerValues.ConsumableSlotNR[i];
            if (id != 9999)
            {
                EventManager2.AddGameComponent(GameObject.FindGameObjectWithTag("ConsumablePanel").transform.GetChild(i).GetChild(0).gameObject, id, "Consumable");
            }
        }
    }

    public void UpdateCharges()
    {

    }



}
