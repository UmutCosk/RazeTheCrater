﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AquaPear : Consumable
{

    int energyGain = 2;
    void Start()
    {
        description = "Gain " + energyGain + " energy";
    }

    public override void FruitEffect(GameObject target)
    {
        GetBuffed(this.gameObject, BuffType.Energy, energyGain, 0);
    }
}
