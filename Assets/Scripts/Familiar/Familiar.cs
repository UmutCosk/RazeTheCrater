﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Familiar : MonoBehaviour
{

    public int id;
    public string title;
    public string scriptName;
    public string imagePath;
    public int size;

    public int damage1;
    public int damage2;
    public int damage3;
    public int damage4;
    public int damage5;

    public int damageDone;

    public GameObject enemyToAtack;

    public Intention[] intentionSequenz;
    public int[] attackSequenz;
    public int[] damageSequenz;
    public int sequenzCounter = 0;

    public void ChooseRandomEnemyForAttack()
    {
        int enemyChildCount = GameObject.FindGameObjectWithTag("Enemys").transform.childCount;
        enemyToAtack = GameObject.FindGameObjectWithTag("Enemys").transform.GetChild(Random.Range(0, enemyChildCount)).gameObject;

    }


    public void DamageEnemy(int damage, float delay)
    {
        StartCoroutine(DamageEnemy2(damage, delay));

    }
    IEnumerator DamageEnemy2(int damage, float delay)
    {
        damageDone = 0;
        yield return new WaitForSeconds(delay);
        damage = CalcBuffsForIntention(damage);
        damageDone = enemyToAtack.GetComponent<BaseEnemy>().GetDamage(damage); //Hier noch buffs Calculieren

    }


    public void SetDamage(int[] damageArray)
    {

        for (int i = 0; i < damageArray.Length; i++)
        {
            switch (i)
            {
                case 0:
                    damage1 = damageArray[i];
                    break;
                case 1:
                    damage2 = damageArray[i];
                    break;
                case 2:
                    damage3 = damageArray[i];
                    break;
                case 3:
                    damage4 = damageArray[i];
                    break;
                case 4:
                    damage5 = damageArray[i];
                    break;
            }
        }

    }

    public void SetFamiliarHP(int hp_value)
    {
        this.GetComponent<BaseFamiliar>().maxHP = hp_value;
        this.GetComponent<BaseFamiliar>().curHP = hp_value;
    }

    int GetRandomSequenz(int sequenz)
    {
        int sequenzNR = 0;
        int counter = 0;
        List<int> seqList = new List<int>();

        if (sequenz >= 100)
        {
            while (sequenz >= 100)
            {
                sequenz -= 100;
                counter++;
            }
            seqList.Add(counter);
            counter = 0;
            sequenzNR++;
        }
        if (sequenz >= 10)
        {
            while (sequenz >= 10)
            {
                sequenz -= 100;
                counter++;
            }
            sequenzNR++;
            seqList.Add(counter);
        }
        sequenzNR++;
        seqList.Add(sequenz);
        sequenzNR = Random.Range(0, sequenzNR);
        return sequenzNR;
    }

    public void ChooseAttack(int attackSeq, GameObject Familiar)
    {
        Familiar.GetComponent<Familiar>().ChooseRandomEnemyForAttack();

        if (attackSeq > 9)
        {
            attackSeq = GetRandomSequenz(attackSeq);
        }
        switch (attackSeq)
        {
            case 1:
                Familiar.GetComponent<Familiar>().AttackNumber1();
                break;
            case 2:
                Familiar.GetComponent<Familiar>().AttackNumber2();
                break;
            case 3:
                Familiar.GetComponent<Familiar>().AttackNumber3();
                break;
            case 4:
                Familiar.GetComponent<Familiar>().AttackNumber4();
                break;
            case 5:
                Familiar.GetComponent<Familiar>().AttackNumber5();
                break;
        }
        Familiar.GetComponent<Familiar>().sequenzCounter++;
        if (Familiar.GetComponent<Familiar>().sequenzCounter == attackSequenz.Length)
        {
            Familiar.GetComponent<Familiar>().sequenzCounter = 0;
        }

    }
    public virtual void FamiliarAttack()
    {

    }
    public virtual void AttackNumber1()
    {

    }
    public virtual void AttackNumber2()
    {

    }
    public virtual void AttackNumber3()
    {

    }
    public virtual void AttackNumber4()
    {

    }
    public virtual void AttackNumber5()
    {

    }

    public virtual void AfterFamiliarGotAttacked()
    {

    }

    public virtual void AfterUsingScroll()
    {

    }

    public void UpdateFamiliarIntention()
    {

        int curSequenz = this.gameObject.GetComponent<Familiar>().sequenzCounter;
        if (this.gameObject.GetComponent<Familiar>().intentionSequenz[curSequenz] == Intention.Attack || this.gameObject.GetComponent<Familiar>().intentionSequenz[curSequenz] == Intention.AttackDefend || this.gameObject.GetComponent<Familiar>().intentionSequenz[curSequenz] == Intention.AttackBuff || this.gameObject.GetComponent<Familiar>().intentionSequenz[curSequenz] == Intention.AttackDebuff)
        {
            this.gameObject.transform.GetChild(0).GetChild(5).GetChild(0).gameObject.SetActive(true);
            int value = CalcBuffsForIntention(this.gameObject.GetComponent<Familiar>().damageSequenz[curSequenz]);
            this.gameObject.transform.GetChild(0).GetChild(5).GetChild(0).GetComponent<TextMeshProUGUI>().text = value.ToString();
        }
        else
        {
            this.gameObject.transform.GetChild(0).GetChild(5).GetChild(0).gameObject.SetActive(false);
        }
        this.gameObject.transform.GetChild(0).GetChild(5).GetComponent<SetFamiliarIntention>().SetIntention(this.gameObject.GetComponent<Familiar>().intentionSequenz[curSequenz]);
    }

    public int CalcBuffsForIntention(int damageSequenz)
    {
        /*    
        foreach () //Buffs //Debuffs
           {

           }
            */
        return damageSequenz;
    }




}
