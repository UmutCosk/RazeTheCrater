﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Familiar1 : Familiar
{

    void Awake()
    {
        SetFamiliarHP(37);
        intentionSequenz = new Intention[] { Intention.Attack, Intention.Buff };
        attackSequenz = new int[] { 1, 2 };
        damageSequenz = new int[] { 8, 4 };

        SetDamage(damageSequenz);
    }

    public override void FamiliarAttack()
    {

        ChooseAttack(attackSequenz[sequenzCounter], this.gameObject);


    }

    public override void AttackNumber1()
    {
        DamageEnemy(damage1, 0.5f);
        //   EndTurnInSeconds(1f);
    }

    public override void AttackNumber2()
    {


    }


}
