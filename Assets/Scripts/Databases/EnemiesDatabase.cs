
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;
using System.IO;

public class EnemiesDatabase : MonoBehaviour
{

    public List<Enemy> enemies = new List<Enemy>();
    private JsonData jsonEnemiesDB; // Object that holds the json Data that is pulled

    void Awake()
    {
        //converts from json and passes it as an object
        jsonEnemiesDB = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/EnemiesDB.json"));
        CreateEnemyObjects();
    }




    //fetch item
    public Enemy GetEnemyByID(int id)
    {

        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i].id == id)
            {

                return enemies[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }

    public Enemy GetEnemyByScriptName(string scriptName)
    {

        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i].scriptName == scriptName)
            {
                return enemies[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }


    // Helpermethod to sync database-values with outsided created item-components
    // @param: toSync The outside created item-component
    // @param: origin The Item from database-items
    public void SyncValues(Enemy toSync, Enemy origin)
    {
        toSync.id = origin.id;
        toSync.title = origin.title;
        toSync.scriptName = origin.scriptName;
        toSync.imagePath = origin.imagePath;
        toSync.size = origin.size;

    }






    private void CreateEnemyObjects()
    {

        for (int i = 0; i < jsonEnemiesDB.Count; i++)//looping through each item from json file
        {

            Enemy toAdd = gameObject.AddComponent<Enemy>();
            toAdd.id = (int)jsonEnemiesDB[i]["id"];
            toAdd.title = jsonEnemiesDB[i]["title"].ToString();
            toAdd.scriptName = jsonEnemiesDB[i]["scriptName"].ToString();
            toAdd.imagePath = jsonEnemiesDB[i]["imagePath"].ToString();
            toAdd.size = (int)jsonEnemiesDB[i]["size"];



            enemies.Add(toAdd);
            Destroy(toAdd);
        }
    }


}
