﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;
using System.IO;

public class QuestionRoomDatabase : MonoBehaviour
{
    private List<QuestionRoom> questionRooms = new List<QuestionRoom>(); // store all the items that we drag from the json file
    private JsonData questionRoomDB; // Object that holds the json Data that is pulled


    void Awake()
    {
        questionRoomDB = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/QuestionRoom.json")); //converts from json and passes it as an object
        CreateQuestionRoomObjects();
    }

    //fetch item
    public QuestionRoom GetQuestionRoomByID(int id)
    {
        for (int i = 0; i < questionRooms.Count; i++)
        {
            if (questionRooms[i].id == id)
            {
                return questionRooms[i]; // ist Object von class Item
            }
        }
        return null;
    }
    public QuestionRoom GetRandomEventByRarity(int rarity)
    {
        List<QuestionRoom> eventList = new List<QuestionRoom>();
        MapPrefSerialize.LoadMapValues();
        List<int> alreadyDoneEvents = MapPrefSerialize.mapValues.EventIDAlreadyDone;
    tryAgain:
        for (int i = 0; i < questionRooms.Count; i++)
        {
            if (questionRooms[i].level == 0 || questionRooms[i].level == MapPrefSerialize.mapValues.CurrentLevel)
            {
                if (questionRooms[i].rarity == rarity)
                {
                    if (!ListContains(alreadyDoneEvents, questionRooms[i].id))
                    {
                        eventList.Add(questionRooms[i]);
                    }

                }
            }
        }
        int eventNumber = eventList.Count;
        if (eventNumber == 0)
        {
            Debug.Log("KEINE EVENTS MEHR DA!");
            if (rarity == 2) { rarity = 1; }
            else if (rarity == 1) { rarity = 2; }
            goto tryAgain;
        }
        int randIndex = Random.Range(0, eventNumber);
      /*   MapPrefSerialize.LoadMapValues();
        MapPrefSerialize.mapValues.EventIDAlreadyDone.Add(eventList[randIndex].id);
        MapPrefSerialize.SaveMapValues(); */
        return eventList[randIndex];
    }


    private void CreateQuestionRoomObjects()
    {

        for (int i = 0; i < questionRoomDB.Count; i++)//looping through each item from json file
        {

            QuestionRoom toAdd = gameObject.AddComponent<QuestionRoom>();
            toAdd.id = (int)questionRoomDB[i]["id"];
            toAdd.title = questionRoomDB[i]["title"].ToString();
            toAdd.rarity = (int)questionRoomDB[i]["rarity"];
            toAdd.level = (int)questionRoomDB[i]["level"];


            questionRooms.Add(toAdd);
            Destroy(toAdd);
        }
    }
    public bool ListContains(List<int> list, int value)
    {
        bool result = false;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == value)
            {
                return true;
            }
        }
        return result;
    }



}

