
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;
using System.IO;

public class EnemySetsDatabase : MonoBehaviour
{

    public List<EnemySet> enemySets = new List<EnemySet>();
    private JsonData jsonEnemySetsDB; // Object that holds the json Data that is pulled
    EnemiesDatabase enemyDB;
    void Awake()
    {
        //converts from json and passes it as an object
        jsonEnemySetsDB = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/EnemySetsDB.json"));
        CreateEnemySetObjects();
    }




    //fetch item
    public EnemySet GetEnemySetByID(int id)
    {

        for (int i = 0; i < enemySets.Count; i++)
        {
            if (enemySets[i].id == id)
            {

                return enemySets[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }

    public void SetEnemySet()
    {
        CombatPrefSerialize.LoadCombatValues();
        CombatPrefSerialize.combatValues.EnemySetIDsDone.Clear();
        CombatPrefSerialize.combatValues.EnemySetList.Clear();
        CombatPrefSerialize.SaveCombatValues();

        MapPrefSerialize.LoadMapValues();
        int currentLevel = MapPrefSerialize.mapValues.CurrentLevel;
        MonsterType currentMonsterType = MapPrefSerialize.mapValues.CurrentMonsterType;


        List<int> chooseFrom = new List<int>();

        for (int i = 0; i < enemySets.Count; i++)
        {

            //Gib mir alle im jetzigen LevelDists (currentLevel)

            if (enemySets[i].level == currentLevel || enemySets[i].level == 0)
            {
                if (enemySets[i].monsterType == currentMonsterType)
                {
                    chooseFrom.Add(enemySets[i].id);
                }
            }

        }

        List<int> newChooseFrom = new List<int>();
        newChooseFrom = DeleteAlreadyUsedEnemySets(chooseFrom);


        int rand_index = Random.Range(0, newChooseFrom.Count);
        CombatPrefSerialize.LoadCombatValues();

        CombatPrefSerialize.combatValues.CurrentEnemySetID = enemySets[newChooseFrom[rand_index]].id;
        CombatPrefSerialize.SaveCombatValues();

        CreateEnemySetCombination();
        //SetEnemySizes();
    }

    bool ListContains(List<int> list, int value)
    {
        bool result = false;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == value)
            {
                return true;
            }
        }
        return result;
    }


    List<int> DeleteAlreadyUsedEnemySets(List<int> chooseFrom)
    {
        CombatPrefSerialize.LoadCombatValues();
        List<int> newChooseFrom = new List<int>();

        for (int i = 0; i < chooseFrom.Count; i++)
        {
            if (!ListContains(CombatPrefSerialize.combatValues.EnemySetIDsDone, chooseFrom[i]))
            {
                newChooseFrom.Add(chooseFrom[i]);
            }
        }

        return newChooseFrom;

    }


    public void CreateEnemySetCombination()
    {

        CombatPrefSerialize.LoadCombatValues();
        int temp_id = CombatPrefSerialize.combatValues.CurrentEnemySetID;
        CombatPrefSerialize.combatValues.EnemySetIDsDone.Add(temp_id);
        List<int> enemySet_list = new List<int>();
        List<int> kombination_list = new List<int>();
        int stableCount = 0;
        int kombiCount = 0;

        if (enemySets[temp_id].monsterID1 != 9999) { enemySet_list.Add(enemySets[temp_id].monsterID1); stableCount++; }
        if (enemySets[temp_id].monsterID2 != 9999) { enemySet_list.Add(enemySets[temp_id].monsterID2); stableCount++; }
        if (enemySets[temp_id].monsterID3 != 9999) { enemySet_list.Add(enemySets[temp_id].monsterID3); stableCount++; }
        if (enemySets[temp_id].monsterID4 != 9999) { enemySet_list.Add(enemySets[temp_id].monsterID4); stableCount++; }
        if (enemySets[temp_id].monsterID5 != 9999) { enemySet_list.Add(enemySets[temp_id].monsterID5); stableCount++; }
        if (enemySets[temp_id].kombiID1 != 9999) { kombination_list.Add(enemySets[temp_id].kombiID1); kombiCount++; }
        if (enemySets[temp_id].kombiID2 != 9999) { kombination_list.Add(enemySets[temp_id].kombiID2); kombiCount++; }
        if (enemySets[temp_id].kombiID3 != 9999) { kombination_list.Add(enemySets[temp_id].kombiID3); kombiCount++; }
        if (enemySets[temp_id].kombiID4 != 9999) { kombination_list.Add(enemySets[temp_id].kombiID4); kombiCount++; }
        if (enemySets[temp_id].kombiID5 != 9999) { kombination_list.Add(enemySets[temp_id].kombiID5); kombiCount++; }

        //Error case

        if (stableCount + enemySets[temp_id].kombiCounter > 5)
        {
            Debug.Log("ERROR! ZUVIELE EINHEITEN!");
        }

        if (enemySets[temp_id].kombiCounter > 0)
        {
            for (int j = 0; j < enemySets[temp_id].kombiCounter; j++)
            {
                int rand_index = Random.Range(0, kombination_list.Count);
                enemySet_list.Add(kombination_list[rand_index]);
            }
        }
        CombatPrefSerialize.LoadCombatValues();
        for (int i = 0; i < enemySet_list.Count; i++)
        {
            CombatPrefSerialize.combatValues.EnemySetList.Add(enemySet_list[i]);
        }


        CombatPrefSerialize.SaveCombatValues();

    }


    public int GetEnemySetIndexByID(int id)
    {
        int result = 9999;
        for (int i = 0; i < enemySets.Count; i++)
        {
            if (id == enemySets[i].id)
            {
                return i;
            }
        }
        return result;
    }


    public void CreateEnemySetObjects()
    {
        for (int i = 0; i < jsonEnemySetsDB.Count; i++)//looping through each item from json file
        {
            EnemySet toAdd = gameObject.AddComponent<EnemySet>();
            toAdd.id = (int)jsonEnemySetsDB[i]["id"];
            toAdd.level = (int)jsonEnemySetsDB[i]["level"];
            toAdd.monsterType = (MonsterType)System.Enum.Parse(typeof(MonsterType), jsonEnemySetsDB[i]["monsterType"].ToString());
            toAdd.monsterID1 = (int)jsonEnemySetsDB[i]["monsterIDs"]["monsterID1"];
            toAdd.monsterID2 = (int)jsonEnemySetsDB[i]["monsterIDs"]["monsterID2"];
            toAdd.monsterID3 = (int)jsonEnemySetsDB[i]["monsterIDs"]["monsterID3"];
            toAdd.monsterID4 = (int)jsonEnemySetsDB[i]["monsterIDs"]["monsterID4"];
            toAdd.monsterID5 = (int)jsonEnemySetsDB[i]["monsterIDs"]["monsterID5"];
            toAdd.kombiID1 = (int)jsonEnemySetsDB[i]["kombinationID"]["kombiID1"];
            toAdd.kombiID2 = (int)jsonEnemySetsDB[i]["kombinationID"]["kombiID2"];
            toAdd.kombiID3 = (int)jsonEnemySetsDB[i]["kombinationID"]["kombiID3"];
            toAdd.kombiID4 = (int)jsonEnemySetsDB[i]["kombinationID"]["kombiID4"];
            toAdd.kombiID5 = (int)jsonEnemySetsDB[i]["kombinationID"]["kombiID5"];
            toAdd.kombiCounter = (int)jsonEnemySetsDB[i]["kombiCounter"];
            toAdd.scale1 = float.Parse(jsonEnemySetsDB[i]["scale"]["ID1"].ToString());
            toAdd.scale2 = float.Parse(jsonEnemySetsDB[i]["scale"]["ID2"].ToString());
            toAdd.scale3 = float.Parse(jsonEnemySetsDB[i]["scale"]["ID3"].ToString());
            toAdd.scale4 = float.Parse(jsonEnemySetsDB[i]["scale"]["ID4"].ToString());
            toAdd.scale5 = float.Parse(jsonEnemySetsDB[i]["scale"]["ID5"].ToString());

            toAdd.position1.x = float.Parse(jsonEnemySetsDB[i]["position1"]["x"].ToString());
            toAdd.position1.y = float.Parse(jsonEnemySetsDB[i]["position1"]["y"].ToString());
            toAdd.position1.z = float.Parse(jsonEnemySetsDB[i]["position1"]["z"].ToString());

            toAdd.position2.x = float.Parse(jsonEnemySetsDB[i]["position2"]["x"].ToString());
            toAdd.position2.y = float.Parse(jsonEnemySetsDB[i]["position2"]["y"].ToString());
            toAdd.position2.z = float.Parse(jsonEnemySetsDB[i]["position2"]["z"].ToString());

            toAdd.position3.x = float.Parse(jsonEnemySetsDB[i]["position3"]["x"].ToString());
            toAdd.position3.y = float.Parse(jsonEnemySetsDB[i]["position3"]["y"].ToString());
            toAdd.position3.z = float.Parse(jsonEnemySetsDB[i]["position3"]["z"].ToString());

            toAdd.position4.x = float.Parse(jsonEnemySetsDB[i]["position4"]["x"].ToString());
            toAdd.position4.y = float.Parse(jsonEnemySetsDB[i]["position4"]["y"].ToString());
            toAdd.position4.z = float.Parse(jsonEnemySetsDB[i]["position4"]["z"].ToString());

            toAdd.position5.x = float.Parse(jsonEnemySetsDB[i]["position5"]["x"].ToString());
            toAdd.position5.y = float.Parse(jsonEnemySetsDB[i]["position5"]["y"].ToString());
            toAdd.position5.z = float.Parse(jsonEnemySetsDB[i]["position5"]["z"].ToString());




            enemySets.Add(toAdd);
            Destroy(toAdd);
        }
    }


}
