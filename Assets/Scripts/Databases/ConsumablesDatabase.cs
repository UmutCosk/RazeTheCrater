﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

using LitJson;
using System.IO;

public class ConsumablesDatabase : MonoBehaviour
{
    public List<Consumable> consumables = new List<Consumable>();
    private JsonData jsonConsumablesDB; // Object that holds the json Data that is pulled

    void Awake()
    {
        jsonConsumablesDB = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/ConsumablesDB.json"));
        CreateConsumablesObjects();
    }



    public List<Consumable> GetXConsumablesBySource(int amount, ConsumableType consumableType)
    {
        List<Consumable> result = new List<Consumable>();
        int[] usedValues = new int[amount];

        for (int i = 0; i < usedValues.Length; i++)
        {
            usedValues[i] = 9999;
        }

        for (int i = 0; i < amount; i++)
        {
            int randIndex = 0;
            randIndex = GetRandomConsumableBySource(consumableType).id;

            while (ArrayContains(usedValues, randIndex))
            {
                randIndex = GetRandomConsumableBySource(consumableType).id;
            }
            usedValues[i] = randIndex;
            result.Add(GetConsumableByID(randIndex));
        }
        return result;
    }

    public Consumable GetRandomConsumableBySource(ConsumableType consumableType)
    {

        int randIndex = Random.Range(0, this.consumables.Count);
        while (!(consumables[randIndex].consumableType == consumableType))
        {
            randIndex = Random.Range(0, this.consumables.Count);
        }
        return consumables[randIndex];
    }



    public Consumable GetConsumableByID(int id)
    {
        for (int i = 0; i < consumables.Count; i++)
        {
            if (consumables[i].id == id)
            {
                return consumables[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }
    private bool ArrayContains(int[] array, int value)
    {
        bool result = false;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == value)
            {
                return true;
            }
        }
        return result;
    }

    private void CreateConsumablesObjects()
    {
        for (int i = 0; i < jsonConsumablesDB.Count; i++)
        {
            Consumable toAdd = gameObject.AddComponent<Consumable>();
            toAdd.id = (int)jsonConsumablesDB[i]["id"];
            toAdd.title = jsonConsumablesDB[i]["title"].ToString();
            toAdd.imagePath = jsonConsumablesDB[i]["imagePath"].ToString();
            toAdd.targetType = (TargetStyle)System.Enum.Parse(typeof(TargetStyle), jsonConsumablesDB[i]["targetType"].ToString());
            toAdd.consumableType = (ConsumableType)System.Enum.Parse(typeof(ConsumableType), jsonConsumablesDB[i]["consumableType"].ToString());
            toAdd.consumableColor = (ConsumableColor)System.Enum.Parse(typeof(ConsumableColor), jsonConsumablesDB[i]["consumableColor"].ToString());
            toAdd.scriptName = jsonConsumablesDB[i]["scriptName"].ToString();

            //Global Prefs Lock
            GlobalPrefSerialize.LoadPlayerValues();
            if (!ListContains(GlobalPrefSerialize.globalValues.lockConsumableID, toAdd.id))
            {
                consumables.Add(toAdd);
            }
            else
            {
                //  Debug.Log("Consumable: " + toAdd.title + " is locked!");
            }
            Destroy(toAdd);
        }
    }

    // Helpermethod to sync database-values with outsided created item-components
    // @param: toSync The outside created item-component
    // @param: origin The Item from database-items
    public void SyncValues(Consumable toSync, Consumable origin)
    {
        toSync.id = origin.id;
        toSync.title = origin.title;
        toSync.imagePath = origin.imagePath;
        toSync.scriptName = origin.scriptName;
        toSync.consumableType = origin.consumableType;
        toSync.targetType = origin.targetType;
        toSync.consumableColor = origin.consumableColor;
    }
    private bool ListContains(List<int> list, int value)
    {
        bool result = false;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == value)
            {
                return true;
            }
        }
        return result;
    }

}
