
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;
using System.IO;

public class FamiliarDatabase : MonoBehaviour
{

    public List<Familiar> Familiar = new List<Familiar>();
    private JsonData jsonFamiliarDB; // Object that holds the json Data that is pulled

    void Awake()
    {
        //converts from json and passes it as an object
        jsonFamiliarDB = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/FamiliarDB.json"));
        CreateFamiliarObjects();
    }




    //fetch item
    public Familiar GetFamiliarByID(int id)
    {

        for (int i = 0; i < Familiar.Count; i++)
        {
            if (Familiar[i].id == id)
            {

                return Familiar[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }

    public Familiar GetFamiliarByScriptName(string scriptName)
    {

        for (int i = 0; i < Familiar.Count; i++)
        {
            if (Familiar[i].scriptName == scriptName)
            {
                return Familiar[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }


    // Helpermethod to sync database-values with outsided created item-components
    // @param: toSync The outside created item-component
    // @param: origin The Item from database-items
    public void SyncValues(Familiar toSync, Familiar origin)
    {
        toSync.id = origin.id;
        toSync.title = origin.title;
        toSync.scriptName = origin.scriptName;
        toSync.imagePath = origin.imagePath;
        toSync.size = origin.size;

    }






    private void CreateFamiliarObjects()
    {

        for (int i = 0; i < jsonFamiliarDB.Count; i++)//looping through each item from json file
        {

            Familiar toAdd = gameObject.AddComponent<Familiar>();
            toAdd.id = (int)jsonFamiliarDB[i]["id"];
            toAdd.title = jsonFamiliarDB[i]["title"].ToString();
            toAdd.scriptName = jsonFamiliarDB[i]["scriptName"].ToString();
            toAdd.imagePath = jsonFamiliarDB[i]["imagePath"].ToString();
            toAdd.size = (int)jsonFamiliarDB[i]["size"];



            Familiar.Add(toAdd);
            Destroy(toAdd);
        }
    }


}
