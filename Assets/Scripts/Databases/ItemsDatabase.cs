﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;
using System.IO;

public class ItemsDatabase : MonoBehaviour
{
    public List<Item> items = new List<Item>();
    private JsonData jsonItemDB; // Object that holds the json Data that is pulled
    void Awake()
    {
        //converts from json and passes it as an object
        jsonItemDB = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/ItemsDB.json"));
        CreateItemObjects();
    }
    private bool ArrayContains(int[] array, int value)
    {
        bool result = false;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == value)
            {
                return true;
            }
        }
        return result;
    }
    private bool ListContains(List<int> list, int value)
    {
        bool result = false;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == value)
            {
                return true;
            }
        }
        return result;
    }

    public List<Item> GetAllItemsBySource(ItemTier tierToCheck)
    {
        List<Item> allItems = new List<Item>();
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].itemTier == tierToCheck)
            {
                allItems.Add(items[i]);
            }
        }
        return allItems;
    }


    public List<Item> GetXItemsBySource(int amount, ItemTier tierToCheck)
    {
        PlayerPrefSerialize.LoadPlayerValues();
        List<Item> result = new List<Item>();
        int extra_amount = PlayerPrefSerialize.playerValues.Items.Count;

        int[] usedValues = new int[amount + extra_amount];
        for (int i = 0; i < usedValues.Length; i++)
        {
            usedValues[i] = 9999;
        }

        for (int j = 0; j < extra_amount; j++)
        {
            usedValues[j] = PlayerPrefSerialize.playerValues.Items[j];
        }


        for (int i = extra_amount; i < usedValues.Length; i++)
        {
            int randIndex = 0;

            randIndex = GetRandomItemBySource(tierToCheck).id;

            while (ArrayContains(usedValues, randIndex))
            {
                randIndex = GetRandomItemBySource(tierToCheck).id;
            }


            usedValues[i] = randIndex;
            result.Add(GetItemByID(randIndex));
        }
        return result;

    }


    public Item GetRandomItemBySource(ItemTier tierToCheck)
    {

        int randIndex = Random.Range(0, this.items.Count);
        while (!(items[randIndex].itemTier == tierToCheck)) // 1  := Shop
        {
            randIndex = Random.Range(0, this.items.Count);
        }
        return items[randIndex];
    }


    public int GetPlayerItemAmountByTier(ItemTier itemTier)
    {
        PlayerPrefSerialize.LoadPlayerValues();
        int amount = 0;
        for (int i = 0; i < PlayerPrefSerialize.playerValues.Items.Count; i++)
        {
            if (GetItemByID(PlayerPrefSerialize.playerValues.Items[i]).itemTier == itemTier)
            {
                amount++;
            }

        }

        return amount;
    }


    public int GetItemAmountByTier(ItemTier itemTier)
    {
        int amount = 0;

        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].itemTier == itemTier)
            {
                amount++;
            }

        }

        return amount;
    }

    public Item GetItemByID(int id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].id == id)
            {
                return items[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }
    public Item GetItemByName(string name)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].scriptName == name)
            {
                return items[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }

    public void ScriptNameCheck(string nameToCheck)
    {
        bool isIn = false;
        for (int i = 0; i < items.Count; i++)
        {
            if (nameToCheck == items[i].scriptName)
            {
                isIn = true;
                break;
            }

        }
        if (isIn == false)
        {
            Debug.Log(nameToCheck + " fehlt in der Datenbank!");
        }

    }
    private void CreateItemObjects()
    {
        for (int i = 0; i < jsonItemDB.Count; i++)//looping through each item from json file
        {

            Item toAdd = gameObject.AddComponent<Item>();
            toAdd.id = (int)jsonItemDB[i]["id"];
            toAdd.title = jsonItemDB[i]["title"].ToString();
            toAdd.scriptName = jsonItemDB[i]["scriptName"].ToString();
            toAdd.itemTier = (ItemTier)System.Enum.Parse(typeof(ItemTier), jsonItemDB[i]["itemTier"].ToString());
            toAdd.imagePath = jsonItemDB[i]["imagePath"].ToString();

            //Global Prefs Lock
            GlobalPrefSerialize.LoadPlayerValues();
            if (!ListContains(GlobalPrefSerialize.globalValues.lockItemID, toAdd.id))
            {
                items.Add(toAdd);
            }
            else
            {
             //   Debug.Log("Item: " + toAdd.title + " is locked!");
            }
            Destroy(toAdd);
        }
    }

    // Helpermethod to sync database-values with outsided created item-components
    // @param: toSync The outside created item-component
    // @param: origin The Item from database-items
    public void SyncValues(Item toSync, Item origin)
    {
        toSync.id = origin.id;
        toSync.title = origin.title;
        toSync.scriptName = origin.scriptName;
        toSync.itemTier = origin.itemTier;
        toSync.imagePath = origin.imagePath;
    }



}