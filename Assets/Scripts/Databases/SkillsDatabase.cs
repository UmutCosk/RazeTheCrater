
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

public class SkillsDatabase : MonoBehaviour
{

    public List<Skill> skills = new List<Skill>();
    private JsonData jsonSkillsDB; // Object that holds the json Data that is pulled

    private bool ArrayContains(int[] array, int value)
    {
        bool result = false;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == value)
            {
                return true;
            }
        }
        return result;
    }

    void Awake()
    {
        //converts from json and passes it as an object
        jsonSkillsDB = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/SkillsDB.json"));
        CreateSkillObjects();
    }

    public List<Skill> GetRandomSkills(int amount)
    {
        List<Skill> result = new List<Skill>();
        int[] usedValues = new int[amount];

        for (int i = 0; i < amount; i++)
        {
            int randIndex = Random.Range(1, this.skills.Count + 1);
            usedValues[i] = randIndex;

            while (ArrayContains(usedValues, randIndex))
            {
                randIndex = Random.Range(1, this.skills.Count + 1);
                usedValues[i] = randIndex;
            }

            result.Add(skills[randIndex - 1]);
        }
        return result;
    }

    public List<Skill> GetRandomBronzeSkills(int amount)
    {
        List<Skill> result = new List<Skill>();
        List<Skill> allBronzeSkills = GetBronzeSkills();
        int[] usedValues = new int[amount];
        for (int i = 0; i < usedValues.Length; i++)
        {
            usedValues[i] = 9999;
        }

        for (int i = 0; i < usedValues.Length; i++)
        {
            int randIndex = Random.Range(0, allBronzeSkills.Count);

            while (ArrayContains(usedValues, randIndex))
            {
                randIndex = Random.Range(0, allBronzeSkills.Count);
            }
            usedValues[i] = randIndex;
            result.Add(allBronzeSkills[randIndex]);
        }
        return result;
    }

    public List<Skill> GetBronzeSkills()
    {
        List<Skill> result = new List<Skill>();
        for (int i = 0; i < skills.Count; i++)
        {
            if (skills[i].rarity == 1)
            {
                result.Add(skills[i]);
            }
        }
        return result;
    }


    public List<Skill> GetRandomSilverSkills(int amount)
    {
        List<Skill> result = new List<Skill>();
        List<Skill> allSilverSkills = GetSilverSkills();
        int[] usedValues = new int[amount];

        for (int i = 0; i < usedValues.Length; i++)
        {
            int randIndex = Random.Range(1, allSilverSkills.Count + 1);

            while (ArrayContains(usedValues, randIndex))
            {
                randIndex = Random.Range(1, allSilverSkills.Count + 1);
            }
            usedValues[i] = randIndex;
            result.Add(allSilverSkills[randIndex - 1]);
        }
        return result;
    }

    public List<Skill> GetSilverSkills()
    {
        List<Skill> result = new List<Skill>();
        for (int i = 0; i < skills.Count; i++)
        {
            if (skills[i].rarity == 2)
            {
                result.Add(skills[i]);
            }
        }
        return result;
    }

    public List<Skill> GetRandomGoldSkills(int amount)
    {
        List<Skill> result = new List<Skill>();
        List<Skill> allGoldSkills = GetGoldSkills();
        int[] usedValues = new int[amount];
        for (int i = 0; i < usedValues.Length; i++)
        {

            int randIndex = Random.Range(1, allGoldSkills.Count + 1);
            while (ArrayContains(usedValues, randIndex))
            {
                randIndex = Random.Range(1, allGoldSkills.Count + 1);
            }
            usedValues[i] = randIndex;
            result.Add(allGoldSkills[randIndex - 1]);
        }
        return result;
    }

    public List<Skill> GetGoldSkills()
    {
        List<Skill> result = new List<Skill>();
        for (int i = 0; i < skills.Count; i++)
        {
            if (skills[i].rarity == 3)
            {

                result.Add(skills[i]);
            }
        }
        return result;
    }

    //fetch item
    public Skill GetSkillByID(int id)
    {

        for (int i = 0; i < skills.Count; i++)
        {
            if (skills[i].id == id)
            {

                return skills[i]; // ist Object von class Item -! nein, sondern von ItemsShop
            }
        }
        return null;
    }

    public Skill GetSkillByScriptName(string scriptName)
    {
        Skill result = null;

        for (int i = 0; i < skills.Count; i++)
        {
            if (skills[i].scriptName == scriptName)
            {
                result = skills[i];
                return result;
            }
        }

        return result;
    }



    public int GetUpgradeIDByString(string skillToUpgrade)
    {
        int upgradeID = 9999;
        for (int i = 0; i < skills.Count; i++)
        {
            if (skills[i].title == skillToUpgrade + "+")
            {
                upgradeID = skills[i].id;
                return upgradeID;
            }
        }
        return upgradeID;

    }

    // Helpermethod to sync database-values with outsided created item-components
    // @param: toSync The outside created item-component
    // @param: origin The Item from database-items
    public void SyncValues(Skill toSync, Skill origin)
    {
        toSync.id = origin.id;
        toSync.title = origin.title;
        toSync.scriptName = origin.scriptName;
        toSync.rarity = origin.rarity;
        toSync.imagePath = origin.imagePath;
        toSync.skillSource = origin.skillSource;
        toSync.targetType = origin.targetType;
        toSync.skillCategory = origin.skillCategory;

        toSync.energyCost = origin.energyCost;
    }

    private void CreateSkillObjects()
    {
        for (int i = 0; i < jsonSkillsDB.Count; i++)//looping through each item from json file
        {
            Skill toAdd = gameObject.AddComponent<Skill>();
            toAdd.id = (int)jsonSkillsDB[i]["id"];
            toAdd.title = jsonSkillsDB[i]["title"].ToString();
            toAdd.scriptName = jsonSkillsDB[i]["scriptName"].ToString();
            toAdd.rarity = (int)jsonSkillsDB[i]["rarity"];
            toAdd.energyCost = (int)jsonSkillsDB[i]["energyCost"];
            toAdd.imagePath = jsonSkillsDB[i]["imagePath"].ToString();
            toAdd.targetType = (TargetStyle)System.Enum.Parse(typeof(TargetStyle), jsonSkillsDB[i]["targetType"].ToString());
            toAdd.skillSource = (SkillSource)System.Enum.Parse(typeof(SkillSource), jsonSkillsDB[i]["skillSource"].ToString());
            toAdd.skillCategory = (SkillCategory)System.Enum.Parse(typeof(SkillCategory), jsonSkillsDB[i]["skillCategorie"].ToString());


            //Global Prefs Lock
            GlobalPrefSerialize.LoadPlayerValues();
            if (!ListContains(GlobalPrefSerialize.globalValues.lockSkillID, toAdd.id))
            {
                skills.Add(toAdd);
            }
            else
            {
                //Debug.Log("Item: " + toAdd.title + " is locked!");
            }
            Destroy(toAdd);

        }
    }

    private bool ListContains(List<int> list, int value)
    {
        bool result = false;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == value)
            {
                return true;
            }
        }
        return result;
    }



}
