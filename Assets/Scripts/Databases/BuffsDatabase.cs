
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using LitJson;
using System.IO;

public class BuffsDatabase : MonoBehaviour
{

    public List<Buff> buffs = new List<Buff>();
    private JsonData jsonBuffsDB; // Object that holds the json Data that is pulled

    void Awake()
    {
        //converts from json and passes it as an object
        jsonBuffsDB = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/BuffsDB.json"));
        CreateBuffObjects();
    }




    //fetch item
    public Buff GetBuffByID(int id)
    {

        for (int i = 0; i < buffs.Count; i++)
        {
            if (buffs[i].id == id)
            {

                return buffs[i];
            }
        }
        return null;
    }

    //fetch item
    public int GetBuffIDByBuffType(BuffType buffType)
    {
        int resultID = 9999;
        for (int i = 0; i < buffs.Count; i++)
        {
            if (buffs[i].buffType == buffType)
            {
                resultID = buffs[i].id;
                break;
            }
        }
        return resultID;
    }


    // Helpermethod to sync database-values with outsided created item-components
    // @param: toSync The outside created item-component
    // @param: origin The Item from database-items
    public void SyncValues(Buff toSync, Buff origin)
    {


        toSync.id = origin.id;
        toSync.title = origin.title;
        toSync.scriptName = origin.scriptName;
        toSync.imagePath = origin.imagePath;
        toSync.buffModel = origin.buffModel;
        toSync.buffType = origin.buffType;

    }






    private void CreateBuffObjects()
    {

        for (int i = 0; i < jsonBuffsDB.Count; i++)//looping through each item from json file
        {

            Buff toAdd = gameObject.AddComponent<Buff>();
            toAdd.id = (int)jsonBuffsDB[i]["id"];
            toAdd.title = jsonBuffsDB[i]["title"].ToString();
            toAdd.scriptName = jsonBuffsDB[i]["scriptName"].ToString();
            toAdd.imagePath = jsonBuffsDB[i]["imagePath"].ToString();
            toAdd.buffModel = (BuffModel)System.Enum.Parse(typeof(BuffModel), jsonBuffsDB[i]["buffModel"].ToString());
            toAdd.buffType = (BuffType)System.Enum.Parse(typeof(BuffType), jsonBuffsDB[i]["buffType"].ToString());


            buffs.Add(toAdd);
            Destroy(toAdd);
        }
    }


}
