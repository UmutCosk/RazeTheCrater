﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;



public class Sniper : Skill
{

    void Start()
    {

        damage1 = 5;
        burst = 3;
        ricochet = 0;

        description = "Deal " + CalcDamageForTooltip(damage1) + " damage";
    }



    public override void UseFocus(GameObject caster)
    {

        //Focus on
        ActivateGlowVariable();
        if (GameObject.Find("SlowMotion").GetComponent<SlowMotion>().toggle)
        {
            GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOn();
        }
        else
        {
            GameObject.Find("SlowMotion").GetComponent<SlowMotion>().sniper = true;
        }

        //Clear in EventManager
        EventManager2.sniperTargets.Clear();
        //Push to EventManager
        if (burst > 0)
        {
            EventManager2.burstSniper = burst;
            EventManager2.sniperMode = SniperMode.Burst;
        }
        else if (ricochet > 0)
        {
            EventManager2.ricochetSniper = ricochet;
            EventManager2.sniperMode = SniperMode.Ricochet;
        }

    }





    public override void UseSniper()
    {

        //Spawn Projectile
        SpawnShot("Sniper");

        //Target
        projectile_clone.GetComponent<SniperFlying>().SniperShot();
    }

    public override void HitEffect(GameObject caster, GameObject target)
    {

        DamageEnemy(this.gameObject, caster, target, 0f);
        HitVFX(target.transform.position, "SlashFX");
        GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOff();


    }


}
