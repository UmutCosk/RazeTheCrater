﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;



public class Shot : Skill
{

    void Start()
    {

        damage1 = 5;
        description = "Deal " + CalcDamageForTooltip(damage1) + " damage";
        // this.GetComponent<SniperCheck>().sniperTarget = SniperTarget.Standard;
    }


    public override void UseCard(GameObject caster, GameObject target)
    {
        //Spawn Projectile
        SpawnShot(this.name);
        //Target
        projectile_clone.GetComponent<StandardFlying>().Shot(caster, target, this.gameObject);
    }

    public override void HitEffect(GameObject caster, GameObject target)
    {
        DamageEnemy(this.gameObject, caster, target, 0f);
        HitVFX(target.transform.position, this.name + "FX");

    }

    public override void SnipedEffect()
    {
        HitVFX(this.gameObject.transform.position, this.name + "FX");
        Destroy(this.gameObject);

    }



}
