﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
public class Kevlar : Skill
{
    int armor = 8;
    void Start()
    {

        description = "Get " + CalcBuffForTooltip(BuffType.Armor, armor) + " Armor";

    }






    public override void UseInstantCard(GameObject caster)
    {
        CastProtect(armor);
    }



}
