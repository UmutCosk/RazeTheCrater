﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class Shield : Skill
{
    int armor = 8;
    int toughness = 2;
    void Start()
    {

        description = "Get " + CalcBuffForTooltip(BuffType.Armor, armor) + " Armor and " + CalcBuffForTooltip(BuffType.Toughness, toughness) + " Toughness";
    }




    public override void UseInstantCard(GameObject caster)
    {
        CastProtect(armor);
        GetBuffed(BuffType.Toughness, toughness, 0f);
    }

}
