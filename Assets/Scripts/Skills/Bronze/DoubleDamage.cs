﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;



public class DoubleDamage : Skill
{
    float factor = 2;

    void Start()
    {
        ricochet = 1;
        burst = 0;
        description = "Apply double damage to a projectile";
    }



    public override void UseFocus(GameObject caster)
    {

        //Focus on
        ActivateGlowVariable();
        TurnGlowForQuick();
        GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOn();
        //Clear in EventManager
        EventManager2.sniperTargets.Clear();
        //Push to EventManager
        if (burst > 0)
        {
            EventManager2.burstSniper = burst;
            EventManager2.sniperMode = SniperMode.Burst;
        }
        else if (ricochet > 0)
        {
            EventManager2.ricochetSniper = ricochet;
            EventManager2.sniperMode = SniperMode.Ricochet;
        }

    }





    public override void UseSniper()
    {

        //Spawn Projectile
        SpawnShot("Sniper");

        //Target
        projectile_clone.GetComponent<SniperFlying>().SniperShot();
    }

    public override void QuickEffect(GameObject card)
    {

        card.GetComponent<Skill>().damage1 = card.GetComponent<Skill>().damage1 * 2;
        HitVFX(this.transform.position, "SlashFX");
        GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOff();


    }


}
