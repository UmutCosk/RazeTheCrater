﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class Granade : Skill
{

    void Start()
    {

        damage1 = 7;

        //  this.GetComponent<SniperCheck>().sniperTarget = SniperTarget.Throw;
        description = "Deal " + CalcDamageForTooltip(damage1) + " damage";



    }


    public override void UseCard(GameObject caster, GameObject target)
    {
        //Spawn Projectile
        SpawnShot(this.name);
        //Target
        projectile_clone.GetComponent<StandardFlying>().Shot(caster, target, this.gameObject);
    }

    public override void HitEffect(GameObject caster, GameObject target)
    {
        DamageEnemy(this.gameObject, caster, target, 0f);

    }

    public override void SnipedEffect()
    {
        HitVFX(this.gameObject.transform.position, this.name + "FX");
        Destroy(this.gameObject);

    }

}
