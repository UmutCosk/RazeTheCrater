using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public enum SkillCategory { Standard, Throw, Sniper, Protect, Drug, Quick }
public enum TargetStyle { None, Single, PureSingle, AOE, PureAoe }
public enum SkillSource { Neutral, Warrior }
public enum SniperMode { Burst, Ricochet, Quick }
public enum SniperTarget { Standard, Throw, Enemy }

public class Skill : MonoBehaviour
{
    //Für 
    public int id;
    public string title;
    public string description;
    public string scriptName;
    public int rarity;
    public int energyCost;


    public string imagePath;
    public TargetStyle targetType;
    public SkillSource skillSource;
    public SkillCategory skillCategory;

    //Sniper
    public int burst = 0;
    public int ricochet = 0;
    public bool scriptSniper = false;

    //Für Tooltip & Damage
    public int damage1;
    public int damage2;
    public int damage3;

    public int buff1;
    public int buff2;
    public int buff3;

    public int damageDone;

    public GameObject projectile_clone;

    void Start()
    {

    }
    public void HitVFX(Vector3 position, string pathOfEffect)
    {
        GameObject sniped = Resources.Load<GameObject>("Projectiles/SniperFX/" + pathOfEffect);
        Vector3 temp_position = position;
        temp_position.z = 1;
        GameObject sniped_clone = Instantiate(sniped, temp_position, Quaternion.identity);
        Destroy(sniped_clone, 5f);
    }

    public void AnimationEffect(string animationPath, GameObject castOnObject, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float destroyTime)
    {
        castOnObject = castOnObject.transform.GetChild(0).gameObject;
        StartCoroutine(AnimationEffect2(animationPath, castOnObject, adjustAnimationPosition, adjustAnimationRotation, castDelay, destroyTime));
    }

    IEnumerator AnimationEffect2(string animationPath, GameObject castOnObject, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float destroyTime)
    {
        yield return new WaitForSeconds(castDelay);
        //Animation
        Vector3 startPosi = castOnObject.transform.position;
        GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
        startPosi += adjustAnimationPosition;
        GameObject animationClone = (GameObject)Instantiate(animation_prefab, startPosi, Quaternion.identity) as GameObject;
        Vector3 temp_rotation = new Vector3(0, 0, 0);
        temp_rotation += adjustAnimationRotation;
        animationClone.transform.localRotation = Quaternion.Euler(temp_rotation);
        Destroy(animationClone, destroyTime);
    }

    public void SpawnShot(string name)
    {
        GameObject projectile = Resources.Load<GameObject>("Projectiles/" + name);

        projectile_clone = Instantiate(projectile, GameObject.FindGameObjectWithTag("Gamer").transform.GetChild(0).position, Quaternion.identity);
        projectile_clone.name = name;
        projectile_clone.transform.SetParent(GameObject.Find("Projectiles").transform);
        //projectile_clone.transform.position = GameObject.FindGameObjectWithTag("Gamer").transform.GetChild(0).position;
        projectile_clone.transform.localScale = new Vector3(1, 1, 1);
    }

    public void CastDrug(GameObject card, GameObject caster)
    {
        GameObject castbar = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Casts/Drug");
        GameObject castbar_clone = Instantiate(castbar, Vector3.zero, Quaternion.identity);
        castbar_clone.transform.SetParent(GameObject.FindGameObjectWithTag("Gamer").transform.GetChild(0).GetChild(2).transform);
        castbar_clone.transform.localScale = new Vector3(1, 1, 1);
        castbar_clone.transform.GetChild(0).GetComponent<DrugCast>().cardToCast = card;
        castbar_clone.transform.GetChild(0).GetComponent<DrugCast>().caster = caster;
        Vector3 tempPosi = castbar_clone.transform.localPosition;
        tempPosi.z = 0;
        castbar_clone.transform.localPosition = tempPosi;
    }

    public void CastProtect(int armorValue)
    {
        if (!GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().armorUp)
        {

            GameObject.FindGameObjectWithTag("Gamer").transform.GetChild(0).GetChild(1).GetChild(1).gameObject.SetActive(true);
        }
        else
        {
            GameObject.FindGameObjectWithTag("Gamer").transform.GetChild(0).GetChild(1).GetChild(1).GetChild(0).GetComponent<ArmorBar>().ResetCountdown();
        }

        GetBuffed(BuffType.Armor, armorValue, 0f);
    }


    public virtual void HitEffect(GameObject caster, GameObject target)
    {

    }


    public void UpdateDescription(string description)
    {

    }

    public virtual void UseCard(GameObject caster, GameObject target)
    {

    }
    public virtual void UseSniper()
    {

    }
    public virtual void UseInstantCard(GameObject caster)
    {

    }
    public virtual void UseCastCard(GameObject caster)
    {

    }
    public virtual void UseFocus(GameObject caster)
    {

    }

    public virtual void SnipedEffect()
    {

    }
    public virtual void QuickEffect(GameObject card)
    {

    }


    public void DamageEnemy(GameObject scroll, GameObject caster, GameObject target, float delay)
    {
        StartCoroutine(DamageEnemy2(scroll, caster, target, delay));
    }

    IEnumerator DamageEnemy2(GameObject skill, GameObject caster, GameObject target, float delay)
    {

        damageDone = 0;
        yield return new WaitForSeconds(delay);
        //Calc extra Damage from Buffs & Items

        if (caster.transform.GetChild(0).name != "EnemyPanels")
        {
            int damage = caster.GetComponent<BaseGamer>().CalcDealExtraDamage(skill.GetComponent<Skill>().damage1);
            damageDone = target.GetComponent<BaseEnemy>().GetDamage(damage);
        }
        else
        {
            int damage = caster.GetComponent<BaseEnemy>().CalcExtraDamage(skill.GetComponent<Skill>().damage1);
            damageDone = target.GetComponent<BaseGamer>().GetDamage(damage);
        }



    }
    public void GetBuffed(BuffType bufftype, int amount, float timer)
    {
        GameObject usingObject = GameObject.FindGameObjectWithTag("Gamer").gameObject;
        StartCoroutine(GetBuffed2(usingObject, bufftype, amount, timer));
    }

    IEnumerator GetBuffed2(GameObject usingObject, BuffType bufftype, int amount, float timer)
    {
        yield return new WaitForSeconds(timer);
        switch (bufftype)
        {
            case BuffType.Armor:
                usingObject.GetComponent<BasePlayer>().GetArmor(amount, usingObject);
                break;
            case BuffType.Power:
                usingObject.GetComponent<BasePlayer>().GetPower(amount);
                break;
            case BuffType.Toughness:
                usingObject.GetComponent<BasePlayer>().GetToughness(amount);
                break;
            case BuffType.Dodge:
                usingObject.GetComponent<BasePlayer>().GetDodge(amount);
                break;
            case BuffType.Energy:
                usingObject.GetComponent<BaseGamer>().GainCurEnergy(amount);
                break;
            case BuffType.Heal:
                usingObject.GetComponent<BasePlayer>().HealHP(amount);
                break;
        }

    }

    public string CalcDamageForTooltip(int damage)
    {
        if (this.gameObject.scene.name == "Combat2")
        {
            int changedDamage = GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().CalcDealExtraDamage(damage);
            if (changedDamage != damage)
            {
                return changedDamage.ToString() + "*";
            }
        }
        //hier kommen Buff & Item Calculationen rein
        return damage.ToString();
    }

    public string CalcBuffForTooltip(BuffType bufftype, int amount)
    {
        //Sucht alle effekte die mit dem BuffType correrlieren

        return amount.ToString();
    }
    public void ActivateGlowVariable()
    {
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Shots"))
        {
            if (glowObject.GetComponent<StandardFlying>().enemysProjectile)
            {
                glowObject.GetComponent<SniperCheck>().activeGlow = true;
            }
            else
            {
                glowObject.GetComponent<SniperCheck>().activeGlow = false;
            }

        }
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Throws"))
        {
            glowObject.GetComponent<SniperCheck>().activeGlow = true;
        }
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Enemys"))
        {
            glowObject.GetComponent<SniperCheck>().activeGlow = true;
        }
    }

    public void TurnGlowForQuick()
    {
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Throws"))
        {
            glowObject.GetComponent<SniperCheck>().activeGlow = false;
            glowObject.transform.GetChild(1).gameObject.SetActive(false);

        }
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Enemys"))
        {
            glowObject.GetComponent<SniperCheck>().activeGlow = false;
            glowObject.transform.GetChild(1).gameObject.SetActive(false);

        }
        foreach (GameObject glowObject in GameObject.FindGameObjectsWithTag("Shots"))
        {
            if (!glowObject.GetComponent<StandardFlying>().enemysProjectile)
            {
                glowObject.GetComponent<SniperCheck>().activeGlow = true;
            }
            else
            {
                glowObject.GetComponent<SniperCheck>().activeGlow = false;
            }

        }
    }







    // //Power
    // public int recharge;

    // //Crafting
    // public int craftingPrice;
    // public int craftingPosition;

    // //Hilfsvariablen
    // public int cooldown;
    // public int multipleHits;
    // public bool skillUsed;

    // //Nur Timer
    // public int damageTimer1;
    // public int damageTimer2;
    // public int damageTimer3;
    // public int damageTimer4;
    // public int damageTimer5;
    // public int triggerID1;
    // public int triggerID2;
    // public int triggerID3;
    // public int triggerID4;
    // public int triggerID5;
    // public GameObject chosenEnemyTimer1;
    // public GameObject chosenEnemyTimer2;
    // public GameObject chosenEnemyTimer3;
    // public GameObject chosenEnemyTimer4;
    // public GameObject chosenEnemyTimer5;
    // public GameObject chosenConsumableTimer1;
    // public GameObject chosenConsumableTimer2;
    // public GameObject chosenConsumableTimer3;
    // public GameObject chosenConsumableTimer4;
    // public GameObject chosenConsumableTimer5;
    // public GameObject tempEnemyForTimer;

    // float destroyTime = 6f;
    // public Vector3 zero = new Vector3(0, 0, 0);
    // public List<int> currentEnemys = new List<int>();
    // public GameObject animationClone;

    // public SkillsDatabase skillDB;
    // BaseGamer baseGamer;

    // public int damageSingle;
    // public int damageAOE;
    // public int damageALL;
    // public int damageAdjacent;
    // public int damageRest;
    // public int pureSingle;

    // void Awake()
    // {

    // }

    // void Start()
    // {


    // }

    // public int GetChoosenEnemyPosition()
    // {
    //     return EventManager.chosenEnemy.GetComponent<BaseEnemy>().position;
    // }

    // public IEnumerator DealDamageAnimationAfterAnimation(GameObject enemy, int damage, GameObject animation)
    // {
    //     float animationDuration = animation.GetComponent<ParticleSystem>().main.duration - 0.4f;
    //     EventManager.dealRealDamage = true;
    //     enemy.GetComponent<BaseEnemy>().GetDamage(baseGamer.CalcDealDamage(damage), enemy.GetComponent<BaseEnemy>().position); //inital Target´        
    //     EventManager.dealRealDamage = false;
    //     damage = enemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(baseGamer.CalcDealDamage(damage));
    //     damage = Mathf.RoundToInt(damage);
    //     EventManager.TriggerEvent("UpdateHP");
    //     yield return new WaitForSeconds(animationDuration);


    //     if (damage > 0)
    //     {
    //         FloatingTextController.CreateFloatingText(damage.ToString(), enemy.transform.gameObject.transform, Color.red, "Combat/Animation/GetDamage/EnemyDamage/DamageTextEnemy1");

    //     }
    // }
    // public IEnumerator DealSelfDamageAnimationAfterAnimation(GameObject self, int damage, int position, GameObject animation)
    // {
    //     float animationDuration = animation.GetComponent<ParticleSystem>().main.duration - 0.4f;
    //     self.GetComponent<BasePlayer>().GetDamage(damage, position);
    //     damage = self.GetComponent<BasePlayer>().CalcReceiveDamage(damage);
    //     damage = Mathf.RoundToInt(damage);
    //     EventManager.TriggerEvent("UpdateHP");
    //     yield return new WaitForSeconds(animationDuration);


    //     if (damage > 0)
    //     {
    //         if (self == GameObject.Find("Player").gameObject)  //Wegen Familiar
    //         {
    //             FloatingTextController.CreateFloatingText(damage.ToString(), self.transform.GetChild(0).gameObject.transform, Color.black, "Combat/Animation/GetDamage/PlayerDamage/DamageTextFXParent");
    //         }
    //         else
    //         {
    //             FloatingTextController.CreateFloatingText(damage.ToString(), self.gameObject.transform, Color.black, "Combat/Animation/GetDamage/PlayerDamage/DamageTextFXParent");
    //         }
    //     }
    // }
    // public IEnumerator DealPureDamageAnimationAfterAnimation(GameObject enemy, int damage, GameObject animation)
    // {
    //     float animationDuration = animation.GetComponent<ParticleSystem>().main.duration - 0.4f;
    //     EventManager.dealRealDamage = true;
    //     enemy.GetComponent<BaseEnemy>().GetPureDamage(baseGamer.CalcDealDamage(damage), enemy.GetComponent<BaseEnemy>().position); //inital Target´   
    //     EventManager.dealRealDamage = false;
    //     damage = enemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(baseGamer.CalcDealDamage(damage));
    //     damage = Mathf.RoundToInt(damage);
    //     EventManager.TriggerEvent("UpdateHP");
    //     yield return new WaitForSeconds(animationDuration);
    //     if (damage > 0)
    //     {
    //         FloatingTextController.CreateFloatingText(damage.ToString(), enemy.transform.gameObject.transform, Color.red, "Combat/Animation/GetDamage/EnemyDamage/DamageTextEnemy1");

    //     }
    // }
    // public IEnumerator DealNoArmorDamageAnimationAfterAnimation(GameObject enemy, int damage, GameObject animation)
    // {
    //     float animationDuration = animation.GetComponent<ParticleSystem>().main.duration - 0.4f;
    //     EventManager.dealRealDamage = true;
    //     enemy.GetComponent<BaseEnemy>().GetDamageNoArmor(baseGamer.CalcDealDamage(damage), enemy.GetComponent<BaseEnemy>().position); //inital Target´       
    //     EventManager.dealRealDamage = false;
    //     damage = enemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(baseGamer.CalcDealDamage(damage));
    //     damage = Mathf.RoundToInt(damage);
    //     EventManager.TriggerEvent("UpdateHP");
    //     yield return new WaitForSeconds(animationDuration);
    //     if (damage > 0)
    //     {
    //         FloatingTextController.CreateFloatingText(damage.ToString(), enemy.transform.gameObject.transform, Color.red, "Combat/Animation/GetDamage/EnemyDamage/DamageTextEnemy1");
    //     }
    // }
    // public IEnumerator DealPureNoArmorDamageAnimationAfterAnimation(GameObject enemy, int damage, GameObject animation)
    // {
    //     float animationDuration = animation.GetComponent<ParticleSystem>().main.duration - 0.4f;
    //     EventManager.dealRealDamage = true;
    //     enemy.GetComponent<BaseEnemy>().GetPureDamage(baseGamer.CalcDealDamage(damage), enemy.GetComponent<BaseEnemy>().position); //inital Target´     
    //     EventManager.dealRealDamage = false;
    //     damage = enemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(baseGamer.CalcDealDamage(damage));
    //     damage = Mathf.RoundToInt(damage);
    //     EventManager.TriggerEvent("UpdateHP");
    //     yield return new WaitForSeconds(animationDuration);
    //     if (damage > 0)
    //     {
    //         FloatingTextController.CreateFloatingText(damage.ToString(), enemy.transform.gameObject.transform, Color.red, "Combat/Animation/GetDamage/EnemyDamage/DamageTextEnemy1");
    //     }
    // }
    // private void EndSkill()
    // {
    //     /*     if (EventManager.chosenSupport != null)
    //         {
    //             EventManager.chosenSupport.GetComponent<Skill>().SkillSupportNach();
    //         }
    //         EventManager.chosenSupport = null; */
    //     //ResetFinalDamageDealt();
    // }

    // public void ResetFinalDamageDealt()
    // {
    //     //Reset Final Damage after a SKill is used
    //     CombatPrefSerialize.LoadCombatValues();
    //     int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //     for (int i = 0; i < enemysLeft; i++)
    //     {
    //         GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().finalDmgDealt = 0;
    //     }
    // }


    // public virtual void Cooldown()
    // {


    // }

    // public virtual void TriggerCountdown()
    // {


    // }

    // public virtual void SkillDamage(GameObject chosenEnemy)
    // {


    // }
    // public virtual void SkillDamage2(GameObject chosenEnemy)
    // {


    // }
    // public virtual void SkillDamageTimer(GameObject chosenEnemy, int damageTimer)
    // {


    // }

    // public virtual void SkillDebuff(GameObject chosenEnemy)
    // {


    // }

    // public virtual int BeforeDamageSupports(int value)
    // {

    //     return value;
    // }
    // public virtual void AfterDamageSupports(int value)
    // {

    // }
    // public virtual void AfterHealingSupports(int value)
    // {

    // }

    // public virtual void SkillSupportVor(GameObject chosenEnemy)
    // {


    // }
    // public virtual void SkillSupportNach()
    // {


    // }
    // public virtual int SkillSupportAddDamage(int value)
    // {

    //     return value;
    // }
    // public virtual void KilledEnemy()
    // {


    // }
    // public virtual void AfterAttackEffect()
    // {

    // }

    // public virtual void BeforeAttackEffect()
    // {

    // }


    // public virtual void Animation(GameObject chosenEnemy)
    // {


    // }
    // public void UpdateValues()
    // {
    //     EventManager.TriggerEvent("DamageStep");
    //     EventManager.TriggerEvent("TurnOffSkill");
    // }


    // //-------------Hilfsfunktionen
    // public void SetCurrentEnemies()
    // {
    //     currentEnemys.Clear();
    //     CombatPrefSerialize.LoadCombatValues();
    //     int totalEnemy = CombatPrefSerialize.combatValues.enemysToKill;
    //     for (int j = 0; j < totalEnemy; j++)
    //     {

    //         currentEnemys.Add(j);

    //     }

    // }
    // public GameObject GetRandomAliveEnemy()
    // {
    //     currentEnemys.Clear();
    //     CombatPrefSerialize.LoadCombatValues();
    //     int totalEnemy = CombatPrefSerialize.combatValues.enemysToKill;
    //     for (int j = 0; j < totalEnemy; j++)
    //     {

    //         currentEnemys.Add(j);

    //     }
    //     int randomIndex = Random.Range(0, currentEnemys.Count);
    //     int randomEnemy = currentEnemys[randomIndex];
    //     GameObject enemy = GameObject.Find("EnemyPanel").transform.GetChild(randomEnemy).gameObject;
    //     return enemy;

    // }
    // public IEnumerator AnimationOnly(string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float destroyTime)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = EventManager.chosenEnemy.transform.position;
    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }

    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     animationClone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animationClone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animationClone, destroyTime);

    // }
    // public IEnumerator AnimationCloneNullAfterTime(float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     animationClone = null;
    // }
    // //---------------Combat Funktionen
    // public IEnumerator AttackTarget(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, GameObject enemy)
    // {

    //     yield return new WaitForSeconds(castDelay);
    //     //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = enemy.transform.position;
    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     StartCoroutine(DealDamageAnimationAfterAnimation(enemy, damage, animation_prefab_clone));
    //     Destroy(animation_prefab_clone, destroyTime);
    //     EndSkill();
    // }
    // public IEnumerator AttackTargetPure(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, GameObject enemy)
    // {
    //     yield return new WaitForSeconds(castDelay);

    //     //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = enemy.transform.position;
    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     StartCoroutine(DealPureDamageAnimationAfterAnimation(enemy, damage, animation_prefab_clone));
    //     Destroy(animation_prefab_clone, destroyTime);
    //     EndSkill();
    // }
    // public IEnumerator AttackTargetNoArmor(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, GameObject enemy)
    // {

    //     yield return new WaitForSeconds(castDelay);

    //     //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);

    //     Vector3 temp_position = new Vector3(0, 0, 0);
    //     if (EventManager.chosenEnemy != null)
    //     {
    //         temp_position = enemy.transform.position;
    //     }


    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //         Debug.Log(temp_position);
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animation_prefab_clone, destroyTime);

    //     //Damage
    //     StartCoroutine(DealNoArmorDamageAnimationAfterAnimation(enemy, damage, animation_prefab_clone));
    //     EndSkill();
    // }

    // public IEnumerator AttackLeftFromTarget(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float deltaDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     for (int i = 1; i < 5; i++)
    //     {
    //         if (EventManager.chosenEnemy.GetComponent<BaseEnemy>().position - i >= 0)
    //         {
    //             if (EventManager.chosenEnemy.transform.parent.GetChild(EventManager.chosenEnemy.GetComponent<BaseEnemy>().position - i).GetComponent<BaseEnemy>().curHP > 0)
    //             {
    //                 GameObject enemy = EventManager.chosenEnemy.transform.parent.GetChild(EventManager.chosenEnemy.GetComponent<BaseEnemy>().position - i).gameObject;
    //                 StartCoroutine(AttackLeftFromTargetDeltaDelay(damage, animationPath, startPosi, adjustAnimationPosition, adjustAnimationRotation, deltaDelay * i, enemy));
    //             }

    //         }
    //     }
    //     EndSkill();
    // }
    // IEnumerator AttackLeftFromTargetDeltaDelay(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float deltaDelay, GameObject enemy)
    // {
    //     yield return new WaitForSeconds(deltaDelay);

    //     //Animation 1
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = enemy.transform.position;
    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position += adjustAnimationPosition;
    //     temp_position.z = 0;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 rotationToLeft = new Vector3(0, 0, -90f);
    //     rotationToLeft += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(rotationToLeft);
    //     StartCoroutine(DealDamageAnimationAfterAnimation(EventManager.chosenEnemy, damage, animation_prefab_clone));
    //     Destroy(animation_prefab_clone, destroyTime);
    //     EndSkill();
    // }
    // public IEnumerator AttackRightFromTarget(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float deltaDelay, float sizeOverTime)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     for (int i = 1; i < 5; i++)
    //     {
    //         if (EventManager.chosenEnemy.GetComponent<BaseEnemy>().position + i >= 0)
    //         {
    //             try
    //             {
    //                 if (EventManager.chosenEnemy.transform.parent.GetChild(EventManager.chosenEnemy.GetComponent<BaseEnemy>().position + i).GetComponent<BaseEnemy>().curHP > 0)
    //                 {
    //                     GameObject enemy = EventManager.chosenEnemy.transform.parent.GetChild(EventManager.chosenEnemy.GetComponent<BaseEnemy>().position + i).gameObject;
    //                     StartCoroutine(AttackRightFromTargetDeltaDelay(damage, animationPath, startPosi, adjustAnimationPosition, adjustAnimationRotation, deltaDelay * i, enemy, i * sizeOverTime));
    //                 }
    //             }
    //             catch { }

    //         }
    //     }
    //     EndSkill();
    // }
    // IEnumerator AttackRightFromTargetDeltaDelay(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float deltaDelay, GameObject enemy, float sizeOverTime)
    // {
    //     yield return new WaitForSeconds(deltaDelay);
    //     //Damage
    //     //Animation 1
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = enemy.transform.position;
    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position += adjustAnimationPosition;
    //     temp_position.z = 0;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 rotationToLeft = new Vector3(0, 0, +90f);
    //     rotationToLeft += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(rotationToLeft);
    //     Vector3 newScale = animation_prefab_clone.transform.localScale;
    //     if (sizeOverTime > 0)
    //     {
    //         newScale = newScale * sizeOverTime;
    //         animation_prefab_clone.transform.localScale = newScale;
    //     }
    //     Destroy(animation_prefab_clone, destroyTime);
    //     StartCoroutine(DealDamageAnimationAfterAnimation(EventManager.chosenEnemy, damage, animation_prefab_clone));
    //     EndSkill();
    // }
    // public IEnumerator AttackAroundTarget(int damage, int adjacentDamage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, GameObject chosenEnemy)
    // {
    //     yield return new WaitForSeconds(castDelay);

    //     int position = EventManager.chosenEnemy.GetComponent<BaseEnemy>().position;

    //     //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = EventManager.chosenEnemy.transform.position;
    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     //Damage
    //     StartCoroutine(DealDamageAnimationAfterAnimation(EventManager.chosenEnemy, damage, animation_prefab_clone));
    //     //Adjacent Damage
    //     if (!(position + 1 > 4))
    //     {
    //         try
    //         {
    //             GameObject enemy = EventManager.chosenEnemy.transform.parent.GetChild(position + 1).gameObject;

    //             StartCoroutine(DealDamageAnimationAfterAnimation(enemy, adjacentDamage, animation_prefab_clone));
    //         }
    //         catch { }
    //     }

    //     if (!(position - 1 < 0) && EventManager.chosenEnemy.transform.parent.GetChild(position - 1).gameObject != null)
    //     {
    //         try
    //         {
    //             GameObject enemy = EventManager.chosenEnemy.transform.parent.GetChild(position - 1).gameObject;

    //             StartCoroutine(DealDamageAnimationAfterAnimation(enemy, adjacentDamage, animation_prefab_clone));
    //         }
    //         catch { }
    //     }

    //     Destroy(animation_prefab_clone, destroyTime);
    //     EndSkill();
    // }
    // public IEnumerator AttackAll(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, GameObject chosenEnemy)
    // {
    //     yield return new WaitForSeconds(castDelay);

    //     //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);

    //     Vector3 temp_position = new Vector3(0, 0, 0);
    //     if (EventManager.chosenEnemy != null)
    //     {
    //         temp_position = EventManager.chosenEnemy.transform.position;
    //     }


    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animation_prefab_clone, destroyTime);
    //     GameObject tempEnemy = null;
    //     if (chosenEnemy != null)
    //     {
    //         tempEnemy = EventManager.chosenEnemy;
    //         EventManager.chosenEnemy = chosenEnemy;
    //     }
    //     CombatPrefSerialize.LoadCombatValues();
    //     int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //     for (int i = 0; i < enemysLeft; i++)
    //     {

    //         StartCoroutine(DealDamageAnimationAfterAnimation(GameObject.Find("EnemyPanel").transform.GetChild(i).gameObject, damage, animation_prefab_clone));

    //     }
    //     if (tempEnemy != null)
    //     {
    //         EventManager.chosenEnemy = tempEnemy;
    //     }
    //     EndSkill();
    // }
    // public IEnumerator AttackAllNoArmor(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, GameObject chosenEnemy, Modfier modify)
    // {
    //     yield return new WaitForSeconds(castDelay);

    //     //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);

    //     Vector3 temp_position = new Vector3(0, 0, 0);
    //     if (EventManager.chosenEnemy != null)
    //     {
    //         temp_position = EventManager.chosenEnemy.transform.position;
    //     }


    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //         Debug.Log(temp_position);
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animation_prefab_clone, destroyTime);
    //     //Damage
    //     float damageMulti = 1;
    //     GameObject tempEnemy = null;
    //     if (chosenEnemy != null)
    //     {
    //         tempEnemy = EventManager.chosenEnemy;
    //         EventManager.chosenEnemy = chosenEnemy;
    //     }
    //     CombatPrefSerialize.LoadCombatValues();
    //     int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;

    //     for (int i = 0; i < enemysLeft; i++)
    //     {
    //         EventManager.damageMultiForCalcSkill = 1;

    //         if (modify == Modfier.Extra50 && GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().armor == 0 && GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().shield == 0)
    //         {
    //             damageMulti = 1.5f;
    //         }
    //         else if (modify == Modfier.Extra100 && GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().armor == 0 && GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().shield == 0)
    //         {
    //             damageMulti = 2f;

    //         }
    //         else
    //         {
    //             damageMulti = 1;
    //         }
    //         EventManager.damageMultiForCalcSkill = damageMulti;



    //         StartCoroutine(DealNoArmorDamageAnimationAfterAnimation(GameObject.Find("EnemyPanel").transform.GetChild(i).gameObject, damage, animation_prefab_clone));


    //     }
    //     if (tempEnemy != null)
    //     {
    //         EventManager.chosenEnemy = tempEnemy;
    //     }
    //     EndSkill();
    // }
    // public IEnumerator AttackAllNoAnimation(int damage, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     CombatPrefSerialize.LoadCombatValues();
    //     int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //     for (int i = 0; i < enemysLeft; i++)
    //     {

    //         GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().GetDamage(baseGamer.CalcDealDamage(damage), 0);

    //     }
    // }
    // public IEnumerator AttackWaveFromFront(int damage, string animationPath, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float deltaDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = GameObject.Find("BeginEnemyPanel").transform.position;
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);

    //     SetCurrentEnemies();
    //     for (int i = 0; i < currentEnemys.Count; i++)
    //     {
    //         StartCoroutine(AttackFirstToLastDeltaDelay(baseGamer.CalcDealDamage(damage), deltaDelay * i, currentEnemys[i], animation_prefab_clone));
    //     }

    //     EndSkill();
    // }
    // IEnumerator AttackFirstToLastDeltaDelay(int damage, float deltaDelay, int enemyPosition, GameObject animation)
    // {
    //     yield return new WaitForSeconds(deltaDelay);
    //     GameObject enemy = GameObject.Find("EnemyPanel").transform.GetChild(enemyPosition).gameObject;
    //     if (damage > 0)
    //     {
    //         enemy.GetComponent<BaseEnemy>().GetDamage(baseGamer.CalcDealDamage(damage), enemyPosition);
    //         StartCoroutine(DealDamageAnimationAfterAnimation(enemy, damage, animation));
    //     }
    //     EndSkill();
    // }

    // public IEnumerator AttackRandom(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     GameObject enemy = GetRandomAliveEnemy();
    //     EventManager.chosenRandomEnemy = enemy;

    //     //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = enemy.transform.position;

    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animation_prefab_clone, destroyTime);
    //     StartCoroutine(DealDamageAnimationAfterAnimation(enemy, damage, animation_prefab_clone));
    //     EndSkill();

    // }

    // //Debuffs Funktionen
    // public IEnumerator DebuffTarget(int amount, int level, int position, BuffType bufftype, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     DebuffEnemy(bufftype, position, amount, level, startPosi, adjustAnimationPosition, adjustAnimationRotation);

    // }

    // public IEnumerator DebuffLeftFromTarget(int amount, int level, int position, BuffType bufftype, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float deltaDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    // }
    // public IEnumerator DebuffRightFromTarget(int amount, int level, int position, BuffType bufftype, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float deltaDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     for (int i = 1; i < 5; i++)
    //     {
    //         if (EventManager.chosenEnemy.GetComponent<BaseEnemy>().position + i >= 0)
    //         {
    //             try
    //             {
    //                 if (EventManager.chosenEnemy.transform.parent.GetChild(EventManager.chosenEnemy.GetComponent<BaseEnemy>().position + i).GetComponent<BaseEnemy>().curHP > 0)
    //                 {
    //                     GameObject enemy = EventManager.chosenEnemy.transform.parent.GetChild(EventManager.chosenEnemy.GetComponent<BaseEnemy>().position + i).gameObject;
    //                     StartCoroutine(DebuffRightFromTargetDeltaDelay(amount, level, position, bufftype, startPosi, adjustAnimationPosition, adjustAnimationRotation, deltaDelay * i, enemy));
    //                 }
    //             }
    //             catch { }

    //         }
    //     }

    // }
    // IEnumerator DebuffRightFromTargetDeltaDelay(int amount, int level, int position, BuffType bufftype, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float deltaDelay, GameObject enemy)
    // {
    //     yield return new WaitForSeconds(deltaDelay);
    //     //Debuff
    //     DebuffEnemy(bufftype, position, amount, level, startPosi, adjustAnimationPosition, adjustAnimationRotation);

    // }

    // public IEnumerator DebuffAroundTarget(int amount, int level, int position, BuffType bufftype, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     DebuffEnemy(bufftype, position, amount, level, startPosi, adjustAnimationPosition, adjustAnimationRotation);
    //     //Adjacent Damage
    //     if (!(position + 1 > 4))
    //     {
    //         try
    //         {
    //             DebuffEnemy(bufftype, position + 1, amount, level, startPosi, adjustAnimationPosition, adjustAnimationRotation);
    //         }
    //         catch { }
    //     }

    //     if (!(position - 1 < 0) && EventManager.chosenEnemy.transform.parent.GetChild(position - 1).gameObject != null)
    //     {
    //         try
    //         {

    //             DebuffEnemy(bufftype, position - 1, amount, level, startPosi, adjustAnimationPosition, adjustAnimationRotation);
    //         }
    //         catch { }
    //     }
    // }
    // public IEnumerator DebuffAll(int amount, int position, int level, BuffType bufftype, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     CombatPrefSerialize.LoadCombatValues();
    //     int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //     for (int i = 0; i < enemysLeft; i++)
    //     {

    //         DebuffEnemy(bufftype, position, amount, level, startPosi, adjustAnimationPosition, adjustAnimationRotation);

    //     }

    // }
    // public IEnumerator BuffAllEnemies(int amount, int level, BuffType bufftype, int position, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, bool allowState)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     CombatPrefSerialize.LoadCombatValues();
    //     int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //     for (int i = 0; i < enemysLeft; i++)
    //     {

    //         StartCoroutine(BuffEnemy(bufftype, amount, i, level, startPosi, adjustAnimationPosition, adjustAnimationRotation, 0, allowState));

    //     }

    // }
    // public IEnumerator DebuffFirstToLast(int amount, BuffType bufftype, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);
    // }

    // public IEnumerator DebuffRandom(int amount, int level, int position, BuffType bufftype, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);

    //     GameObject enemy = GetRandomAliveEnemy();
    //     DebuffEnemy(bufftype, position, amount, level, startPosi, adjustAnimationPosition, adjustAnimationRotation);

    // }
    // public IEnumerator DebuffRandomButSelf(int amount, int level, int position, BuffType bufftype, GameObject self, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);

    //     GameObject enemy = GetRandomAliveEnemy();
    //     while (enemy == self)
    //     {
    //         enemy = GetRandomAliveEnemy();
    //     }
    //     DebuffEnemy(bufftype, position, amount, level, startPosi, adjustAnimationPosition, adjustAnimationRotation);

    // }
    // public IEnumerator BuffRandomButSelf(int amount, int level, BuffType bufftype, GameObject self, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, bool allowState)
    // {
    //     if (allowState)
    //     {
    //         yield return new WaitForSeconds(castDelay);

    //         GameObject enemy = GetRandomAliveEnemy();
    //         while (enemy == self)
    //         {
    //             enemy = GetRandomAliveEnemy();
    //         }
    //         StartCoroutine(BuffEnemy(bufftype, amount, self.GetComponent<BaseEnemy>().position, level, startPosi, adjustAnimationPosition, adjustAnimationRotation, 0, allowState));
    //     }
    //     else
    //     {
    //         yield return new WaitForSeconds(0);
    //     }
    // }

    // //Buff
    // public IEnumerator DebuffSelf(BuffType bufftype, int amount, int level, Vector3 startPosi, Vector3 animationPosition, Vector3 animationRotation, float castDelay, bool allowState)
    // {
    //     if (allowState)
    //     {
    //         yield return new WaitForSeconds(castDelay);
    //         DebuffGamer(bufftype, amount, level, startPosi, animationPosition, animationRotation);
    //     }
    //     else
    //     {
    //         yield return new WaitForSeconds(0);

    //     }


    // }

    // public bool ListContains(List<int> list, int value)
    // {
    //     bool result = false;
    //     for (int i = 0; i < list.Count; i++)
    //     {
    //         if (list[i] == value)
    //         {
    //             return true;
    //         }
    //     }
    //     return result;
    // }

    // private void DebuffEnemy(BuffType bufftype, int position, int value, int level, Vector3 startPosi, Vector3 animationPosition, Vector3 animationRotation)
    // {
    //     string temp_path = "";

    //     //Debuff
    //     GameObject enemy = GameObject.Find("EnemyPanel").transform.GetChild(position).gameObject;
    //     switch (bufftype)
    //     {
    //         case BuffType.Bleed:
    //             enemy.GetComponent<BaseEnemy>().GetBleed(value);
    //             temp_path = "Buffs/BleedFX";
    //             break;
    //         case BuffType.Broken:
    //             enemy.GetComponent<BaseEnemy>().GetBroken(value, level);
    //             temp_path = "Buffs/BrokenFX";
    //             break;
    //         case BuffType.Burned:
    //             enemy.GetComponent<BaseEnemy>().GetBurn(value);
    //             temp_path = "Buffs/BurnFX";
    //             break;
    //         case BuffType.Poisoned:
    //             enemy.GetComponent<BaseEnemy>().GetPoison(value);
    //             temp_path = "Buffs/PoisonedFX";
    //             break;
    //         case BuffType.Stunned:
    //             enemy.GetComponent<BaseEnemy>().GetStunned(value, EventManager.StunAfterXRound);
    //             temp_path = "Buffs/StunFX";
    //             break;
    //         case BuffType.Weaken:
    //             enemy.GetComponent<BaseEnemy>().GetStrength(value);
    //             temp_path = "Buffs/WeakenFX";
    //             break;
    //         case BuffType.MagicBomb:
    //             enemy.GetComponent<BaseEnemy>().GetMagicBomb(value);
    //             temp_path = "Buffs/StunFX";
    //             break;
    //         case BuffType.Blind:
    //             enemy.GetComponent<BaseEnemy>().GetBlind(value, level);
    //             temp_path = "Buffs/StunFX";
    //             break;
    //     }
    //     if (value > 0)
    //     {
    //         GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + temp_path);

    //         Vector3 temp_position = enemy.transform.position;

    //         if (startPosi != new Vector3(0, 0, 0))
    //         {
    //             temp_position = startPosi;
    //         }
    //         temp_position += animationPosition;
    //         temp_position.z = 0;
    //         GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //         Vector3 rotationToLeft = new Vector3(0, 0, 0f);
    //         rotationToLeft += animationRotation;
    //         animation_prefab_clone.transform.localRotation = Quaternion.Euler(rotationToLeft);

    //         EventManager.StunAfterXRound = 0;



    //         Destroy(animation_prefab_clone, destroyTime);
    //         EndSkill();
    //     }
    // }
    // private void DebuffGamer(BuffType bufftype, int value, int level, Vector3 startPosi, Vector3 animationPosition, Vector3 animationRotation)
    // {

    //     string temp_path = "";
    //     switch (bufftype)
    //     {
    //         case BuffType.Bleed:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetBleed(value);
    //             temp_path = "Buffs/BleedFX";
    //             break;
    //         case BuffType.Broken:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetBroken(value, level);
    //             temp_path = "Buffs/BrokenFX";
    //             break;
    //         case BuffType.Burned:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetBurn(value);
    //             temp_path = "Buffs/BurnFX";
    //             break;
    //         case BuffType.Poisoned:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetPoison(value);
    //             temp_path = "Buffs/PoisonedFX";
    //             break;
    //         case BuffType.Stunned:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetStunned(value, EventManager.StunAfterXRound);
    //             temp_path = "Buffs/StunFX";
    //             break;
    //         case BuffType.Weaken:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetWeakened(value, level);
    //             temp_path = "Buffs/WeakenFX";
    //             break;
    //     }
    //     if (value > 0)
    //     {
    //         EventManager.StunAfterXRound = 0;

    //         //Animation

    //         GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + temp_path);
    //         Vector3 temp_position = GameObject.Find("Player").transform.GetChild(0).transform.position;

    //         if (startPosi != new Vector3(0, 0, 0))
    //         {
    //             temp_position = startPosi;
    //         }
    //         temp_position += animationPosition;
    //         temp_position.z = 0;
    //         GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //         Vector3 rotationToLeft = new Vector3(0, 0, 0f);
    //         rotationToLeft += animationRotation;
    //         animation_prefab_clone.transform.localRotation = Quaternion.Euler(rotationToLeft);
    //         Destroy(animation_prefab_clone, destroyTime);
    //         EndSkill();
    //     }
    // }
    // public IEnumerator BuffSelf(BuffType bufftype, int amount, int level, Vector3 startPosi, Vector3 animationPosition, Vector3 animationRotation, float castDelay)
    // {
    //     yield return new WaitForSeconds(castDelay);

    //     string temp_path = "";
    //     switch (bufftype)
    //     {
    //         case BuffType.Armor:
    //             amount = GameObject.Find("Player").GetComponent<BaseGamer>().GetArmor(amount);
    //             temp_path = "Buffs/ArmorFX";
    //             break;
    //         case BuffType.Strength:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetStrength(amount);
    //             temp_path = "Buffs/StrengthFX";
    //             break;
    //         case BuffType.Empower:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetEmpower(amount, level);
    //             temp_path = "Buffs/StrengthFX";
    //             break;
    //         case BuffType.Shield:
    //             amount = GameObject.Find("Player").GetComponent<BaseGamer>().GetShield(amount);
    //             temp_path = "Buffs/ArmorFX";
    //             break;
    //         case BuffType.Healing:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetHeal(amount, 9999);
    //             temp_path = "Buffs/HealingFX";
    //             break;
    //         case BuffType.Dodge:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetDodge(amount);
    //             temp_path = "Buffs/DodgeFX";
    //             break;
    //         case BuffType.Stability:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetStability(amount);
    //             temp_path = "Buffs/DodgeFX";
    //             break;
    //         case BuffType.IronBody:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetIronBody(amount);
    //             temp_path = "Buffs/DodgeFX";
    //             break;
    //         case BuffType.Superior:
    //             GameObject.Find("Player").GetComponent<BaseGamer>().GetSuperior(amount);
    //             temp_path = "Buffs/StrengthFX";
    //             break;
    //     }
    //     if (amount > 0)
    //     {


    //         //Animation
    //         GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + temp_path);
    //         Vector3 temp_position = GameObject.Find("Player").transform.GetChild(0).gameObject.transform.position;
    //         if (startPosi != new Vector3(0, 0, 0))
    //         {
    //             temp_position = startPosi;
    //         }
    //         temp_position += animationPosition;
    //         temp_position.z = 0;
    //         GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //         Vector3 rotationToLeft = new Vector3(0, 0, 0f);
    //         rotationToLeft += animationRotation;
    //         animation_prefab_clone.transform.localRotation = Quaternion.Euler(rotationToLeft);
    //         Destroy(animation_prefab_clone, destroyTime);
    //         EndSkill();
    //     }
    // }
    // public IEnumerator BuffEnemy(BuffType bufftype, int amount, int position, int level, Vector3 startPosi, Vector3 animationPosition, Vector3 animationRotation, float castDelay, bool allowState)
    // {
    //     if (allowState)
    //     {
    //         yield return new WaitForSeconds(castDelay);
    //         string temp_path = "";
    //         switch (bufftype)
    //         {
    //             case BuffType.Armor:
    //                 amount = GameObject.Find("EnemyPanel").transform.GetChild(position).GetComponent<BaseEnemy>().GetArmor(amount);
    //                 temp_path = "Buffs/ArmorFX";
    //                 break;
    //             case BuffType.Strength:
    //                 GameObject.Find("EnemyPanel").transform.GetChild(position).GetComponent<BaseEnemy>().GetStrength(amount);
    //                 temp_path = "Buffs/StrengthFX";
    //                 break;
    //             case BuffType.Empower:
    //                 GameObject.Find("EnemyPanel").transform.GetChild(position).GetComponent<BaseEnemy>().GetEmpower(amount, level);
    //                 temp_path = "Buffs/StrengthFX";
    //                 break;
    //             case BuffType.Shield:
    //                 amount = GameObject.Find("EnemyPanel").transform.GetChild(position).GetComponent<BaseEnemy>().GetShield(amount);
    //                 temp_path = "Buffs/ArmorFX";
    //                 break;
    //             case BuffType.Healing:
    //                 GameObject.Find("EnemyPanel").transform.GetChild(position).GetComponent<BaseEnemy>().GetHeal(amount);
    //                 temp_path = "Buffs/HealingFX";
    //                 break;
    //             case BuffType.Dodge:
    //                 GameObject.Find("EnemyPanel").transform.GetChild(position).GetComponent<BaseEnemy>().GetDodge(amount);
    //                 temp_path = "Buffs/DodgeFX";
    //                 break;
    //             case BuffType.Spike:
    //                 GameObject.Find("EnemyPanel").transform.GetChild(position).GetComponent<BaseEnemy>().GetSpike(amount);
    //                 temp_path = "Buffs/DodgeFX";
    //                 break;
    //             case BuffType.Fortitude:
    //                 GameObject.Find("EnemyPanel").transform.GetChild(position).GetComponent<BaseEnemy>().GetFortitude(amount);
    //                 temp_path = "Buffs/FortitudeFX";
    //                 break;
    //         }
    //         if (amount > 0)
    //         {

    //             //Animation
    //             GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + temp_path);
    //             Vector3 temp_position = GameObject.Find("EnemyPanel").transform.GetChild(position).GetChild(0).GetChild(0).gameObject.transform.position;
    //             if (startPosi != new Vector3(0, 0, 0))
    //             {
    //                 temp_position = startPosi;
    //             }
    //             temp_position += animationPosition;
    //             temp_position.z = 0;
    //             GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //             Vector3 rotationToLeft = new Vector3(0, 0, 0f);
    //             rotationToLeft += animationRotation;
    //             animation_prefab_clone.transform.localRotation = Quaternion.Euler(rotationToLeft);
    //             Destroy(animation_prefab_clone, destroyTime);
    //             EndSkill();
    //         }
    //     }
    //     else
    //     {
    //         yield return new WaitForSeconds(0);
    //     }
    // }
    // public IEnumerator DamageSelf(int damage, int position, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float multiFaktor, bool allowDamage)
    // {
    //     if (allowDamage)
    //     {
    //         yield return new WaitForSeconds(castDelay);
    //         GameObject target;
    //         //Wegen Familiar
    //         if (position != 9999)
    //         {
    //             target = EventManager.chosenEnemy; //Falls Gegner dran: Familiar oder Gamer?
    //         }
    //         else
    //         {
    //             target = EventManager.chosenSelf;
    //         }
    //         damage = Mathf.RoundToInt(damage * multiFaktor);
    //         GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);

    //         Vector3 temp_position = target.gameObject.transform.position;
    //         if (target == GameObject.Find("Player").gameObject)
    //         {
    //             temp_position = target.transform.GetChild(0).gameObject.transform.position;
    //         }


    //         if (startPosi != new Vector3(0, 0, 0))
    //         {
    //             temp_position = startPosi;
    //         }
    //         temp_position.z = 0;
    //         temp_position += adjustAnimationPosition;
    //         GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //         Vector3 temp_rotation = new Vector3(0, 0, 0);
    //         temp_rotation += adjustAnimationRotation;
    //         animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);

    //         StartCoroutine(DealSelfDamageAnimationAfterAnimation(target, damage, position, animation_prefab_clone));
    //         Destroy(animation_prefab_clone, destroyTime);
    //     }
    //     else
    //     {
    //         GameObject.Find("Player").GetComponent<BasePlayer>().GetDodge(-1);
    //         yield return new WaitForSeconds(0);

    //     }

    // }
    // public IEnumerator DamagePureSelf(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float multiFaktor)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     damage = Mathf.RoundToInt(damage * multiFaktor);
    //     GameObject.Find("Player").GetComponent<BaseGamer>().GetPureDamage(damage, 9999); //inital Target
    //                                                                                      //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = GameObject.Find("Player").transform.GetChild(0).gameObject.transform.position;

    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animation_prefab_clone, destroyTime);
    // }
    // public IEnumerator DamageSelfNoShield(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float multiFaktor)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     damage = Mathf.RoundToInt(damage * multiFaktor);
    //     GameObject.Find("Player").GetComponent<BaseGamer>().GetDamageNoShield(damage, 9999); //inital Target
    //                                                                                          //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = GameObject.Find("Player").transform.GetChild(0).gameObject.transform.position;

    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animation_prefab_clone, destroyTime);
    // }
    // public IEnumerator DamageSelfNoShieldAndArmor(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float multiFaktor)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     damage = Mathf.RoundToInt(damage * multiFaktor);
    //     GameObject.Find("Player").GetComponent<BaseGamer>().GetDamageNoShieldAndArmor(damage, 9999); //inital Target
    //                                                                                                  //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = GameObject.Find("Player").transform.GetChild(0).gameObject.transform.position;

    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animation_prefab_clone, destroyTime);
    // }
    // public IEnumerator DamagePureSelfNoShieldAndArmor(int damage, string animationPath, Vector3 startPosi, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float multiFaktor)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     damage = Mathf.RoundToInt(damage * multiFaktor);
    //     GameObject.Find("Player").GetComponent<BaseGamer>().GetDamagePureNoShieldAndArmor(damage, 9999); //inital Target
    //                                                                                                      //Animation
    //     GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
    //     Vector3 temp_position = GameObject.Find("Player").transform.GetChild(0).gameObject.transform.position;

    //     if (startPosi != new Vector3(0, 0, 0))
    //     {
    //         temp_position = startPosi;
    //     }
    //     temp_position.z = 0;
    //     temp_position += adjustAnimationPosition;
    //     GameObject animation_prefab_clone = (GameObject)Instantiate(animation_prefab, temp_position, Quaternion.identity) as GameObject;
    //     Vector3 temp_rotation = new Vector3(0, 0, 0);
    //     temp_rotation += adjustAnimationRotation;
    //     animation_prefab_clone.transform.localRotation = Quaternion.Euler(temp_rotation);
    //     Destroy(animation_prefab_clone, destroyTime);
    // }

    // public void SetSkillTimer(int countdown, int triggerID, GameObject chosenEnemy, int damage)
    // {

    //     if (EventManager.countdown1 == 0)
    //     {
    //         EventManager.countdown1 = countdown;
    //         triggerID1 = triggerID;
    //         chosenEnemyTimer1 = chosenEnemy;
    //         Debug.Log("triggerID " + triggerID);
    //         Debug.Log("countdown: " + EventManager.countdown1);
    //         damageTimer1 = damage;
    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown1);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 0f;
    //         temp_position2.z = 0;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);

    //     }
    //     else if (EventManager.countdown2 == 0)
    //     {
    //         EventManager.countdown2 = countdown;
    //         triggerID2 = triggerID;
    //         chosenEnemyTimer2 = chosenEnemy;
    //         damageTimer2 = damage;
    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown2);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 1.5f;
    //         temp_position2.z = 0;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);

    //     }
    //     else if (EventManager.countdown3 == 0)
    //     {
    //         EventManager.countdown3 = countdown;
    //         triggerID3 = triggerID;
    //         chosenEnemyTimer3 = chosenEnemy;
    //         damageTimer3 = damage;
    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown3);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 3f;
    //         temp_position2.z = 0;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);

    //     }
    //     else if (EventManager.countdown4 == 0)
    //     {
    //         EventManager.countdown4 = countdown;
    //         triggerID4 = triggerID;
    //         chosenEnemyTimer4 = chosenEnemy;
    //         damageTimer4 = damage;
    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown4);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 3f;
    //         temp_position2.z -= 1.5f;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);

    //     }
    //     else if (EventManager.countdown5 == 0)
    //     {
    //         EventManager.countdown5 = countdown;
    //         triggerID5 = triggerID;
    //         chosenEnemyTimer5 = chosenEnemy;
    //         damageTimer5 = damage;
    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown5);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 3f;
    //         temp_position2.z -= 3f;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);

    //     }
    // }

    // public GameObject FindSkillObjectByID(int checkID)
    // {
    //     GameObject skillSlot = null;
    //     for (int i = 0; i < 12; i++)
    //     {
    //         if (GameObject.Find("SlotPanel" + i).transform.GetChild(0).GetComponent<Skill>().id == checkID)
    //         {
    //             skillSlot = GameObject.Find("SlotPanel" + i).transform.GetChild(0).gameObject;
    //         }
    //     }
    //     if (skillSlot == null)
    //     {
    //         for (int i = 0; i < 6; i++)
    //         {
    //             try
    //             {
    //                 if (GameObject.Find("SupportSlot" + i).transform.GetChild(0).GetComponent<Skill>().id == checkID)
    //                 {
    //                     skillSlot = GameObject.Find("SupportSlot" + i).transform.GetChild(0).gameObject;
    //                 }
    //             }
    //             catch { }
    //         }
    //     }
    //     return skillSlot;
    // }
    // public GameObject FindConsumableObjectByID(int checkID)
    // {
    //     GameObject consumableSlot = null;
    //     for (int i = 0; i < 3; i++)
    //     {
    //         if (GameObject.Find("ConsumableSlot" + i).transform.GetChild(0).GetComponent<Consumable>().id == checkID)
    //         {
    //             consumableSlot = GameObject.Find("ConsumableSlot" + i).transform.GetChild(0).gameObject;
    //         }
    //     }

    //     return consumableSlot;
    // }

    // public bool CountdownSet(int triggerID)
    // {
    //     bool result = false;
    //     if (EventManager.countdown1 > 0 && triggerID1 == triggerID || EventManager.countdown2 > 0 && triggerID2 == triggerID || EventManager.countdown3 > 0 && triggerID3 == triggerID || EventManager.countdown4 > 0 && triggerID4 == triggerID || EventManager.countdown5 > 0 && triggerID5 == triggerID)
    //     {
    //         result = true;
    //     }

    //     return result;

    // }

    // public void CheckCountdown(int triggerID)
    // {
    //     if (EventManager.countdown1 > 0 && triggerID1 == triggerID)
    //     {

    //         EventManager.countdown1--;
    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown1);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 0;
    //         temp_position2.z = 0;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);
    //         if (EventManager.countdown1 == 0)
    //         {
    //             if (chosenConsumableTimer1 != null)
    //             {
    //                 chosenConsumableTimer1.GetComponent<Consumable>().ConsumableEffectTimer(chosenEnemyTimer1, damageTimer1);
    //                 chosenConsumableTimer1 = null;
    //             }
    //             else
    //             {
    //                 GameObject skillSlot = FindSkillObjectByID(triggerID);
    //                 skillSlot.GetComponent<Skill>().SkillDamageTimer(chosenEnemyTimer1, damageTimer1);

    //             }
    //             triggerID1 = 9999;
    //         }
    //     }

    //     if (EventManager.countdown2 > 0 && triggerID2 == triggerID)
    //     {
    //         EventManager.countdown2--;

    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown2);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 1.5f;
    //         temp_position2.z = 0;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);



    //         if (EventManager.countdown2 == 0)
    //         {
    //             if (chosenConsumableTimer2 != null)
    //             {
    //                 chosenConsumableTimer2.GetComponent<Consumable>().ConsumableEffectTimer(chosenEnemyTimer2, damageTimer2);
    //                 chosenConsumableTimer2 = null;
    //             }
    //             else
    //             {
    //                 GameObject skillSlot = FindSkillObjectByID(triggerID);
    //                 skillSlot.GetComponent<Skill>().SkillDamageTimer(chosenEnemyTimer2, damageTimer2);

    //             }
    //             triggerID2 = 9999;
    //         }
    //     }
    //     if (EventManager.countdown3 > 0 && triggerID3 == triggerID)
    //     {
    //         EventManager.countdown3--;

    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown3);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 3f;
    //         temp_position2.z = 0;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);



    //         if (EventManager.countdown3 == 0)
    //         {
    //             if (chosenConsumableTimer3 != null)
    //             {
    //                 chosenConsumableTimer3.GetComponent<Consumable>().ConsumableEffectTimer(chosenEnemyTimer3, damageTimer3);
    //                 chosenConsumableTimer3 = null;
    //             }
    //             else
    //             {
    //                 GameObject skillSlot = FindSkillObjectByID(triggerID);
    //                 skillSlot.GetComponent<Skill>().SkillDamageTimer(chosenEnemyTimer3, damageTimer3);

    //             }
    //             triggerID3 = 9999;
    //         }
    //     }
    //     if (EventManager.countdown4 > 0 && triggerID4 == triggerID)
    //     {
    //         EventManager.countdown4--;


    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown4);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 3f;
    //         temp_position2.x -= 1.5f;
    //         temp_position2.z = 0;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);


    //         if (EventManager.countdown4 == 0)
    //         {
    //             if (chosenConsumableTimer4 != null)
    //             {
    //                 chosenConsumableTimer4.GetComponent<Consumable>().ConsumableEffectTimer(chosenEnemyTimer4, damageTimer4);
    //                 chosenConsumableTimer4 = null;
    //             }
    //             else
    //             {
    //                 GameObject skillSlot = FindSkillObjectByID(triggerID);
    //                 skillSlot.GetComponent<Skill>().SkillDamageTimer(chosenEnemyTimer4, damageTimer4);

    //             }
    //             triggerID4 = 9999;
    //         }
    //     }
    //     if (EventManager.countdown5 > 0 && triggerID5 == triggerID)
    //     {
    //         EventManager.countdown5--;

    //         // Animation
    //         GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/TimerFX" + EventManager.countdown5);
    //         Vector3 temp_position2 = GameObject.Find("Enemy").transform.position;
    //         temp_position2.y += 3f;
    //         temp_position2.x -= 3f;
    //         temp_position2.z = 0;

    //         GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //         animation_prefab_clone2.transform.localRotation = Quaternion.Euler(0f, -90f, 90f);


    //         if (EventManager.countdown5 == 0)
    //         {
    //             if (chosenConsumableTimer5 != null)
    //             {
    //                 chosenConsumableTimer5.GetComponent<Consumable>().ConsumableEffectTimer(chosenEnemyTimer5, damageTimer5);
    //                 chosenConsumableTimer5 = null;
    //             }
    //             else
    //             {
    //                 GameObject skillSlot = FindSkillObjectByID(triggerID);
    //                 skillSlot.GetComponent<Skill>().SkillDamageTimer(chosenEnemyTimer5, damageTimer5);

    //             }
    //             triggerID5 = 9999;
    //         }
    //     }


    // }
    // public int ConsumAllEnergy()
    // {
    //     int curEnergy = EventManager.chosenSelf.GetComponent<BaseGamer>().curEnergy;
    //     EventManager.chosenSelf.GetComponent<BaseGamer>().curEnergy = 0;
    //     GameObject.Find("EndTurnButton").transform.GetChild(2).GetComponent<ChangeEnergyLevel>().RefreshEnergyLevel();
    //     return curEnergy;

    // }
    // public void GainEnergy(int energyToGain)
    // {
    //     /*  PlayerPrefSerialize.LoadPlayerValues();
    //      PlayerPrefSerialize.playerValues.maxEnergy += energyToGain;
    //      PlayerPrefSerialize.SavePlayerValues(); */
    //     GameObject.Find("Player").GetComponent<BaseGamer>().curEnergy += energyToGain;
    //     GameObject.Find("EndTurnButton").transform.GetChild(2).GetComponent<ChangeEnergyLevel>().RefreshEnergyLevel();
    //     GameObject.Find("EndTurnButton").transform.GetChild(0).GetComponent<EnergyChart>().UpdateEnergy();
    // }
    // public void ReduceCooldownOfAllSupportSkills(int reduceCooldown)
    // {
    //     for (int i = 0; i < 6; i++)
    //     {
    //         GameObject tempSupport = GameObject.Find("SupportSlot" + i).transform.GetChild(0).gameObject;
    //         if (tempSupport.GetComponent<Skill>().cooldown > 0)
    //         {
    //             tempSupport.GetComponent<Skill>().cooldown -= reduceCooldown;
    //             if (tempSupport.GetComponent<Skill>().cooldown < 0)
    //             {
    //                 tempSupport.GetComponent<Skill>().cooldown = 0;
    //             }
    //             tempSupport.gameObject.GetComponent<ToggleSupport>().TurnOffInteractable();
    //             tempSupport.gameObject.transform.GetChild(0).GetComponent<Text>().text = "" + cooldown + "";
    //             if (cooldown == 0)
    //             {
    //                 tempSupport.gameObject.GetComponent<ToggleSupport>().TurnOnInteractable();
    //                 tempSupport.gameObject.GetComponent<ToggleSupport>().TurnOff();
    //                 tempSupport.gameObject.transform.GetChild(0).gameObject.SetActive(false);
    //             }
    //         }
    //     }
    // }

    // public void DeactivateSkills(int skillsToDeactive)
    // {

    //     List<int> deactiveClockSkills = new List<int>();
    //     for (int i = 0; i < 12; i++)
    //     {
    //         if (GameObject.Find("SlotPanel" + i).transform.GetChild(0).GetComponent<Toggle>().interactable)
    //         {
    //             deactiveClockSkills.Add(i);
    //         }
    //     }
    //     if (deactiveClockSkills.Count >= skillsToDeactive)
    //     {
    //         for (int j = 0; j < skillsToDeactive; j++)
    //         {
    //             deactiveClockSkills.Clear();
    //             for (int i = 0; i < 12; i++)
    //             {
    //                 if (GameObject.Find("SlotPanel" + i).transform.GetChild(0).GetComponent<Toggle>().interactable)
    //                 {
    //                     deactiveClockSkills.Add(i);
    //                 }
    //             }
    //             int deactiveSkill = Random.Range(0, deactiveClockSkills.Count);
    //             GameObject.Find("SlotPanel" + deactiveSkill).transform.GetChild(0).GetComponent<Toggle>().interactable = false;
    //             deactiveClockSkills.Clear();
    //         }
    //         //SkillDamageTimer(EventManager.chosenEnemy);
    //     }
    //     else
    //     {
    //         EventManager.chosenSelf.GetComponent<BaseGamer>().curEnergy++;
    //         GameObject.Find("EndTurnButton").transform.GetChild(2).GetComponent<ChangeEnergyLevel>().RefreshEnergyLevel();
    //     }
    // }
    // public void ActivateSkills(int skillsToActive)
    // {
    //     List<int> activeClockSkills = new List<int>();
    //     for (int i = 0; i < 12; i++)
    //     {
    //         if (GameObject.Find("SlotPanel" + i).transform.GetChild(0).GetComponent<Toggle>().interactable == false)
    //         {
    //             activeClockSkills.Add(i);
    //         }
    //     }
    //     if (activeClockSkills.Count < skillsToActive)
    //     {
    //         skillsToActive = activeClockSkills.Count;
    //     }
    //     for (int j = 0; j < skillsToActive; j++)
    //     {
    //         int activeSkill = Random.Range(0, activeClockSkills.Count);
    //         GameObject.Find("SlotPanel" + activeClockSkills[activeSkill]).transform.GetChild(0).GetComponent<Toggle>().isOn = false;
    //         GameObject.Find("SlotPanel" + activeClockSkills[activeSkill]).transform.GetChild(0).GetComponent<Toggle>().interactable = true;

    //         activeClockSkills.Clear();
    //         for (int i = 0; i < 12; i++)
    //         {
    //             if (GameObject.Find("SlotPanel" + i).transform.GetChild(0).GetComponent<Toggle>().interactable == false)
    //             {
    //                 activeClockSkills.Add(i);
    //             }
    //         }
    //     }
    //     // SkillDamageTimer(EventManager.chosenEnemy);


    //     /* {
    //         EventManager.chosenSelf.GetComponent<BaseGamer>().curEnergy++;
    //         GameObject.Find("EndTurnButton").transform.GetChild(2).GetComponent<ChangeEnergyLevel>().RefreshEnergyLevel();
    //     } */
    // }
    // public IEnumerator TriggerBurn(float castDelay, GameObject chosenEnemy)
    // {
    //     yield return new WaitForSeconds(castDelay);
    //     foreach (Transform child in chosenEnemy.transform.GetChild(0).GetChild(2))
    //     {
    //         if (child.name == "Burned")
    //         {
    //             child.GetComponent<Burned>().BurnDamage();
    //         }
    //     }
    // }

    // //
    // public int DamageThatWouldBeDealt()
    // {
    //     int damage = 0;
    //     int finalDamage = 0;
    //     CombatPrefSerialize.LoadCombatValues();
    //     if (EventManager.chosenSkill.GetComponent<Skill>().targetType == TargetType.AOE)
    //     {
    //         damage = EventManager.chosenSkill.GetComponent<Skill>().damageAOE;
    //         damage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);
    //         int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //         for (int i = 0; i < enemysLeft; i++)
    //         {

    //             finalDamage += GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);

    //         }

    //     }
    //     else if (EventManager.chosenSkill.GetComponent<Skill>().targetType == TargetType.InitPlus)
    //     {
    //         damage = EventManager.chosenSkill.GetComponent<Skill>().damageAOE;
    //         damage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);

    //         //Init
    //         finalDamage += EventManager.chosenEnemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);
    //         //Rest
    //         int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //         for (int i = 0; i < enemysLeft; i++)
    //         {

    //             if (GameObject.Find("EnemyPanel").transform.GetChild(i).gameObject != EventManager.chosenEnemy)
    //             {
    //                 finalDamage += GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);
    //             }


    //         }
    //     }
    //     else if (EventManager.chosenSkill.GetComponent<Skill>().targetType == TargetType.Adjacent)
    //     {
    //         int damageAdjacent = EventManager.chosenSkill.GetComponent<Skill>().damageAdjacent;
    //         damage = EventManager.chosenSkill.GetComponent<Skill>().damageSingle;
    //         damage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);
    //         damageAdjacent = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damageAdjacent);
    //         int position = EventManager.chosenEnemy.GetComponent<BaseEnemy>().position;
    //         //Adjacent Damage
    //         if (!(position + 1 > 4))
    //         {
    //             try
    //             {
    //                 EventManager.chosenEnemy.transform.parent.GetChild(position + 1).GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damageAdjacent);
    //             }
    //             catch { }
    //         }

    //         if (!(position - 1 < 0) && EventManager.chosenEnemy.transform.parent.GetChild(position - 1).gameObject != null)
    //         {
    //             try
    //             {
    //                 EventManager.chosenEnemy.transform.parent.GetChild(position - 1).GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damageAdjacent);
    //             }
    //             catch { }
    //         }
    //         finalDamage += EventManager.chosenEnemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);
    //     }
    //     else if (EventManager.chosenSkill.GetComponent<Skill>().targetType == TargetType.Single)
    //     {
    //         damage = EventManager.chosenSkill.GetComponent<Skill>().damageSingle;
    //         damage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);
    //         finalDamage = EventManager.chosenEnemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);
    //     }
    //     else if (EventManager.chosenSkill.GetComponent<Skill>().targetType == TargetType.PureSingle)
    //     {
    //         damage = EventManager.chosenSkill.GetComponent<Skill>().pureSingle;
    //         finalDamage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);
    //     }
    //     damage = baseGamer.CalcDealDamage(damage); // FOR TOOLTIPS
    //     int result = finalDamage;

    //     return result;
    // }
    // public int DamageThatWouldBeDealt2(TargetType targetType, int damage, int damageAdjacent)
    // {
    //     int finalDamage = 0;
    //     CombatPrefSerialize.LoadCombatValues();
    //     if (targetType == TargetType.AOE)
    //     {
    //         damage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);
    //         int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //         for (int i = 0; i < enemysLeft; i++)
    //         {

    //             finalDamage += GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);

    //         }

    //     }
    //     else if (targetType == TargetType.InitPlus)
    //     {
    //         damage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);

    //         //Init
    //         finalDamage += EventManager.chosenEnemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);
    //         //Rest
    //         int enemysLeft = CombatPrefSerialize.combatValues.enemysToKill;
    //         for (int i = 0; i < enemysLeft; i++)
    //         {

    //             if (GameObject.Find("EnemyPanel").transform.GetChild(i).gameObject != EventManager.chosenEnemy)
    //             {
    //                 finalDamage += GameObject.Find("EnemyPanel").transform.GetChild(i).GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);
    //             }


    //         }
    //     }
    //     else if (targetType == TargetType.Adjacent)
    //     {
    //         damage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);
    //         damageAdjacent = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damageAdjacent);
    //         int position = EventManager.chosenEnemy.GetComponent<BaseEnemy>().position;
    //         //Adjacent Damage
    //         if (!(position + 1 > 4))
    //         {
    //             try
    //             {
    //                 EventManager.chosenEnemy.transform.parent.GetChild(position + 1).GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damageAdjacent);
    //             }
    //             catch { }
    //         }

    //         if (!(position - 1 < 0) && EventManager.chosenEnemy.transform.parent.GetChild(position - 1).gameObject != null)
    //         {
    //             try
    //             {
    //                 EventManager.chosenEnemy.transform.parent.GetChild(position - 1).GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damageAdjacent);
    //             }
    //             catch { }
    //         }
    //         finalDamage += EventManager.chosenEnemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);
    //     }
    //     else if (EventManager.chosenSkill.GetComponent<Skill>().targetType == TargetType.Single)
    //     {
    //         damage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);
    //         finalDamage = EventManager.chosenEnemy.GetComponent<BaseEnemy>().CalcReceiveDamageSimulator(damage);
    //     }
    //     else if (EventManager.chosenSkill.GetComponent<Skill>().targetType == TargetType.PureSingle)
    //     {
    //         damage = EventManager.chosenSkill.GetComponent<Skill>().pureSingle;
    //         finalDamage = GameObject.Find("Player").GetComponent<BaseGamer>().CalcDealDamage(damage);
    //     }
    //     damage = baseGamer.CalcDealDamage(damage); // FOR TOOLTIPS
    //     int result = finalDamage;

    //     return result;
    // }
    // public void SpawnFamiliar(int hp)
    // {
    //     //Animation Spawn
    //     GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/FamiliarFX");
    //     Vector3 temp_position2 = GameObject.Find("FamiliarSpawnPoint").transform.position;
    //     temp_position2.z = 0;
    //     GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //     animation_prefab_clone2.transform.localRotation = Quaternion.Euler(-90f, -90f, 0f);
    //     Destroy(animation_prefab_clone2, 3f);
    //     StartCoroutine(Spawn(0.5f, animation_prefab_clone2, hp));

    // }
    // IEnumerator Spawn(float timer, GameObject clone, int hp)
    // {
    //     yield return new WaitForSeconds(timer);
    //     //Bild aktivieren
    //     GameObject Familiar = GameObject.Find("Player").transform.GetChild(7).gameObject;
    //     Familiar.SetActive(true);
    //     Familiar.GetComponent<Image>().sprite = Resources.Load<Sprite>("Consumables/Pictures/best_friend");
    //     Color alpha = Familiar.GetComponent<Image>().color;
    //     alpha.a = 0;
    //     Familiar.GetComponent<Image>().color = alpha;
    //     Familiar.transform.GetComponent<BaseFamiliar>().SetHP(hp);
    //     Familiar.transform.GetChild(0).GetComponent<SetFamiliarHP>().SetHP();
    //     Familiar.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<SetFamiliarHP>().SetHP();
    // }


}

