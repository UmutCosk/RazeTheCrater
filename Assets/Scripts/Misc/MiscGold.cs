﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MiscGold : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        UpdateGold();
    }

    // Update is called once per frame
    public void UpdateGold()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        this.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = PlayerPrefSerialize.playerValues.Gold.ToString();


    }
}
