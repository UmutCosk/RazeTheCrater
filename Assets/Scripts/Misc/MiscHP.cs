﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class MiscHP : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        UpdateHP();
    }

    // Update is called once per frame
    public void UpdateHP()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        this.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = PlayerPrefSerialize.playerValues.curHP + " / " + PlayerPrefSerialize.playerValues.maxHP;

    }
}
