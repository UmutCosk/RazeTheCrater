﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GetOptionMenuWithEsc : MonoBehaviour {

  public GameObject OptionPanel;
  public GameObject SceneObject;


	public void RestartRun(){

		 SceneManager.LoadScene("ChooseCharacter");
	}

    public void SaveAndQuit(){
         Application.Quit();

	}
	void Update () {
		    if (Input.GetKeyDown(KeyCode.Escape)&& OptionPanel.activeSelf == false)
        {
	
            OptionPanel.SetActive(true);
			SceneObject.SetActive(false);

        }
        else if (Input.GetKeyDown(KeyCode.Escape) && OptionPanel.activeSelf == true)
        {
	
            OptionPanel.SetActive(false);
			SceneObject.SetActive(true);
        }


	}
}
