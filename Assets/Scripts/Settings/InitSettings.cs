﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitSettings : MonoBehaviour
{

    public Toggle fullScreenOn;
    public Slider musicVolumeSlider;
    public Slider effectVolumeSlider;
    public Dropdown resolutionDropdown;

    public Resolution[] resolutions;

    void Awake()
    {
        SetAllPossibleResolutions();
		
        fullScreenOn.isOn = convertToBool(PlayerPrefs.GetInt("FullScreen"));
        musicVolumeSlider.value = PlayerPrefs.GetFloat("MusicVolume");
        effectVolumeSlider.value = PlayerPrefs.GetFloat("EffectVolume");
        
      
        Screen.SetResolution(PlayerPrefs.GetInt("ResolutionWidth"), PlayerPrefs.GetInt("ResolutionHeight"), fullScreenOn.isOn);

    }


    void SetAllPossibleResolutions()
    {

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();


        List<string> options = new List<string>();
        int currentResolutionIndex = 0;
        currentResolutionIndex = PlayerPrefs.GetInt("ResolutionIndex");

        for (int i = 0; i < resolutions.Length; i = i + 1)
        {

            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);
            currentResolutionIndex = currentResolutionIndex + 1;
            if (resolutions[i].width == Screen.currentResolution.width &&
            resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }


        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = PlayerPrefs.GetInt("ResolutionIndex");
        resolutionDropdown.RefreshShownValue();

    }

	bool convertToBool(int intToConvert){
    if(intToConvert == 1){ return true;}
	else{return false;}
	}
}
