﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionOnChange : MonoBehaviour
{

    public Dropdown resolutionDropdown;

    InitSettings resolutionList;
    // Use this for initialization

    void Awake()
    {
        resolutionDropdown.value = PlayerPrefs.GetInt("ResolutionIndex");
        Screen.SetResolution(1920, 1080, Screen.fullScreen);
    }
    void Start()
    {
        resolutionDropdown.value = PlayerPrefs.GetInt("ResolutionIndex");
        resolutionDropdown.onValueChanged.AddListener(delegate { ResolutionChangeCheck(); });
    }

    // Update is called once per frame
    public void ResolutionChangeCheck()
    {

        resolutionList = GetComponent<InitSettings>();
        int index = resolutionDropdown.value;
        PlayerPrefs.SetInt("ResolutionIndex", index);
        PlayerPrefs.SetInt("ResolutionWidth", resolutionList.resolutions[index].width);
        PlayerPrefs.SetInt("ResolutionHeight", resolutionList.resolutions[index].height);

        Screen.SetResolution(1920, 1080, Screen.fullScreen);
        //Screen.SetResolution(PlayerPrefs.GetInt("ResolutionWidth"), PlayerPrefs.GetInt("ResolutionHeight"), Screen.fullScreen);
    }
}
