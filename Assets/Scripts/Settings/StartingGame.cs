﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingGame : MonoBehaviour
{

    // Use this for initialization
    void Awake()
    {
        GlobalPrefSerialize.LoadPlayerValues();
        if (GlobalPrefSerialize.globalValues.firstGame)
        {
            GlobalPrefSerialize.LockIfFirstGame();
            GlobalPrefSerialize.SaveGlobalValues();


            MapPrefSerialize.ResetMapValues();
            MapPrefSerialize.LoadMapValues();
            MapPrefSerialize.mapValues.NewGame = 1;
            MapPrefSerialize.mapValues.CurrentLevel = 1;
            MapPrefSerialize.mapValues.CurrentMonsterType = MonsterType.Start;
            MapPrefSerialize.SaveMapValues();

            PlayerPrefSerialize.LoadPlayerValues();
            PlayerPrefSerialize.ResetPlayerValues();
            PlayerPrefSerialize.SavePlayerValues();

            CombatPrefSerialize.ResetCombatValues();
            CombatPrefSerialize.LoadCombatValues();
            CombatPrefSerialize.combatValues.EnemySetIDsDone.Add(9999);
            CombatPrefSerialize.SaveCombatValues();
            Screen.SetResolution(1920, 1080, Screen.fullScreen);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
