﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class EffectSoundVolume : MonoBehaviour
{


    private AudioSource soundFX;
    public float timer;

    void Start()
    {
        soundFX = this.GetComponent<AudioSource>();
        if (timer != 0)
        {
            StartCoroutine(AnimationAfterTime(timer));
        }


    }
    public IEnumerator AnimationAfterTime(float timer)
    {
        yield return new WaitForSeconds(timer);
        soundFX.Play();
    }

    // Update is called once per frame
    void Update()
    {

        soundFX.volume = PlayerPrefs.GetFloat("EffectVolume");
    }
}
