﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using System;

public class BackgroundMusicVolume : MonoBehaviour
{


    private AudioSource backgroundMusic;
    AudioClip clip;
    void Start()
    {
        backgroundMusic = this.GetComponent<AudioSource>();

        //Load Background Music

        MapPrefSerialize.LoadMapValues();
        if (this.gameObject.scene.name == "Combat")
        {
            if (MapPrefSerialize.mapValues.CurrentStep < 11) //Normal Music
            {
                clip = Resources.Load<AudioClip>("Scenes/Combat/Sounds/Backgrounds/EnemyMusic");
                backgroundMusic.GetComponent<AudioSource>().clip = clip;
                backgroundMusic.GetComponent<AudioSource>().pitch = 0.75f;
                backgroundMusic.GetComponent<AudioSource>().spatialBlend = 0.5f;
                backgroundMusic.Play();
            }
            else if (MapPrefSerialize.mapValues.CurrentStep == 11) //Boss Music
            {
                clip = Resources.Load<AudioClip>("Scenes/Combat/Sounds/Backgrounds/BossMusic");
                backgroundMusic.GetComponent<AudioSource>().clip = clip;
                backgroundMusic.Play();
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (backgroundMusic != null)
        {
            backgroundMusic.volume = PlayerPrefs.GetFloat("MusicVolume");
        }

    }

    public void AedioxEnrage()
    {
        clip = Resources.Load<AudioClip>("Scenes/Combat/Sounds/Backgrounds/AedioxEnrage");
        backgroundMusic.GetComponent<AudioSource>().clip = clip;
        backgroundMusic.GetComponent<AudioSource>().pitch = 1.105f;
        backgroundMusic.Play();
    }
}
