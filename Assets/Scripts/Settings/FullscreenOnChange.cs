﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FullscreenOnChange : MonoBehaviour
{

    public Toggle fullscreenOn;

    // Use this for initialization
    void Start()
    {
        fullscreenOn.onValueChanged.AddListener(delegate { FullScreenChangeCheck(); });
    }

    // Update is called once per frame
    void FullScreenChangeCheck()
    {

        if (fullscreenOn == true)
        {
            Screen.fullScreen = true;
            PlayerPrefs.SetInt("FullScreen", 1);
        }
        else
        {
            Screen.fullScreen = false;
            PlayerPrefs.SetInt("FullScreen", 0);

        }

    }
}
