﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeOnChange : MonoBehaviour {

	    public Slider musicSlider;
		public Slider sfxSlider;


  public void Start()
    {
        //Adds a listener to the main slider and invokes a method when the value changes.
        musicSlider.onValueChanged.AddListener(delegate {MusicValueChangeCheck(); });
		sfxSlider.onValueChanged.AddListener(delegate {SfxValueChangeCheck(); });
		
    }

 // Invoked when the value of the slider changes.
    public void MusicValueChangeCheck()
    {
	PlayerPrefs.SetFloat("MusicVolume", musicSlider.value);
    }
	 // Invoked when the value of the slider changes.
    public void SfxValueChangeCheck()
    {
    PlayerPrefs.SetFloat("EffectVolume", sfxSlider.value);
    }
}
