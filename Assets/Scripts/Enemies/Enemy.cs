﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public enum Intention { Attack, Buff, Defend, Debuff, AttackBuff, AttackDefend, AttackDebuff }
public enum AttackMovement { Forward }

public class Enemy : MonoBehaviour
{
    // EventManager.chosenEnemy.GetComponent<BaseGamer>().GetDamage(damage, this.GetComponent<BaseEnemy>().position);
    /*     public BaseEnemy baseEnemy;
        EnemiesDatabase enemyDB; */


    public int id;
    public string title;
    public string scriptName;
    public string imagePath;
    public int size;

    public int damage1;
    public int damage2;
    public int damage3;
    public int damage4;
    public int damage5;

    public int damageDone;

    public GameObject gamerToAttack;
    public int Gamer1Familiar0;

    /*   public Intention[] intentionSequenz;
      public int[] attackSequenz;
      public int[] attackWishSequenz;
      public int[] damageSequenz;
      public int sequenzCounter = 0;
      public float endTurnInSecond; */

    public Animator animator;

    //Timers
    public bool allowAttack = false;
    public float sleepTime;

    public void AllowAttack()
    {
        allowAttack = true;
        sleepTime = Random.Range(0, EventManager2.roundTimer - 6.5f);
    }
    public void StopAttack()
    {
        allowAttack = false;
    }

    public void CastDrug(GameObject thisEnemy, int index, float delay)
    {
        StartCoroutine(CastDrug2(thisEnemy, index, delay));
    }

    IEnumerator CastDrug2(GameObject thisEnemy, int index, float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject card = this.transform.GetChild(0).GetChild(3).GetChild(index).gameObject;
        GameObject castbar = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Casts/Drug");
        GameObject castbar_clone = Instantiate(castbar, Vector3.zero, Quaternion.identity);
        castbar_clone.transform.SetParent(thisEnemy.transform.GetChild(0).GetChild(2).transform);
        castbar_clone.transform.localScale = new Vector3(1, 1, 1);
        castbar_clone.transform.GetChild(0).GetComponent<DrugCast>().cardToCast = card;
        castbar_clone.transform.GetChild(0).GetComponent<DrugCast>().caster = thisEnemy;
        Vector3 tempPosi = castbar_clone.transform.localPosition;
        tempPosi.z = 0;
        castbar_clone.transform.localPosition = tempPosi;
    }
    public void AnimationEffect(string animationPath, GameObject castOnObject, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float destroyTime)
    {
        castDelay = (castDelay / EventManager2.gameSpeed);
        castOnObject = castOnObject.transform.GetChild(0).gameObject;
        StartCoroutine(AnimationEffect2(animationPath, castOnObject, adjustAnimationPosition, adjustAnimationRotation, castDelay, destroyTime));
    }

    IEnumerator AnimationEffect2(string animationPath, GameObject castOnObject, Vector3 adjustAnimationPosition, Vector3 adjustAnimationRotation, float castDelay, float destroyTime)
    {
        yield return new WaitForSeconds(castDelay);
        //Animation
        Vector3 startPosi = castOnObject.transform.position;
        GameObject animation_prefab = Resources.Load<GameObject>("Effects/" + animationPath);
        startPosi += adjustAnimationPosition;
        GameObject animationClone = (GameObject)Instantiate(animation_prefab, startPosi, Quaternion.identity) as GameObject;
        Vector3 temp_rotation = new Vector3(0, 0, 0);
        temp_rotation += adjustAnimationRotation;
        animationClone.transform.localRotation = Quaternion.Euler(temp_rotation);
        Destroy(animationClone, destroyTime);
    }
    /*  public void EnemyAttackNow()
     {
         ChooseAttack(attackSequenz[sequenzCounter], EventManager2.chosenEnemy.gameObject);
     }

     public void EndTurnInSeconds(float timer)
     {
         timer = (timer / EventManager2.gameSpeed);
         StartCoroutine(EndTurnInSeconds2(timer));
     }
     IEnumerator EndTurnInSeconds2(float timer)
     {
         yield return new WaitForSeconds(timer);
         EventManager2.TurnOffMainPhase();
     } */

    public void CastProtect(int armorValue, GameObject thisEnemy, float delay)
    {
        StartCoroutine(CastProtect2(armorValue, thisEnemy, delay));
    }

    IEnumerator CastProtect2(int armorValue, GameObject thisEnemy, float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject protect = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Casts/Protect");
        GameObject protect_clone = Instantiate(protect, Vector3.zero, Quaternion.identity);
        protect_clone.transform.SetParent(thisEnemy.transform.GetChild(0).GetChild(2).transform);
        protect_clone.transform.localScale = new Vector3(1, 1, 1);
        Vector3 tempPosi = protect_clone.transform.localPosition;
        tempPosi.z = 0;
        protect_clone.transform.localPosition = tempPosi;
        protect_clone.transform.SetAsFirstSibling();
        protect_clone.name = "Protect";

        protect_clone.transform.GetChild(0).GetComponent<ProtectCast>().SetArmorValue(armorValue);

    }


    public void CastStandard(int index, GameObject thisEnemy, float delay)
    {
        StartCoroutine(CastStandard2(index, thisEnemy, delay));
    }

    IEnumerator CastStandard2(int index, GameObject thisEnemy, float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject standard = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Casts/Standard");
        GameObject standard_clone = Instantiate(standard, Vector3.zero, Quaternion.identity);
        standard_clone.transform.SetParent(thisEnemy.transform.GetChild(0).GetChild(2).transform);
        standard_clone.transform.localScale = new Vector3(1, 1, 1);
        Vector3 tempPosi = standard_clone.transform.localPosition;
        tempPosi.z = 0;
        standard_clone.transform.localPosition = tempPosi;
        standard_clone.transform.SetAsFirstSibling();
        standard_clone.name = "Standard";

        int damage = this.transform.GetChild(0).GetChild(3).GetChild(index).GetComponent<Skill>().damage1;
        standard_clone.transform.GetChild(0).GetComponent<StandardCast>().SetDamageValueAndIndex(damage, index);

    }

    public void ChooseRandomEnemyForAttack()
    {

        int enemyChildCount = GameObject.FindGameObjectWithTag("Familiar").transform.childCount;
        if (enemyChildCount > 0)
        {
            enemyChildCount = Random.Range(0, enemyChildCount + 1);
            if (enemyChildCount == 0)
            {
                gamerToAttack = GameObject.FindGameObjectWithTag("Gamer").gameObject;
                Gamer1Familiar0 = 1;
            }
            else
            {
                gamerToAttack = GameObject.FindGameObjectWithTag("Familiar").transform.GetChild(enemyChildCount - 1).gameObject;
                Gamer1Familiar0 = 0;
            }

        }
        else
        {
            gamerToAttack = GameObject.FindGameObjectWithTag("Gamer").gameObject;
            Gamer1Familiar0 = 1;
        }

    }
    public void DamageGamer(GameObject attacker, int damage, float delay)
    {
        delay = (delay / EventManager2.gameSpeed);
        StartCoroutine(DamageGamer2(attacker, damage, delay));
    }
    IEnumerator DamageGamer2(GameObject attacker, int damage, float delay)
    {
        damageDone = 0;
        yield return new WaitForSeconds(delay);
        damage = CalcBuffsExtraDamage(damage);

        if (Gamer1Familiar0 == 1)
        {
            damageDone = gamerToAttack.GetComponent<BaseGamer>().GetDamage(damage); //Hier noch buffs Calculieren
        }
        else
        {
            damageDone = gamerToAttack.GetComponent<BaseFamiliar>().GetDamage(damage); //Hier noch buffs Calculieren
        }
    }

    public void GetBuffed(GameObject enemy, BuffType bufftype, int amount, float timer)
    {
        StartCoroutine(GetBuffed2(enemy, bufftype, amount, timer));
    }

    IEnumerator GetBuffed2(GameObject enemy, BuffType bufftype, int amount, float timer)
    {
        yield return new WaitForSeconds(timer);
        switch (bufftype)
        {
            case BuffType.Armor:
                enemy.GetComponent<BaseEnemy>().GetArmor(amount, enemy);
                break;
            case BuffType.Power:
                enemy.GetComponent<BaseEnemy>().GetPower(amount);
                break;
            case BuffType.Toughness:
                enemy.GetComponent<BaseEnemy>().GetToughness(amount);
                break;
        }
    }


    public void SetDamage(int[] damageArray)
    {
        for (int i = 0; i < damageArray.Length; i++)
        {
            switch (i)
            {
                case 0:
                    damage1 = damageArray[i];
                    break;
                case 1:
                    damage2 = damageArray[i];
                    break;
                case 2:
                    damage3 = damageArray[i];
                    break;
                case 3:
                    damage4 = damageArray[i];
                    break;
                case 4:
                    damage5 = damageArray[i];
                    break;
            }
        }
    }

    public void SetEnemyHP(int hp_value)
    {
        this.GetComponent<BaseEnemy>().maxHP = hp_value;
        this.GetComponent<BaseEnemy>().curHP = hp_value;
    }

    int GetRandomSequenz(int sequenz)
    {
        int sequenzNR = 0;
        int counter = 0;
        List<int> seqList = new List<int>();

        if (sequenz >= 100)
        {
            while (sequenz >= 100)
            {
                sequenz -= 100;
                counter++;
            }
            seqList.Add(counter);
            counter = 0;
            sequenzNR++;
        }
        if (sequenz >= 10)
        {
            while (sequenz >= 10)
            {
                sequenz -= 10;
                counter++;
            }
            sequenzNR++;
            seqList.Add(counter);
        }
        sequenzNR++;
        seqList.Add(sequenz);
        sequenzNR = Random.Range(0, sequenzNR);
        return seqList[sequenzNR];
    }
    /* 
       public void SetAttackSequenz()
       {
           attackSequenz = new int[attackWishSequenz.Length];
           for (int i = 0; i < attackWishSequenz.Length; i++)
           {
               attackSequenz[i] = GetRandomSequenz(attackWishSequenz[i]);
           }
       }

       public void ChooseAttack(int attackSeq, GameObject enemy)
       {
           enemy.GetComponent<Enemy>().ChooseRandomEnemyForAttack();

           switch (attackSeq)
           {
               case 1:
                   enemy.GetComponent<Enemy>().AbilityNumber1();
                   break;
               case 2:
                   enemy.GetComponent<Enemy>().AbilityNumber2();
                   break;
               case 3:
                   enemy.GetComponent<Enemy>().AbilityNumber3();
                   break;
               case 4:
                   enemy.GetComponent<Enemy>().AbilityNumber4();
                   break;
               case 5:
                   enemy.GetComponent<Enemy>().AbilityNumber5();
                   break;
           }
           enemy.GetComponent<Enemy>().sequenzCounter++;
           if (enemy.GetComponent<Enemy>().sequenzCounter == attackSequenz.Length)
           {
               enemy.GetComponent<Enemy>().sequenzCounter = 0;
               SetAttackSequenz();
           }

       }
    */
    public void PutInSkills(string[] skillNames)
    {
        for (int i = 0; i < skillNames.Length; i++)
        {
            GameObject skill = Resources.Load<GameObject>("Combat2/EnemyPrefabs/Skill");
            GameObject skill_clone = Instantiate(skill, Vector3.zero, Quaternion.identity);
            skill_clone.transform.SetParent(this.transform.GetChild(0).GetChild(3).transform);
            skill_clone.transform.localScale = Vector3.zero;

            skill_clone.gameObject.AddComponent(System.Type.GetType(skillNames[i]));

            //Sync
            SkillsDatabase skillDB = GameObject.Find("Database").GetComponent<SkillsDatabase>();
            skillDB.SyncValues(skill_clone.GetComponent<Skill>(), skillDB.GetSkillByID(id));

            skill_clone.name = skillNames[i];
        }

    }
    public virtual void EnemyAttack()
    {

    }

    public virtual void ShotGamer(int index)
    {

    }
    public void ShotGamer2(string skillname, int skillIndex)
    {
        GameObject projectile = Resources.Load<GameObject>("Projectiles/" + skillname);

        GameObject projectile_clone = Instantiate(projectile, this.transform.position, Quaternion.identity);
        projectile_clone.name = skillname;
        projectile_clone.transform.SetParent(GameObject.Find("Canvas").transform);
        projectile_clone.transform.Rotate(0, 180, 0);
        projectile_clone.transform.localScale = new Vector3(1, 1, 1);

        //Target
        projectile_clone.GetComponent<StandardFlying>().Shot(this.gameObject, GameObject.FindGameObjectWithTag("Gamer").gameObject, this.transform.GetChild(0).GetChild(3).GetChild(skillIndex).gameObject);

    }

    public void UseAbility(GameObject enemy, int[] abilities)
    {
        foreach (int number in abilities)
        {
            switch (number)
            {
                case 1:
                    enemy.GetComponent<Enemy>().AbilityNumber1();
                    break;
                case 2:
                    enemy.GetComponent<Enemy>().AbilityNumber2();
                    break;
                case 3:
                    enemy.GetComponent<Enemy>().AbilityNumber3();
                    break;
                case 4:
                    enemy.GetComponent<Enemy>().AbilityNumber4();
                    break;
                case 5:
                    enemy.GetComponent<Enemy>().AbilityNumber5();
                    break;
            }
        }
    }
    public virtual void AbilityNumber1()
    {

    }
    public virtual void AbilityNumber2()
    {

    }
    public virtual void AbilityNumber3()
    {

    }
    public virtual void AbilityNumber4()
    {

    }
    public virtual void AbilityNumber5()
    {

    }

    public virtual void AfterEnemyGotAttacked()
    {

    }

    public virtual void AfterUsingScroll()
    {

    }

    /* 
        public void UpdateEnemyIntention()
        {

            int curSequenz = this.gameObject.GetComponent<Enemy>().sequenzCounter;
            Intention curIntention = this.gameObject.GetComponent<Enemy>().intentionSequenz[attackSequenz[curSequenz] - 1];
            if (curIntention == Intention.Attack || curIntention == Intention.AttackDefend || curIntention == Intention.AttackBuff || curIntention == Intention.AttackDebuff)
            {
                this.gameObject.transform.GetChild(0).GetChild(5).GetChild(0).gameObject.SetActive(true);
                int value = CalcBuffsExtraDamage(this.gameObject.GetComponent<Enemy>().damageSequenz[curSequenz]);
                this.gameObject.transform.GetChild(0).GetChild(5).GetChild(0).GetComponent<TextMeshProUGUI>().text = value.ToString();
            }
            else
            {
                this.gameObject.transform.GetChild(0).GetChild(5).GetChild(0).gameObject.SetActive(false);
            }
            this.gameObject.transform.GetChild(0).GetChild(5).GetComponent<SetEnemyIntention>().SetIntention(curIntention);
        } */

    public int CalcBuffsExtraDamage(int damageSequenz)
    {
        damageSequenz = this.GetComponent<BaseEnemy>().CalcExtraDamage(damageSequenz);
        return damageSequenz;
    }


    public IEnumerator MoveWhenAttack(AttackMovement attackMovement, float delay)
    {
        yield return new WaitForSeconds(delay);



    }





}
