﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy1 : Enemy
{

    // Use this for initialization
    void Start()
    {
        animator = this.transform.GetChild(0).GetComponent<Animator>();
    }

    string[] skillNames = { "Shot", "Shot", "Shield" };

    int[] sequenz1 = { 2 };
    int[] sequenz2 = { 1 };

    List<int[]> SequenzList = new List<int[]>();


    void Awake()
    {
        SetEnemyHP(20);
        PutInSkills(skillNames);
        SequenzList.Add(sequenz1);
        SequenzList.Add(sequenz2);

    }



    public override void ShotGamer(int index)
    {
        ShotGamer2(skillNames[index], index);
        //  animator.SetBool("attack", true);
    }

    void Update()
    {
        if (allowAttack)
        {
            StartCoroutine(UseAbilityAfterSleepTime(sleepTime));
            allowAttack = false;
        }

    }

    IEnumerator UseAbilityAfterSleepTime(float sleepTime)
    {
        yield return new WaitForSeconds(sleepTime);
        int randomIndex = Random.Range(0, SequenzList.Count);
        UseAbility(this.gameObject, SequenzList[randomIndex]);
    }




    public override void AbilityNumber1()
    {
        int index = 0;
        CastStandard(index, this.gameObject, 0f);
    }

    public override void AbilityNumber2()
    {
        int armor = 6;
        CastProtect(armor, this.gameObject, 1f);
    }

    public override void AbilityNumber3()
    {
        int index = 2;
        CastDrug(this.gameObject, index, 1f);
    }





}
