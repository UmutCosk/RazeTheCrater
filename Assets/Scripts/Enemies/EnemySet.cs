﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public enum MonsterType { Start, Normal, Elite, Special }

public class EnemySet : MonoBehaviour
{
    public int id;
    public int level;
    public MonsterType monsterType;
    public int monsterID1;
    public int monsterID2;
    public int monsterID3;
    public int monsterID4;
    public int monsterID5;
    public int kombiID1;
    public int kombiID2;
    public int kombiID3;
    public int kombiID4;
    public int kombiID5;
    public int kombiCounter;

    public Vector3 position1;
    public Vector3 position2;
    public Vector3 position3;
    public Vector3 position4;
    public Vector3 position5;

    public float scale1;
    public float scale2;
    public float scale3;
    public float scale4;
    public float scale5;






    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
