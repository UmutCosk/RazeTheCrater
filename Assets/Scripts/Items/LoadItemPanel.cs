﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadItemPanel : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        UpdateItemPanel();
    }

    // Update is called once per frame
    public void UpdateItemPanel()
    {
        foreach (Transform item in GameObject.FindGameObjectWithTag("ItemPanel").transform)
        {
            Destroy(item.gameObject);
        }

        PlayerPrefSerialize.LoadPlayerValues();
        for (int i = 0; i < PlayerPrefSerialize.playerValues.Items.Count; i++)
        {
            GameObject itemPrefab = Resources.Load<GameObject>("Items/ItemCombat");
            GameObject itemClone = (GameObject)Instantiate(itemPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            itemClone.transform.SetParent(this.gameObject.transform);
            Vector3 tempPosi = itemClone.transform.localPosition;
            tempPosi.z = 0;
            itemClone.transform.localPosition = tempPosi;
            itemClone.transform.localScale = new Vector3(1, 1, 1);

            //Add Component & Transfer from Deck To Hand
            itemClone = EventManager2.AddGameComponent(itemClone, PlayerPrefSerialize.playerValues.Items[i], "Item");
        }

    }
}
