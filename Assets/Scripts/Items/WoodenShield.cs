﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodenShield : Item
{
    int toughnessGain = 2;
    void Start()
    {
        description = "Start each combat with " + toughnessGain + " Toughness";
    }

    public override void StartCombat()
    {

        GetBuffed(BuffType.Toughness, toughnessGain, 0.2f);
    }
}
