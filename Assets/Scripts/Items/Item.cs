﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

public enum ItemTier { Common, Rare, UltraRare, SecretRare, Trait }
public class Item : MonoBehaviour
{
    public int id;
    public string title;
    public string scriptName;
    public string description;

    //Findable only in a goldroom
    public ItemTier itemTier;
    public string imagePath;


    public virtual void UponPickUp() { }
    public virtual void StartCombat() { }

    //Shield & Armor
    public virtual float ShieldArmorConversion(float shieldAmount) { return shieldAmount; }




    public void GetBuffed(BuffType bufftype, int amount, float timer)
    {
        StartCoroutine(GetBuffed2(bufftype, amount, timer));
    }

    IEnumerator GetBuffed2(BuffType bufftype, int amount, float timer)
    {
        yield return new WaitForSeconds(timer);
        switch (bufftype)
        {
            case BuffType.Armor:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().GetArmor(amount,GameObject.FindGameObjectWithTag("Gamer").gameObject);
                break;
            case BuffType.Power:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().GetPower(amount);
                break;
            case BuffType.Toughness:
                GameObject.FindGameObjectWithTag("Gamer").GetComponent<BaseGamer>().GetToughness(amount);
                break;
        }
    }


    // //Other Parameters
    // int itemAlreadyUsed;
    // public Vector3 zero = new Vector3(0, 0, 0);
    // public int cooldown;
    // // Use this for initialization
    // public Skill skill;
    // List<int> currentEnemys = new List<int>();
    // void Awake()
    // {
    //     skill = GameObject.Find("Database").GetComponent<Skill>();
    // }

    // public virtual void OnPointerClick(PointerEventData eventData)
    // {
    // }

    // public virtual void TriggerEffect()
    // {

    // }

    // public virtual int TriggerEffect2(int value)
    // {
    //     return value;
    // }

    // public virtual int ChangePriceOf(int value, ChangePrice changePrice)
    // {

    //     return value;
    // }

    // public virtual int ChangeChanceOf(int value, ChangeChance changeChance)
    // {

    //     return value;
    // }
    // public virtual int ChangeRewardOf(int value, ChangeReward ChangeReward)
    // {

    //     return value;
    // }
    // public virtual int ChangeValueOf(int value, ChangeValue ChangeValue)
    // {

    //     return value;
    // }
    // public virtual void ConsumableBuyEffect()
    // {

    // }
    // public virtual void EnteringRoom()
    // {

    // }
    // public virtual void LeavingRoom()
    // {

    // }
    // public virtual void StartOfCombat()
    // {

    // }
    // public virtual void EndOfCombat()
    // {

    // }
    // public virtual void StartTurn()
    // {

    // }
    // public virtual void EndTurn()
    // {

    // }

    // public virtual void FamiliarDeath()
    // {

    // }

    // public virtual int FamiliarTakesDamageAfter(int damageValue)
    // {
    //     return damageValue;
    // }
    // public virtual int FamiliarTakesDamageBefore(int damageValue)
    // {
    //     return damageValue;
    // }

    // public virtual void UponPickUpEffect() { }

    // public virtual void KilledEnemy()
    // {

    // }

    // public virtual void KilledUniqueEnemy()
    // {

    // }
    // public virtual void ItemCooldown()
    // {

    // }


    // public virtual int BurnGamerEffectItemBeforeDamage(int burnValue)
    // {

    //     return burnValue;
    // }
    // public virtual int PoisonGamerEffectItemBeforeDamage(int poisonValue)
    // {

    //     return poisonValue;
    // }
    // public virtual int BleedGamerEffectItemBeforeDamage(int bleedValue)
    // {
    //     return bleedValue;
    // }
    // public virtual int BurnGamerEffectItemAfterDamage(int burnValue)
    // {

    //     return burnValue;
    // }
    // public virtual int PoisonGamerEffectItemAfterDamage(int poisonValue)
    // {

    //     return poisonValue;
    // }
    // public virtual int BleedGamerEffectItemAfterDamage(int bleedValue)
    // {
    //     return bleedValue;
    // }
    // public virtual int BurnImmunItem(int burnValue)
    // {

    //     return burnValue;
    // }
    // public virtual int PoisonImmunItem(int poisonValue)
    // {

    //     return poisonValue;
    // }
    // public virtual int BleedImmunItem(int bleedValue)
    // {
    //     return bleedValue;
    // }
    // //Damage Calculations
    // public virtual int DamageDealAdd(int value) { return value; }
    // public virtual int DamageDealMulti(int value) { return value; }
    // public virtual int DamageReceiveAdd(int value) { return value; }
    // public virtual int DamageReceiveMulti(int value) { return value; }

    // //Damage Effects
    // public virtual int BeforeDealDamageEffect(int value, int position) { return value; }
    // public virtual int AfterDealDamageEffect(int value, int position) { return value; }
    // public virtual int BeforeReceiveDamageEffect(int value) { return value; }
    // public virtual int AfterReceiveDamageEffect(int value) { return value; }

    // //Healing Calculations
    // public virtual int ReceiveHealAdd(int value) { return value; }
    // public virtual int ReceiveHealMulti(int value) { return value; }

    // //Damage Effects
    // public virtual int BeforeHealEffect(int value) { return value; }
    // public virtual int AfterHealEffect(int value) { return value; }

    // //In DamageStep
    // public virtual void BeforeAttackEffect() { }
    // public virtual void AfterAttackEffect() { } //DamageStep
    // public virtual void AfterHPUpdate() { }

    // public bool ListContains(List<int> list, int value)
    // {
    //     bool result = false;
    //     for (int i = 0; i < list.Count; i++)
    //     {
    //         if (list[i] == value)
    //         {
    //             return true;
    //         }
    //     }
    //     return result;
    // }

    // //Getting Armor & Shield
    // public virtual int WhenGettingArmor(int value) { return value; }
    // public virtual int WhenGettingShield(int value) { return value; }

    // public virtual void AllSkillSlotsUsed() { }
    // public void SpawnFamiliar(int hp)
    // {
    //     foreach (Transform child in GameObject.Find("Player").transform.GetChild(1).transform)
    //     {
    //         hp = child.GetComponent<Item>().ChangeValueOf(hp, ChangeValue.FamiliarHP);
    //     }
    //     //Animation Spawn
    //     GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/FamiliarFX");
    //     Vector3 temp_position2 = GameObject.Find("FamiliarSpawnPoint").transform.position;
    //     temp_position2.z = 0;
    //     GameObject animation_prefab_clone2 = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
    //     animation_prefab_clone2.transform.localRotation = Quaternion.Euler(-90f, -90f, 0f);
    //     Destroy(animation_prefab_clone2, 3f);
    //     StartCoroutine(Spawn(1.5f, animation_prefab_clone2, hp));

    // }
    // IEnumerator Spawn(float timer, GameObject clone, int hp)
    // {
    //     yield return new WaitForSeconds(timer);
    //     //Bild aktivieren
    //     GameObject Familiar = GameObject.Find("Player").transform.GetChild(7).gameObject;
    //     Familiar.SetActive(true);
    //     Familiar.GetComponent<Image>().sprite = Resources.Load<Sprite>("Consumables/Pictures/best_friend");
    //     Color alpha = Familiar.GetComponent<Image>().color;
    //     // alpha.a = 0;
    //     Familiar.GetComponent<Image>().color = alpha;
    //     Familiar.transform.GetComponent<BaseFamiliar>().SetHP(hp);
    //     Familiar.transform.GetChild(0).GetComponent<SetFamiliarHP>().SetHP();
    //     Familiar.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<SetFamiliarHP>().SetHP();
    // }

    // public GameObject GetRandomAliveEnemy()
    // {
    //     currentEnemys.Clear();
    //     CombatPrefSerialize.LoadCombatValues();
    //     int totalEnemy = CombatPrefSerialize.combatValues.enemysToKill;
    //     for (int j = 0; j < totalEnemy; j++)
    //     {

    //         currentEnemys.Add(j);

    //     }
    //     int randomIndex = Random.Range(0, currentEnemys.Count);
    //     int randomEnemy = currentEnemys[randomIndex];
    //     GameObject enemy = GameObject.Find("EnemyPanel").transform.GetChild(randomEnemy).gameObject;
    //     return enemy;

    // }
}
