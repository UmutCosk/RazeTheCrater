﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public float ShieldArmorConversion(float faktor)
    {
        foreach (Transform searchingItem in this.transform)
        {
            faktor = searchingItem.GetComponent<Item>().ShieldArmorConversion(faktor);
        }

        return faktor;
    }

    public void StartCombat()
    {
        foreach (Transform searchingItem in this.transform)
        {
            searchingItem.GetComponent<Item>().StartCombat();
        }
    }
}
