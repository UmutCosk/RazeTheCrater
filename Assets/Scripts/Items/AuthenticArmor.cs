﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuthenticArmor : Item
{
    // Use this for initialization
    void Start()
    {
        description = "You now get 100% shield conversion from armor";

    }

    public override float ShieldArmorConversion(float faktor)
    {
        faktor = 1;
        return faktor;
    }
}
