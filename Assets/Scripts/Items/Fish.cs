﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : Item
{

    int raiseHP = 6;
    void Start()
    {
        description = "Raise your max HP by " + raiseHP;
    }


    public override void UponPickUp() //Wenn Item dem ItemPanel hinzugefügt wird
    {
        PlayerPrefSerialize.RaiseMaxHP(raiseHP);
    }
}
