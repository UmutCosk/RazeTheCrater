﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodge : Buff
{

    void Start()
    {
        description = "Reduce the next damage to 0";
    }

}
