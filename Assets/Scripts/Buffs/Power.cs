﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : Buff
{


    public override int DealExtraPlusDamage(int value)
    {
        int power = this.transform.parent.parent.parent.GetComponent<BasePlayer>().power;
        value = value + power;
        return value;
    }
}
