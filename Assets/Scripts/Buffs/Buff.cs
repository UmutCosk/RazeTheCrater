﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public enum BuffModel { Buff, Debuff, BuffState, DebuffState, None };
public enum BuffType
{
    Armor, Power, Toughness, Shield , Energy, Dodge, Heal
}
public class Buff : MonoBehaviour
{

    public int id;
    public string title;
    public string scriptName;
    public string description;
    public string imagePath;
    public BuffModel buffModel;
    public BuffType buffType;

 
 

    public virtual int DealExtraPlusDamage(int value) { return value; }
    public virtual int DealExtraDotDamage(int value) { return value; }

    public virtual int GetExtraPlusDamage(int value) { return value; }
    public virtual int GetExtraDotDamage(int value) { return value; }
    


    // void Start()
    // {
    //     this.level = 1;
    // }
    // public void ChangeLevel(int newLevel)
    // {
    //     this.level = newLevel;

    // }
    // public int LevelAlter(GameObject buff, BuffType bufftype, int level, int value)
    // {
    //     int result = value;

    //     switch (bufftype)
    //     {
    //         case BuffType.Bless:
    //             float blessAlter = 0;
    //             switch (level)
    //             {
    //                 case 1:
    //                     blessAlter = 50;

    //                     break;
    //                 case 2:
    //                     blessAlter = 100;
    //                     break;
    //                 case 3:
    //                     blessAlter = 150;
    //                     break;
    //             }
    //             result = Mathf.RoundToInt(value * ((blessAlter / 100f) + 1));
    //             break;
    //         case BuffType.Blind:
    //             float blindAlter = 0;
    //             switch (level)
    //             {
    //                 case 1:
    //                     blindAlter = 25;

    //                     break;
    //                 case 2:
    //                     blindAlter = 50;
    //                     break;
    //                 case 3:
    //                     blindAlter = 75;
    //                     break;
    //             }
    //             result = Mathf.RoundToInt(value * ((blindAlter / 100f) + 1));
    //             break;
    //         case BuffType.Broken:
    //             float brokenAlter = 0;
    //             switch (level)
    //             {
    //                 case 1:
    //                     brokenAlter = 25;

    //                     break;
    //                 case 2:
    //                     brokenAlter = 50;
    //                     break;
    //                 case 3:
    //                     brokenAlter = 100;
    //                     break;
    //             }
    //             result = Mathf.RoundToInt(value * ((brokenAlter / 100f) + 1));
    //             break;
    //         case BuffType.Empower:
    //             float empowerAlter = 0;
    //             switch (level)
    //             {
    //                 case 1:
    //                     empowerAlter = 50;

    //                     break;
    //                 case 2:
    //                     empowerAlter = 100;
    //                     break;
    //                 case 3:
    //                     empowerAlter = 150;
    //                     break;
    //             }
    //             result = Mathf.RoundToInt(value * ((empowerAlter / 100f) + 1));
    //             break;
    //         case BuffType.Toughness:
    //             float toughnessAlter = 0;
    //             switch (level)
    //             {
    //                 case 1:
    //                     toughnessAlter = 25;

    //                     break;
    //                 case 2:
    //                     toughnessAlter = 50;
    //                     break;
    //                 case 3:
    //                     toughnessAlter = 100;
    //                     break;
    //             }
    //             result = Mathf.RoundToInt(value * ((toughnessAlter / 100f) + 1));
    //             break;
    //         case BuffType.Weaken:
    //             float weakenAlter = 0;
    //             switch (level)
    //             {
    //                 case 1:
    //                     weakenAlter = 25;

    //                     break;
    //                 case 2:
    //                     weakenAlter = 50;
    //                     break;
    //                 case 3:
    //                     weakenAlter = 100;
    //                     break;
    //             }
    //             if (weakenAlter == 100)
    //             {
    //                 result = 1;
    //             }
    //             else
    //             {
    //                 result = Mathf.RoundToInt(value * ((weakenAlter / 100f) + 1));
    //             }
    //             break;

    //     }
    //     return result;


    // }

    // //Sonstiges
    // public virtual int StartPhase(int value) { return value; } //EventManager
    // public virtual int EndPhase(int value) { return value; }  //EventManager
    // public virtual void AfterAttackEffect(GameObject attacker) { } //DamageStep


    // //Damage Calculations
    // public virtual int DamageDealAdd(int value) { return value; }
    // public virtual int DamageDealMulti(int value) { return value; }
    // public virtual int DamageReceiveAdd(int value) { return value; }
    // public virtual int DamageReceiveMulti(int value) { return value; }

    // //Damage Effects
    // public virtual int BeforeReceiveDamageEffect(int value) { return value; }
    // public virtual int AfterReceiveDamageEffect(int value) { return value; }
    // public virtual int BeforeDealDamageEffect(int value, int position) { return value; }
    // public virtual int AfterDealDamageEffect(int value, int position) { return value; }

    // public virtual bool AllowAttack() { return true; }
    // public virtual bool DodgeAttack() { return true; }

    /*  */
    // //Healing Calculations
    // public virtual int ReceiveHealAdd(int value) { return value; }
    // public virtual int ReceiveHealMulti(int value) { return value; }

    // //Damage Effects
    // public virtual int BeforeHealEffect(int value) { return value; }
    // public virtual int AfterHealEffect(int value) { return value; }
    // public virtual int AfterGettingDebuffed(int value) { return value; }

    // //Getting Armor & Shield
    // public virtual int WhenGettingArmor(int value) { return value; }
    // public virtual int WhenGettingShield(int value) { return value; }

}
