﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toughness : Buff
{

    float reduceDamage = 0.5f;
    public override int GetExtraDotDamage(int value)
    {
        int toughness = this.transform.parent.parent.parent.GetComponent<BasePlayer>().toughness;
        if (toughness > 0)
        {
            value = Mathf.RoundToInt(reduceDamage * value);
        }
        return value;
    }
}
