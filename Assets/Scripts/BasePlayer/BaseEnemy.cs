﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

public class BaseEnemy : BasePlayer, IPointerClickHandler
{

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (EventManager2.chosenCard != null)
            {
                EventManager2.chosenEnemy = this.gameObject;
                DamageStep.UseCard(this.gameObject);
            }
            else if (EventManager2.chosenConsumable != null && EventManager2.chosenConsumable.GetComponent<Consumable>().consumableType == ConsumableType.Fruit)
            {
                EventManager2.chosenEnemy = this.gameObject;
                DamageStep.UseConsumableOnEnemy();
            }
        }
    }

    void Start()
    {

    }

    public int GetDamage(int value)
    {

        //Before Getting Damaged
        value = CalcGetExtraBuffDamage(value);

        //Push Getting Damaged
        value = DodgeEffect(value);
        value = CalcArmorDamage(value);
        curHP = curHP - value;

        //After Getting Damaged
        UpdateHPAfterDamage(value);
        return ReturnDamageDone(curHP, value);
    }
    public int GetPureDamage(int value)
    {
        curHP = curHP - value;
        //After Getting Damaged
        UpdateHPAfterDamage(value);
        return ReturnDamageDone(curHP, value);
    }

    int CalcGetExtraBuffDamage(int damageValue)
    {
        //------------------------Punkt 

        foreach (Transform buff in this.transform.GetChild(0).GetChild(0).transform)
        {
            damageValue = buff.GetComponent<Buff>().GetExtraDotDamage(damageValue);
        }

        //------------------------Strich
        foreach (Transform buff in this.transform.GetChild(0).GetChild(0).transform)
        {
            damageValue = buff.GetComponent<Buff>().GetExtraPlusDamage(damageValue);
        }
        return damageValue;
    }


    int ReturnDamageDone(int curHP, int value)
    {
        int result = 0;
        if (curHP < 0)
        {
            result = value - curHP * (-1);
        }
        return result;
    }

    void UpdateHPAfterDamage(int value)
    {
        int hpLost = ReturnDamageDone(curHP, value);
        //HP TEXT ANZEIGE
        this.gameObject.transform.GetChild(0).GetChild(1).GetComponent<SetHP>().UpdateHP();
        CheckKill();
    }

    void CheckKill()
    {
        if (curHP <= 0)
        {
            KillEvent.OnKillEventEnemy(this.gameObject);
        }

    }


    public int CalcExtraDamage(int damageValue)
    {

        damageValue = CalcExtraBuffDamage(damageValue);
        return damageValue;

    }

    int CalcExtraBuffDamage(int damageValue)
    {
        //------------------------Punkt 



        //------------------------Strich
        foreach (Transform buff in this.transform.GetChild(0).GetChild(0).transform)
        {
            damageValue = buff.GetComponent<Buff>().DealExtraPlusDamage(damageValue);
        }



        return damageValue;
    }

}


/*  public void InstantCast()
 {
     allowClick = true;
     thisMustbeTaunt = false;
     List<GameObject> allTaunts = new List<GameObject>();

     // Taunt Start
     foreach (Transform child in GameObject.Find("EnemyPanel").transform)
     {
         if (child.GetComponent<BaseEnemy>().taunt > 0)
         {
             allTaunts.Add(child.gameObject);
             thisMustbeTaunt = true;
             allowClick = false;
         }
     }
     if (thisMustbeTaunt == true)
     {
         for (int i = 0; i < allTaunts.Count; i++)
         {
             if (allTaunts[i] == this.gameObject)
             {
                 allowClick = true;
                 break;
             }
         }
     }
     // Taunt END

     //Skill gewähöt? else Consumable gewählt?
     if (EventManager.chosenSkill != null && allowClick == true)
     {
         //Normaler Skill & Energy vorhanden? Oder Combo Skill?
         if (EventManager.TurnOffSkill != true || EventManager.chosenSkill.GetComponent<Skill>().type == SkillType.ComboSkill)
         {
             EventManager.chosenEnemy = this.gameObject;
             EventManager.chosenTargetNr = this.gameObject.GetComponent<BaseEnemy>().position;
             EventManager.TriggerEvent("TurnOffSkill");
             EventManager.TriggerEvent("ComboEvent");
             EventManager.TriggerEvent("DamageStep");
         }
         else
         {


         }
     }
     else if (EventManager.chosenConsumable != null && allowClick == true)
     {
         EventManager.chosenEnemy = this.gameObject;
         EventManager.TriggerEvent("ConsumConsumable");
         string tempName = EventManager.chosenConsumable.name;
         Consumable consumableComponent = EventManager.chosenConsumable.GetComponent(System.Type.GetType(tempName)) as Consumable;
         consumableComponent.ConsumableEffect();
         EventManager.chosenConsumable = null;
         EventManager.chosenSupport = null;
     }
 }
 public override void OnPointerClick(PointerEventData eventData)
 {
     InstantCast();
 } */




/* 


 public int CalcReceiveDamageSimulator(int value)
 {
     //Before Damage Push
     //Buffs
     foreach (Transform child in GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.GetChild(0).GetChild(2).transform)
     {
         value = child.GetComponent<Buff>().BeforeReceiveDamageEffect(value);
     }

     //Add
     //Buffs
     foreach (Transform child in GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.GetChild(0).GetChild(2).transform)
     {
         value = child.GetComponent<Buff>().DamageReceiveAdd(value);
     }


     //Multi
     //Buffs
     foreach (Transform child in GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.GetChild(0).GetChild(2).transform)
     {
         value = child.GetComponent<Buff>().DamageReceiveMulti(value);
     }

     //After Damage Push
     //Buffs
     foreach (Transform child in GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.GetChild(0).GetChild(2).transform)
     {
         value = child.GetComponent<Buff>().AfterReceiveDamageEffect(value);
     }
     return value;
 }


 private int BeforeReceiveDamageEffect(int value, int position)
 {
     //Buffs
     foreach (Transform child in GameObject.Find("EnemyPanel").transform.GetChild(position).GetChild(0).GetChild(2).transform)
     {
         value = child.GetComponent<Buff>().BeforeReceiveDamageEffect(value);
     }
     return value;
 }
 private int AfterReceiveDamageEffect(int value, int position)
 {
     //Buffs
     foreach (Transform child in GameObject.Find("EnemyPanel").transform.GetChild(position).GetChild(0).GetChild(2).transform)
     {
         value = child.GetComponent<Buff>().AfterReceiveDamageEffect(value);
     }
     return value;
 }
 private int BeforeDealingDamageEffect(int value, int position)//Enemy
 {
     //Items
     foreach (Transform child in GameObject.Find("Player").transform.GetChild(1).transform)
     {
         value = child.GetComponent<Item>().BeforeDealDamageEffect(value, position);
     }
     //Buffs
     foreach (Transform child in GameObject.Find("Player").transform.GetChild(5).transform)
     {
         value = child.GetComponent<Buff>().BeforeDealDamageEffect(value, position);
     }
     return value;
 }
 private int AfterDealingDamageEffect(int value, int position) //Enemy
 {
     //Items
     foreach (Transform child in GameObject.Find("Player").transform.GetChild(1).transform)
     {
         value = child.GetComponent<Item>().AfterDealDamageEffect(value, position);
     }
     //Buffs
     foreach (Transform child in GameObject.Find("Player").transform.GetChild(5).transform)
     {
         value = child.GetComponent<Buff>().AfterDealDamageEffect(value, position);
     }
     //Enemy Script
     foreach (Transform child in GameObject.Find("EnemyPanel").transform)
     {
         value = child.GetComponent<Enemy>().AfterReceivingDamageEffect(value, position);
     }
     return value;
 }


 private int BeforeHealingEffect(int value)
 {

     //Buffs
     foreach (Transform child in GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.GetChild(0).GetChild(2).transform)
     {
         value = child.GetComponent<Buff>().BeforeHealEffect(value);
     }
     return value;
 }
 private int AfterHealingEffect(int value)
 {
     //Buffs
     foreach (Transform child in GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.GetChild(0).GetChild(2).transform)
     {
         value = child.GetComponent<Buff>().AfterHealEffect(value);
     }
     return value;
 }


 int BeforeDamageSupports(int value)
 {

     foreach (Transform child in GameObject.Find("Player").transform.GetChild(8).transform)
     {
         try
         {
             value = child.transform.GetChild(0).GetComponent<Skill>().BeforeDamageSupports(value);
         }
         catch { }
     }


     return value;
 }
 void AfterDamageSupports(int value)
 {
     foreach (Transform child in GameObject.Find("Player").transform.GetChild(8).transform)
     {
         try
         {
             child.transform.GetChild(0).GetComponent<Skill>().AfterDamageSupports(value);
         }
         catch { }
     }

 }

 void Start()
 {

 }

 public void AddMaxHP(int value)
 {
     this.maxHP = this.maxHP + value;
     this.curHP = this.curHP + value;
 }
 public void ReduceAllCooldowns()
 {
     if (cooldown1 > 0)
     {
         cooldown1--;
     }
     if (cooldown2 > 0)
     {
         cooldown2--;
     }
     if (cooldown3 > 0)
     {
         cooldown3--;
     }
     if (cooldown4 > 0)
     {
         cooldown4--;
     }
     if (cooldown5 > 0)
     {
         cooldown5--;
     }
     if (cooldown6 > 0)
     {
         cooldown6--;
     }
 }

 public void AddCooldownToSkillNr(int value, int skillNr)
 {
     if (value > 0)
     {
         switch (skillNr)
         {
             case 1:
                 cooldown1 = value + 1;
                 break;
             case 2:
                 cooldown2 = value + 1;
                 break;
             case 3:
                 cooldown3 = value + 1;
                 break;
             case 4:
                 cooldown4 = value + 1;
                 break;
             case 5:
                 cooldown5 = value + 1;
                 break;
             case 6:
                 cooldown6 = value + 1;
                 break;
         }
     }
 }

 public void AddFatigueToSkillNr(int skillNr)
 {
     switch (skillNr)
     {
         case 1:
             fatigue1 = 1;
             break;
         case 2:
             fatigue2 = 1;
             break;
         case 3:
             fatigue3 = 1;
             break;
         case 4:
             fatigue4 = 1;
             break;
         case 5:
             fatigue5 = 1;
             break;
     }
 }

 public void SkillsAlreadyCastPlusOne()
 {
     skillsAlreadyCast = skillsAlreadyCast++;
 }
 public void ResetSkillsAlreadyCast()
 {
     skillsAlreadyCast = 0;
 }



 public void StealAllBuffs(int stacksToRemove)
 {

 }


 public void GetSpikeDamage(int value)
 {
     curHP = curHP - value;
     FloatingTextController.CreateFloatingText(value.ToString(), this.transform.GetChild(0).gameObject.transform, Color.yellow, "Combat/Animation/GetDamage/EnemyDamage/DamageTextEnemy1");
     EventManager.TriggerEvent("UpdateHP");
 }






 public void SetDeathwish(int value)
 {

 }

 public void setHP(int value)
 {
     this.curHP = value;
     this.maxHP = value;
 }
 public void lowerHP(int value)
 {
     this.curHP = this.curHP - value;

 }

 GameObject animation_prefab_clone_burn;
 public void BurnDamage(int value, int level)
 {

     value = Mathf.RoundToInt(value * EventManager.damageMultiBurn);

     curHP = curHP - value;
     FloatingTextController.CreateFloatingText(value.ToString(), this.transform.GetChild(0).gameObject.transform, Color.red, "Combat/Animation/GetDamage/EnemyDamage/BurnedText");

     // Animation
     GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/BurnedFX/BurnedFX" + level);
     Vector3 temp_position2 = GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.position;
     temp_position2.z = 0;
     animation_prefab_clone_burn = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;


     if (this.curHP <= 0)
     {
         Destroy(animation_prefab_clone_burn);
     }
     else
     {
         Destroy(animation_prefab_clone_burn, 3f);
     }


 }

 GameObject animation_prefab_clone_poison;
 public void PoisonDamage(int value, int level)
 {

     value = Mathf.RoundToInt(value * EventManager.damageMultiPoison);
     curHP = curHP - value;
     FloatingTextController.CreateFloatingText(value.ToString(), this.transform.GetChild(0).gameObject.transform, Color.green, "Combat/Animation/GetDamage/EnemyDamage/PoisonText");

     // Animation
     GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/PoisonFX/PoisonFX" + level);
     Vector3 temp_position2 = GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.position;
     temp_position2.z = 0;
     animation_prefab_clone_poison = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
     animation_prefab_clone_poison.transform.localRotation = Quaternion.Euler(180f, -90f, 90f);
     if (this.curHP <= 0)
     {
         Destroy(animation_prefab_clone_poison);
     }
     else
     {
         Destroy(animation_prefab_clone_poison, 3f);
     }


 }
 GameObject animation_prefab_clone_bleed;
 public void BleedDamage(int value, int level)
 {

     value = Mathf.RoundToInt(value * EventManager.damageMultiBleed);
     curHP = curHP - value;
     FloatingTextController.CreateFloatingText(value.ToString(), this.transform.GetChild(0).gameObject.transform, Color.cyan, "Combat/Animation/GetDamage/EnemyDamage/BleedText");

     // Animation
     GameObject animation_prefab2 = Resources.Load<GameObject>("Effects/Buffs/BleedFX/BleedFX" + level);
     Vector3 temp_position2 = GameObject.Find("EnemyPanel").transform.GetChild(this.position).transform.position;
     temp_position2.z = 0;
     animation_prefab_clone_bleed = (GameObject)Instantiate(animation_prefab2, temp_position2, Quaternion.identity) as GameObject;
     animation_prefab_clone_bleed.transform.localRotation = Quaternion.Euler(0, -90f, 90f);

     if (this.curHP <= 0)
     {
         Destroy(animation_prefab_clone_bleed);
     }
     else
     {
         Destroy(animation_prefab_clone_bleed, 3f);
     }



 }
*/


