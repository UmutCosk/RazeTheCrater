﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

public enum PlayerFocus { Gamer, Enemy0, Enemy1, Enemy2, Enemy3, Enemy4, Dead };

public class BasePlayer : MonoBehaviour, IPointerClickHandler
{

    //---HP & Shield & Armor
    public int maxHP;
    public int curHP;
    public int armor;
    public bool armorUp;

    //-----Buffs (1)
    //Stats
    public int power;
    public int toughness;
    //States
    public int dodge;


    public virtual void OnPointerClick(PointerEventData eventData)
    {

    }

    public void GetArmor(int value, GameObject thisPlayer)
    {
        armor = armor + value;
        if (armor < 0)
        {
            armor = 0;
        }
        if (armorUp)
        {
            thisPlayer.transform.GetChild(0).GetChild(1).GetComponent<SetArmor>().UpdateArmor();
        }

    }
    public void HealHP(int value)
    {
        curHP += value;
        if (curHP > maxHP)
        {
            curHP = maxHP;
        }
        PlayerPrefSerialize.HealHP(value);
        UpdateHP();

    }

    public void UpdateHP()
    {
        this.transform.GetChild(0).GetChild(1).GetComponent<SetHP>().UpdateHP();
    }

    public void GetPower(int value)
    {
        power = power + value;
        if (power < 0)
        {
            power = 0;
        }
        this.transform.GetChild(0).GetChild(0).GetComponent<BuffManager>().UpdateBuff(BuffType.Power);

    }

    public void GetToughness(int value)
    {
        Debug.Log("test");
        toughness = toughness + value;
        Debug.Log(toughness);
        if (toughness < 0)
        {
            toughness = 0;
        }
        this.transform.GetChild(0).GetChild(0).GetComponent<BuffManager>().UpdateBuff(BuffType.Toughness);
    }
    public void GetDodge(int value)
    {
        dodge = dodge + value;
        if (dodge < 0)
        {
            dodge = 0;
        }
        this.transform.GetChild(0).GetChild(0).GetComponent<BuffManager>().UpdateBuff(BuffType.Dodge);
    }


    public void ReduceAllBuffs()
    {
        GetToughness(-1);

        //Remove Armor
        armorUp = false;
        GetArmor(-9999, this.gameObject);
        this.transform.GetChild(0).GetChild(1).GetChild(1).gameObject.SetActive(false);
    }


    public int CalcArmorDamage(int damage)
    {
        int damageDone = damage;
        int newArmor = armor - damage;
        if (newArmor < 0)
        {
            damageDone = newArmor * (-1);
            armor = 0;
        }
        else
        {
            armor = newArmor;
            damageDone = 0;
        }
        this.gameObject.transform.GetChild(0).GetChild(1).GetComponent<SetArmor>().UpdateArmor();

        return damageDone;
    }


    public int DodgeEffect(int damage)
    {
        if (damage > 0 && dodge > 0)
        {
            damage = 0;
            dodge--;
        }
        return damage;
    }

}
