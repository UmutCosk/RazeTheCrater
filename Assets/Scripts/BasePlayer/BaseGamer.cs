﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;


public class BaseGamer : BasePlayer
{
    public int curEnergy;
    public int curFocus;

    void Awake()
    {
        InitGamer();
    }
    void InitGamer()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        maxHP = PlayerPrefSerialize.playerValues.maxHP;
        curHP = PlayerPrefSerialize.playerValues.curHP;
        curEnergy = EventManager2.startEnergy;
        curFocus = EventManager2.startFocus;
    }

    public int GetDamage(int value)
    {
        //Before Getting Damaged
        value = CalcGetExtraBuffDamage(value);

        //Push Getting Damaged
        value = DodgeEffect(value);

        if (armorUp)
        {
            value = CalcArmorDamage(value);
        }

        ReduceHP(value);

        //After Getting Damaged

        return value;
    }

    public int CalcDealExtraDamage(int damageValue)
    {

        damageValue = CalcDealExtraBuffDamage(damageValue);
        return damageValue;

    }

    int CalcDealExtraBuffDamage(int damageValue)
    {
        //------------------------Punkt 
        foreach (Transform buff in this.transform.GetChild(0).GetChild(0).transform)
        {
            damageValue = buff.GetComponent<Buff>().DealExtraDotDamage(damageValue);
        }

        //------------------------Strich
        foreach (Transform buff in this.transform.GetChild(0).GetChild(0).transform)
        {
            damageValue = buff.GetComponent<Buff>().DealExtraPlusDamage(damageValue);
        }
        return damageValue;
    }
    public int CalcGetExtraDamage(int damageValue)
    {

        damageValue = CalcGetExtraBuffDamage(damageValue);
        return damageValue;

    }

    int CalcGetExtraBuffDamage(int damageValue)
    {
        //------------------------Punkt 

        foreach (Transform buff in this.transform.GetChild(0).GetChild(0).transform)
        {
            damageValue = buff.GetComponent<Buff>().GetExtraDotDamage(damageValue);
        }

        //------------------------Strich
        foreach (Transform buff in this.transform.GetChild(0).GetChild(0).transform)
        {
            damageValue = buff.GetComponent<Buff>().GetExtraPlusDamage(damageValue);
        }
        return damageValue;
    }

    public void ReduceHP(int damage)
    {
        curHP -= damage;
        PlayerPrefSerialize.LoseHP(damage, 0);
        UpdateHP();
    }


    public void ReduceCurEnergy(int value)
    {
        curEnergy = curEnergy - value;
        if (curEnergy < 0)
        {
            curEnergy = 0;
        }
        GameObject.FindGameObjectWithTag("Energy").GetComponent<SetEnergy>().UpdateEnergy();

    }

    public void GainCurEnergy(int value)
    {
        curEnergy = curEnergy + value;
        GameObject.FindGameObjectWithTag("Energy").GetComponent<SetEnergy>().UpdateEnergy();
    }



}
