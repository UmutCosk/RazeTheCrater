﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseFamiliar : BasePlayer
{
    public int GetDamage(int value)
    {

        curHP = curHP - value;


        UpdateHPAfterDamage(value);
        return ReturnDamageDone(curHP, value);
    }

    int ReturnDamageDone(int curHP, int value)
    {
        int result = 0;
        if (curHP < 0)
        {
            result = value - curHP * (-1);
        }
        return result;
    }

    void UpdateHPAfterDamage(int value)
    {
        int hpLost = ReturnDamageDone(curHP, value);
        //HP TEXT ANZEIGE
        this.gameObject.transform.GetChild(0).GetChild(1).GetComponent<SetHP>().UpdateHP();
        CheckKill();
    }

    void CheckKill()
    {
        if (curHP <= 0)
        {
            KillEvent.OnKillEventFamiliar(this.gameObject);
        }

    }


}
