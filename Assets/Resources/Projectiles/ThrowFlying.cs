﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowFlying : MonoBehaviour
{

    public AnimationCurve throwCurve;
    GameObject target;
    GameObject caster;

    GameObject card;
    Vector3 enemyPosi;

    bool startMoving = false;

    float travelTime = 4f;

    float scale;

    public bool enemysProjectile;


    public void ThrowShot(GameObject chosenCaster, GameObject chosenTarget, GameObject chosenCard)
    {
        if (chosenCaster.name == "GamerPanel")
        {
            enemysProjectile = false;
        }
        else
        {
            enemysProjectile = true;
        }

        caster = chosenCaster;
        target = chosenTarget;
        card = chosenCard;

        scale = target.transform.localScale.x;
        heightMulti = Random.Range(3.5f, 5f);

        //Winkel bestimmen
        Vector3 dir = target.transform.position - caster.transform.position;
        dir = target.transform.InverseTransformDirection(dir);
        float angleZ = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        float angleY = Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg;
        this.transform.Rotate(0, angleY * 4.5f, angleZ * 0.9f);

        startMoving = true;
    }

    float flightTime;
    float heightMulti;

    // Update is called once per frame
    void Update()
    {
        if (startMoving)
        {
            flightTime += Time.deltaTime / travelTime;
            float calcScale = (scale / 1.25f - 1) * flightTime * 2 + 1.5f;
            this.transform.localScale = new Vector3(calcScale, calcScale, calcScale);

            Vector3 targetPosi = target.transform.position;
            Vector3 position = Vector3.Lerp(caster.transform.position, targetPosi, flightTime);

            position.y += throwCurve.Evaluate(flightTime) * heightMulti;
            this.transform.position = position;
            this.gameObject.transform.Rotate(0, 0, Time.deltaTime * 69);
            if (Mathf.Abs(position.x - targetPosi.x) < 0.1f && Mathf.Abs(position.y - targetPosi.y) < 0.1f)
            {
                card.GetComponent<Skill>().HitEffect(caster, target);
                Destroy(this.gameObject);
            }

        }
    }

}
