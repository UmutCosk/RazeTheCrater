﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SniperFlying : MonoBehaviour
{
    public AnimationCurve shotCurve;
    GameObject target;
    GameObject caster;

    List<Vector3> flyingCoords = new List<Vector3>();

    bool startMoving = false;

    float travelTimeRicochet = 0.03f;
    float travelTimeBurst = 0.03f;
    float scale = 1;

    int sniperIndex = 0;

    void TransformToVector()
    {
        flyingCoords.Clear();
        flyingCoords.Add(caster.transform.position);
        for (int i = 0; i < EventManager2.sniperTargets.Count; i++)
        {
            Vector3 tempVec = EventManager2.sniperTargets[i].transform.position;
            tempVec.z = 1;
            flyingCoords.Add(tempVec);
        }
    }


    public void SniperShot()
    {
        sniperIndex = 0;
        caster = GameObject.FindGameObjectWithTag("Gamer").gameObject;
        target = EventManager2.sniperTargets[sniperIndex];
        TransformToVector();


        //Winkel bestimmen
        Vector3 dir = target.transform.position - caster.transform.position;
        dir = target.transform.InverseTransformDirection(dir);
        float angleZ = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        float angleY = Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg;
        this.transform.Rotate(0, angleY * 5.2f, angleZ);

        startMoving = true;
        this.transform.localScale = new Vector3(1, 1, 1);
    }
    float heightMulti;
    float flightTime;

    void Update()
    {
        if (startMoving && EventManager2.sniperMode == SniperMode.Ricochet)
        {

            flightTime += Time.unscaledDeltaTime / travelTimeRicochet;

            Vector3 targetPosi = flyingCoords[sniperIndex + 1];
            Vector3 position = Vector3.zero;

            position = Vector3.MoveTowards(flyingCoords[sniperIndex], flyingCoords[sniperIndex + 1], flightTime);

            this.transform.position = position;


            if (Mathf.Abs(position.x - targetPosi.x) < 0.1f && Mathf.Abs(position.y - targetPosi.y) < 0.1f)
            {

                EventManager2.sniperTargets[sniperIndex].GetComponent<SniperCheck>().SniperHit();
                startMoving = false;
                if (sniperIndex == EventManager2.sniperTargets.Count)
                {
                    StartCoroutine(DelayNextTarget(0f));
                }
                else
                {
                    StartCoroutine(DelayNextTarget(0.075f));
                }

                sniperIndex++;
                flightTime = 0;
                if (sniperIndex == EventManager2.sniperTargets.Count)
                {
                    GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOff();
                    GameObject.Find("SlowMotion").GetComponent<SlowMotion>().sniper = false;
                    Destroy(this.gameObject);
                }
            }

        }
        if (startMoving && EventManager2.sniperMode == SniperMode.Burst)
        {

            flightTime += Time.unscaledDeltaTime / travelTimeBurst;
            Vector3 targetPosi = flyingCoords[sniperIndex + 1];
            Vector3 position = Vector3.zero;

            position = Vector3.MoveTowards(flyingCoords[0], flyingCoords[sniperIndex + 1], flightTime);

            this.transform.position = position;

            if (EventManager2.sniperTargets[sniperIndex] != null)
            {
                if (Mathf.Abs(position.x - targetPosi.x) < 0.1f && Mathf.Abs(position.y - targetPosi.y) < 0.1f)
                {
                    EventManager2.sniperTargets[sniperIndex].GetComponent<SniperCheck>().SniperHit();
                    sniperIndex++;
                    flightTime = 0;
                    if (sniperIndex == EventManager2.sniperTargets.Count)
                    {
                        GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOff();
                        GameObject.Find("SlowMotion").GetComponent<SlowMotion>().sniper = false;
                        Destroy(this.gameObject);
                    }
                }
            }
            else
            {
                sniperIndex++;
                flightTime = 0;
                if (sniperIndex == EventManager2.sniperTargets.Count)
                {
                    GameObject.Find("SlowMotion").GetComponent<SlowMotion>().ToggleOff();
                    GameObject.Find("SlowMotion").GetComponent<SlowMotion>().sniper = false;
                }
                Destroy(this.gameObject);
            }
        }
    }

    IEnumerator DelayNextTarget(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        startMoving = true;
    }




}
