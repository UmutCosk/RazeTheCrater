﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;



public class StandardFlying : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {

    }

    public AnimationCurve shotCurve;
    public AnimationCurve throwCurve;
    GameObject target;
    GameObject caster;

    public GameObject card;
    Vector3 enemyPosi;

    bool standardFlying = false;
    bool throwFlying = false;

    float travelTime;

    public bool enemysProjectile;
    bool glow = true;

    float scale;

    IEnumerator DelayGlow()
    {
        yield return new WaitForSecondsRealtime(0.76f);
        this.GetComponent<SniperCheck>().activeGlow = false;
    }

    public void Shot(GameObject chosenCaster, GameObject chosenTarget, GameObject chosenCard)
    {
        caster = chosenCaster;
        target = chosenTarget;
        card = chosenCard;
        scale = target.transform.localScale.x;
        this.GetComponent<SniperCheck>().activeGlow = true;
        enemysProjectile = true;
        if (chosenCaster.name == "GamerPanel")
        {
            enemysProjectile = false;
        }

        if (chosenCard.GetComponent<Skill>().skillCategory == SkillCategory.Standard)
        {
            heightMulti = 1;
            //Winkel bestimmen
            Vector3 dir = target.transform.position - caster.transform.position;
            dir = target.transform.InverseTransformDirection(dir);
            float angleZ = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            float angleY = Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg;
            this.transform.Rotate(0, angleY * 5.2f, angleZ);
            travelTime = 3f;
            standardFlying = true;


        }
        else if (chosenCard.GetComponent<Skill>().skillCategory == SkillCategory.Throw)
        {
            heightMulti = Random.Range(3.5f, 5f);

            //Winkel bestimmen
            Vector3 dir = target.transform.position - caster.transform.position;
            dir = target.transform.InverseTransformDirection(dir);
            float angleZ = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            float angleY = Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg;
            this.transform.Rotate(0, angleY * 4.5f, angleZ * 0.9f);
            travelTime = 4f;
            throwFlying = true;
        }
    }
    float heightMulti;
    float flightTime;

    // Update is called once per frame

    void Update()
    {
        if (GameObject.Find("SlowMotion").GetComponent<SlowMotion>().sniper)
        {
            if (glow)
            {
                this.transform.GetChild(1).gameObject.SetActive(true);
            }
        }
        if (standardFlying)
        {
            flightTime += Time.deltaTime / travelTime;
            float calcScale = 1;
            if (target != GameObject.FindGameObjectWithTag("Gamer").gameObject)
            {
                calcScale = (scale / 1.25f - 1) * flightTime * 2 + 1.5f;
            }
            else
            {
                calcScale = (1 - scale / 1.5f) * flightTime * 2 + 1;
            }


            this.transform.localScale = new Vector3(calcScale, calcScale, calcScale);
            Vector3 position = Vector3.zero;
            if (target != null && caster != null)
            {
                position = Vector3.Lerp(caster.transform.position, target.transform.position, flightTime);
            }
            else
            {
                Destroy(this.gameObject);
            }
            this.transform.position = position;

            if (target != null && caster != null)
            {
                if (Mathf.Abs(position.x - target.transform.position.x) < 0.1f)
                {
                    if (target != null && caster != null)
                    {
                        this.GetComponent<Skill>().HitEffect(caster, target);
                        standardFlying = false;
                        Destroy(this.gameObject, 0.1f);
                    }
                    else
                    {
                        Destroy(this.gameObject);
                    }
                }
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        else if (throwFlying)
        {
            flightTime += Time.deltaTime / travelTime;
            float calcScale = 1;
            if (target != GameObject.FindGameObjectWithTag("Gamer").gameObject)
            {
                calcScale = (scale / 1.25f - 1) * flightTime * 2 + 1.5f;
            }
            else
            {
                calcScale = (1 - scale / 1.5f) * flightTime * 2 + 1;
            }
            this.transform.localScale = new Vector3(calcScale, calcScale, calcScale);

            Vector3 targetPosi = target.transform.position;
            Vector3 position = new Vector3();
            if (target != null && caster != null)
            {
                position = Vector3.Lerp(caster.transform.position, targetPosi, flightTime);
            }
            else
            {
                Destroy(this.gameObject);
            }

            position.y += throwCurve.Evaluate(flightTime) * heightMulti;
            this.transform.position = position;
            this.gameObject.transform.Rotate(0, 0, Time.deltaTime * 69);
            if (target != null && caster != null)
            {
                if (Mathf.Abs(position.x - targetPosi.x) < 0.1f && Mathf.Abs(position.y - targetPosi.y) < 0.1f)
                {
                    if (target != null && caster != null)
                    {
                        this.GetComponent<Skill>().HitEffect(caster, target);
                        throwFlying = false;
                        Destroy(this.gameObject, 0.1f);
                    }
                    else
                    {
                        Destroy(this.gameObject);
                    }
                }
            }
            else
            {
                Destroy(this.gameObject);
            }
        }




    }

}
