﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using TMPro;

public class SkillTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    private GameObject tooltip_go;
    private GameObject skillPanel;
    private Rect sceneRect;
    private Skill skillComponent;
    SkillsDatabase skillDB;
    IEnumerator timer;
    float deltaX = 0f;
    float deltaY = 0f;
    float correctX = 0f;
    float correctY = 0f;
    string description;
    string title;

    void Start()
    {

        try
        {
            switch (this.gameObject.scene.name)
            {
                case "Combat":
                    skillPanel = GameObject.Find("/Canvas/SceneObj");
                    sceneRect = new Rect(-8.9f, -5f, 2 * 8.9f, 2 * 5f);
                    break;
                case "Map1":
                    skillPanel = GameObject.Find("/Canvas/Crafting");
                    sceneRect = new Rect(-8.9f, -5f, 2 * 8.9f, 2 * 5f);
                    break;
                case "Map2":
                    skillPanel = GameObject.Find("/Canvas/Crafting");
                    sceneRect = new Rect(-8.9f, -5f, 2 * 8.9f, 2 * 5f);
                    break;
                case "Map3":
                    skillPanel = GameObject.Find("/Canvas/Crafting");
                    sceneRect = new Rect(-8.9f, -5f, 2 * 8.9f, 2 * 5f);
                    break;
                case "Blacksmith":
                    skillPanel = GameObject.Find("/Canvas/SceneObj");
                    sceneRect = new Rect(-8.9f, -5f, 2 * 8.9f, 2 * 5f);
                    break;
                case "Shop":
                    skillPanel = GameObject.Find("/Canvas/Upgrade");
                    sceneRect = new Rect(-8.9f, -5f, 2 * 8.9f, 2 * 5f);
                    break;
            }
        }
        catch
        {

        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

        skillDB = GameObject.Find("Database").GetComponent<SkillsDatabase>();
        timer = ToolTipsAfterXSeconds();
        StartCoroutine(timer);


    }

    IEnumerator ToolTipsAfterXSeconds()
    {
        yield return new WaitForSeconds(0.20f); // nach 0,8 Sekunden soll das Tooltip erscheinen

        skillComponent = this.GetComponent(System.Type.GetType(this.name)) as Skill;
        skillDB = GameObject.Find("Database").GetComponent<SkillsDatabase>();
        tooltip_go = Instantiate(Resources.Load("Skills/SkillTooltipPrefab")) as GameObject;

        tooltip_go.transform.SetParent(skillPanel.transform.parent);
        tooltip_go.transform.SetAsLastSibling();
        tooltip_go.transform.localScale = new Vector3(1, 1, 1);
        description = skillComponent.description;
        title = skillComponent.title;

        tooltip_go.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = title;
        tooltip_go.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = description;





        Vector2 tooltipPos = new Vector2(0, 0);
        tooltipPos.x = this.transform.position.x + deltaX; // Später hier statt 1920 PlayerPrefsWidth
        tooltipPos.y = this.transform.position.y + deltaY; // Später hier statt 1080 PlayerPrefsHeight
        tooltip_go.transform.position = tooltipPos;

        correctBounds(tooltip_go);
        Vector3 temp_position = tooltip_go.transform.localPosition;
        temp_position.z = 0;
        tooltip_go.transform.localPosition = temp_position;
    }

    public void OnPointerExit(PointerEventData eventData)
    {

        EventSystem.current.SetSelectedGameObject(null);
        try
        {
            StopCoroutine(timer);

        }
        catch
        {

        }

        Destroy(tooltip_go);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Destroy(this.tooltip_go);  //solves Bug, causing the tooltip not to disappear after the object is destroyed by click.
    }

    private void correctBounds(GameObject tooltip_go)
    {
        Vector3[] objectCorners = new Vector3[4];
        //Vector3 canvasCentre = new Vector3(0, 0, 90); //später hier die Zahlen mit PlayerPrefs Height,Width ersätzen
        Vector3 objectCentre = tooltip_go.transform.position;
        Vector3 transformObjPosi;
        tooltip_go.GetComponent<RectTransform>().GetWorldCorners(objectCorners);

        if (!sceneRect.Contains(objectCorners[1]) && !sceneRect.Contains(objectCorners[2]) && !sceneRect.Contains(objectCorners[3]))
        { //Oben und Rechts
            transformObjPosi = objectCentre;
            transformObjPosi.y = transformObjPosi.y - correctY;
            transformObjPosi.x = transformObjPosi.x - correctX;
            tooltip_go.transform.position = transformObjPosi;

        }
        else if (!sceneRect.Contains(objectCorners[1]) && !sceneRect.Contains(objectCorners[2]))
        { //Oben
            transformObjPosi = objectCentre;
            transformObjPosi.y = transformObjPosi.y - correctY;
            tooltip_go.transform.position = transformObjPosi;

        }
        else if (!sceneRect.Contains(objectCorners[2]) && !sceneRect.Contains(objectCorners[3]))
        { //Rechts

            transformObjPosi = objectCentre;
            transformObjPosi.x = transformObjPosi.x - correctX;
            tooltip_go.transform.position = transformObjPosi;

        }
        else if (!sceneRect.Contains(objectCorners[0]) && !sceneRect.Contains(objectCorners[3]))
        { //Unten

            transformObjPosi = objectCentre;
            transformObjPosi.y = transformObjPosi.y + correctY;
            tooltip_go.transform.position = transformObjPosi;
        }
        else if (!sceneRect.Contains(objectCorners[0]) && !sceneRect.Contains(objectCorners[3]) && !sceneRect.Contains(objectCorners[1]))
        { //Unten und Links

            transformObjPosi = objectCentre;
            transformObjPosi.y = transformObjPosi.y + correctY;
            transformObjPosi.x = transformObjPosi.x + correctX;
            tooltip_go.transform.position = transformObjPosi;
        }
        else if (!sceneRect.Contains(objectCorners[0]) && !sceneRect.Contains(objectCorners[1]))
        { //Links

            transformObjPosi = objectCentre;

            transformObjPosi.x = transformObjPosi.x + correctX;
            tooltip_go.transform.position = transformObjPosi;
        }
    }
}
