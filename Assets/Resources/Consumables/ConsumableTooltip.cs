﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using TMPro;

public class ConsumableTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    private GameObject tooltip_go;
    private GameObject consumablePanel;
    private Rect sceneRect;
    private Consumable consumableComponent;
    ConsumablesDatabase consumableDB;
    float deltaX = 0f;
    float deltaY = 0f;
    float correctX = 0f;
    float correctY = 0f;

    void Start()
    {

        try
        {
            // Definiere ParentObject für Tooltip je nach Szene
            switch (this.gameObject.scene.name)
            {
                case "Shop":
                    consumablePanel = GameObject.Find("/Canvas/SceneObj");
                    sceneRect = new Rect(-8.9f, -5f, 2 * 8.9f, 2 * 5f);
                    deltaX = 1.9f;
                    deltaY = 0f;
                    correctX = 0f;
                    correctY = 0f;

                    break;
                case "Combat":
                    consumablePanel = GameObject.Find("/Canvas/SceneObj");
                    sceneRect = new Rect(-8.9f, -5f, 2 * 8.9f, 2 * 5f);
                    deltaX = 1.46f;
                    deltaY = 0f;
                    correctX = 0f;
                    correctY = 0f;

                    break;
            }
        }
        catch
        {

        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

        consumableComponent = this.GetComponent(System.Type.GetType(this.name)) as Consumable;
        consumableDB = GameObject.Find("Database").GetComponent<ConsumablesDatabase>();
        tooltip_go = Instantiate(Resources.Load("Consumables/ConsumableTooltipPrefab")) as GameObject;
        tooltip_go.transform.SetParent(consumablePanel.transform.parent);
        tooltip_go.transform.SetAsLastSibling();

        string description = "";
        description = consumableComponent.description;
        tooltip_go.transform.localScale = new Vector3(1, 1, 1);
        tooltip_go.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = consumableDB.GetConsumableByID(consumableComponent.id).title;
        tooltip_go.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = description;



        Vector2 tooltipPos = new Vector2(0, 0);
        tooltipPos.x = this.transform.position.x + deltaX; // Später hier statt 1920 PlayerPrefsWidth
        tooltipPos.y = this.transform.position.y + deltaY; // Später hier statt 1080 PlayerPrefsHeight
        tooltip_go.transform.position = tooltipPos;
        correctBounds(tooltip_go);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        EventSystem.current.SetSelectedGameObject(null);
        Destroy(tooltip_go);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        EventSystem.current.SetSelectedGameObject(null);
        Destroy(tooltip_go);
    }





    private void correctBounds(GameObject tooltip_go)
    {
        Vector3[] objectCorners = new Vector3[4];
        //        Vector3 canvasCentre = new Vector3(0, 0, 90); //später hier die Zahlen mit PlayerPrefs Height,Width ersätzen
        Vector3 objectCentre = tooltip_go.transform.position;
        Vector3 transformObjPosi;
        tooltip_go.GetComponent<RectTransform>().GetWorldCorners(objectCorners);


        if (!sceneRect.Contains(objectCorners[1]) && !sceneRect.Contains(objectCorners[2]) && !sceneRect.Contains(objectCorners[3]))
        { //Oben und Rechts

            transformObjPosi = objectCentre;
            transformObjPosi.y = transformObjPosi.y - correctX;
            transformObjPosi.x = transformObjPosi.x - correctY;
            tooltip_go.transform.position = transformObjPosi;

        }
        else if (!sceneRect.Contains(objectCorners[1]) && !sceneRect.Contains(objectCorners[2]))
        { //Oben

            transformObjPosi = objectCentre;
            transformObjPosi.y = transformObjPosi.y - correctY;
            tooltip_go.transform.position = transformObjPosi;

        }
        else if (!sceneRect.Contains(objectCorners[2]) && !sceneRect.Contains(objectCorners[3]))
        { //Rechts

            transformObjPosi = objectCentre;
            transformObjPosi.x = transformObjPosi.x - correctX;
            tooltip_go.transform.position = transformObjPosi;

        }



    }
}
