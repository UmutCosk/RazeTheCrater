﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StopAnimation : StateMachineBehaviour
{

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo)
    {

        if (stateInfo.IsName("ForwardAttack"))
        {
            /*   ac = animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
              ac.layers[0].stateMachine.states[0].state.transitions[1].exitTime = ac.layers[0].stateMachine.states[0].state.transitions[0].exitTime / EventManager2.gameSpeed; */
           animator.speed = 1 * EventManager2.gameSpeed;
           // EventManager2.chosenEnemy.GetComponent<Enemy>().EnemyAttackNow();
            animator.SetBool("attack", false);
        }


    }

    public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo)
    {

        if (stateInfo.IsName("ForwardAttack"))
        {
            /*     float endTimer = EventManager2.chosenEnemy.GetComponent<Enemy>().endTurnInSecond;
                endTimer = endTimer / EventManager2.gameSpeed;
                EventManager2.chosenEnemy.GetComponent<Enemy>().EndTurnInSeconds(endTimer); */
            animator.speed = 1;
        }
     

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
