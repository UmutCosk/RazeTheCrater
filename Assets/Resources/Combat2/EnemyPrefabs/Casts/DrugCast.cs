﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Timers;
using TMPro;


public class DrugCast : MonoBehaviour
{

    Image timerBar;
    float cooldown = 5f;
    float interval = 0.05f;
    float timeLeft;

    public GameObject cardToCast;
    public GameObject caster;

    int armorValue;
    void OnEnable()
    {

        this.name = "Drug";
        timerBar = this.GetComponent<Image>();
        timerBar.fillAmount = 1;
        TimersManager.SetLoopableTimer(this, interval, CountDown);
        ResetCountdown();
    }




    void CountDown()
    {
        timeLeft -= interval;
        float newAmount = 1 - timeLeft / cooldown;
        timerBar.fillAmount = newAmount;

        if (timeLeft <= 0)
        {
            cardToCast.GetComponent<Skill>().UseInstantCard(caster);
            ResetCountdown();
            Destroy(this.transform.parent.gameObject);
        }


    }

    public void ResetCountdown()
    {
        timeLeft = cooldown;
    }

}
