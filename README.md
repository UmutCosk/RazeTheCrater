> This game is an idea of a deck-building game with items and cards to loot from enemies, shops and secrets and much more..
> Round-Based Combat system with real-time fights between each round
> Energy ressources are used for each card. Cards represent projectiles to hit enemy or to buff/debuff a player.

> Combat

![Combat](./show case/combat.gif)

> Map 

![Map](./show case/map.PNG)



# Introduction
Die README ist dazu da, damit sie gelesen wird. Also LEST sie!

# Getting Started
Als erstes solltet ihr wissen, dass wir zwei Repositories haben.  
Einmal das Repo für das Unity-Projekt und eins für die Dokumentation.  
Das Repo für das Unity-Projekt hat zwei Zweige, den sog. master-branch und develop-branch.  
Im develop-branch wird nur der aktuelle Entwicklungsstand verwaltet, während im master-branch nur stable-releases verwaltet werden.  
Das Doku-Repo wird nur den master-branch haben, da wir ja nur Dokumente verwalten werden.  
Trotzdem sollten wir auf eine saubere Struktur der Dokumente achten, z.B Unity-spezifische Dokumente, oder Dokumente bezüglich des Spiels selbst etc.  

Unsere Arbeitsaufgaben werden im Backlog gepflegt.  
Ich schlage vor folgende Struktur beizubehalten:  

+-- Feature1  
|   +-- UserStory1  
|   |   +-- Aufgabe1  
|   |   +-- Aufgabe2  
|   +-- UserStory2  
|   |   +-- Aufgabe1  
|   |   +-- Aufgabe2  
+-- Feature 2  
|   +-- UserStory1  

Unsere Features widerum können wir in einzelne Epics verpacken, die als Meilensteine für das Projekt gesehen werden können. 

In die einzelnen Iterationen packen wir NUR die UserStories mit den dazugehörigen Aufgaben. So können wir im über die Epics bzw. die Features immer den Überblick behalten, während die einzelnen Stories bzw. Aufgaben über die Zeit hinweg verplant und bearbeitet werden können.  


# Commits
Wir sollten uns an eine Commit-Struktur halten, damit wir nicht anfangen irgendwelche Romane in die Commit-Messages zu schreiben.
Die Commit-Message besteht aus 3 Zeilen, mit jeweils einer Zeile Abstand.  
Die erste Zeile soll ganz kurz allgemein bechreiben was passiert ist.  
Z.b Updated Code, Modified UI, Fixed Bug or whatever.  
Die Zweite Zeile soll eine etwas genauere Beschreibung von dem liefern, was passiert ist.  
Also Updated Code? Dann warum und welche Stelle?  
Modified UI? Welche UI und welche Teile?  
Fixed Bug? Welcher Bug und wie wurde er behoben? etc.  
Die Drite Zeile besteht nur aus "Related Workitems: #WorkitemID  
Damit wird der Commit einem Workitem zugeordnet und er taucht im TFS auf, um Codes zu reviewn und ggf. direkt über den TFS drauf einzugehen.

Beispiel Commit:  
`Fixed Bug`  

`Fixed Bug, wenn man das Hauptmenü geöffnet hat, während Sounds gespielt wurden. Soundobjekte wurden Referenzen zu Audiodateien zugewiesen. `  

`Related Workitems: #873485`